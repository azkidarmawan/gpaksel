<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use App\Http\Middleware\CheckAdmin;
use App\Http\Middleware\CheckRole;

Route::get('/', function () {
    return redirect('home');
});

Route::group(['prefix' => 'desa'], function(){
	Route::get('/', 'DesaController@index')->middleware(CheckRole::class);
	Route::get('/add', 'DesaController@create')->middleware(CheckRole::class);
	Route::post('/store', 'DesaController@store')->middleware(CheckRole::class);
	Route::get('/edit/{kode_desa}', 'DesaController@edit')->middleware(CheckAdmin::class);
	Route::post('/update/{kode_desa}', 'DesaController@update')->middleware(CheckAdmin::class);
	Route::get('/delete/{kode_desa}', 'DesaController@destroy')->middleware(CheckAdmin::class);
});

Route::group(['prefix' => 'kelompok'], function(){
	Route::get('/', 'KelompokController@index')->middleware(CheckRole::class);
	Route::get('/add', 'KelompokController@create')->middleware(CheckRole::class);
	Route::post('/store', 'KelompokController@store')->middleware(CheckRole::class);
	Route::get('/edit/{kode_desa}/{kode_kelompok}', 'KelompokController@edit')->middleware(CheckAdmin::class);
	Route::post('/update/{kode_desa}/{kode_kelompok}', 'KelompokController@update')->middleware(CheckAdmin::class);
	Route::get('/delete/{kode_desa}/{kode_kelompok}', 'KelompokController@destroy')->middleware(CheckAdmin::class);
	Route::get('/getkelompok', 'KelompokController@getkelompok');
});

Route::group(['prefix' => 'guru'], function(){
	Route::get('/', 'GuruController@index')->middleware(CheckRole::class);
	Route::get('/add', 'GuruController@create')->middleware(CheckRole::class);
	Route::post('/store', 'GuruController@store')->middleware(CheckRole::class);
	Route::get('/edit/{kode_guru}', 'GuruController@edit')->middleware(CheckAdmin::class);
	Route::post('/update/{kode_guru}', 'GuruController@update')->middleware(CheckAdmin::class);
	Route::get('/delete/{kode_guru}', 'GuruController@destroy')->middleware(CheckAdmin::class);
});

Route::group(['prefix' => 'murid'], function(){
	Route::get('/', 'MuridController@index')->middleware(CheckRole::class);
	Route::get('/add', 'MuridController@create')->middleware(CheckRole::class);
	Route::post('/store', 'MuridController@store')->middleware(CheckRole::class);
	Route::get('/edit/{kode_murid}', 'MuridController@edit')->middleware(CheckAdmin::class);
	Route::post('/update/{kode_murid}', 'MuridController@update')->middleware(CheckAdmin::class);
	Route::get('/delete/{kode_murid}', 'MuridController@destroy')->middleware(CheckAdmin::class);
	Route::get('/getimg', 'MuridController@getimg');
});

Route::group(['prefix' => 'absen-guru'], function(){
	Route::get('/', 'AbsenGuruController@index')->middleware(CheckRole::class);
	Route::get('/add', 'AbsenGuruController@create')->middleware(CheckRole::class);
	Route::post('/store', 'AbsenGuruController@store')->middleware(CheckRole::class);
	Route::get('/edit/{tahun}/{bulan}/{minggu}', 'AbsenGuruController@edit')->middleware(CheckAdmin::class);
	Route::post('/update/{tahun}/{bulan}/{minggu}', 'AbsenGuruController@update')->middleware(CheckAdmin::class);
	Route::get('/delete/{tahun}/{bulan}/{minggu}', 'AbsenGuruController@destroy')->middleware(CheckAdmin::class);
});

Route::group(['prefix' => 'absensi'], function(){
	Route::get('/', 'AbsenController@index')->middleware(CheckRole::class);
	Route::get('/add', 'AbsenController@create')->middleware(CheckRole::class);
	Route::post('/store', 'AbsenController@store')->middleware(CheckRole::class);
	Route::get('/edit/{kode_absen}/{kode_murid}/{tahun}', 'AbsenController@edit')->middleware(CheckAdmin::class);
	Route::post('/update/{kode_absen}/{kode_murid}/{tahun}', 'AbsenController@update')->middleware(CheckAdmin::class);
	Route::get('/delete/{kode_absen}/{kode_murid}/{tahun}', 'AbsenController@destroy')->middleware(CheckAdmin::class);
});

Route::group(['prefix' => 'penilaian'], function(){
	Route::get('/', 'PenilaianController@index')->middleware(CheckRole::class);
	Route::get('/add', 'PenilaianController@create')->middleware(CheckRole::class);
	Route::get('/reset', 'PenilaianController@reset')->middleware(CheckRole::class);
	Route::post('/store', 'PenilaianController@store')->middleware(CheckRole::class);
	Route::get('/edit/{kode_murid}/{semester}', 'PenilaianController@edit')->middleware(CheckAdmin::class);
	Route::post('/update/{kode_murid}/{semester}', 'PenilaianController@update')->middleware(CheckAdmin::class);
	Route::get('/delete/{kode_murid}/{semester}', 'PenilaianController@destroy')->middleware(CheckAdmin::class);
	Route::get('/cetak/{kode_murid}/{semester}', 'PenilaianController@cetak')->middleware(CheckRole::class);
});