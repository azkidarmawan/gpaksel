<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Notifikasi extends Model
{
    public static function DaftarNotifikasi($id, $jenis){
		// jenis_notifikasi 1 = Master, 2 = Proses
		if($jenis < 6)
			$daftar = DB::select( DB::raw("SELECT * FROM t_notifikasi  WHERE id > :id ORDER BY id DESC"), array('id' => $id));
		else $daftar = DB::select( DB::raw("SELECT * FROM t_notifikasi  WHERE jenis = 2 AND id > :id ORDER BY id DESC"), array('id' => $id));
		return $daftar;
	}

	public static function RecentActivity($id, $jenis){
		// jenis_notifikasi 1 = Master, 2 = Proses
		if($jenis < 6)
			$daftar = DB::select( DB::raw("SELECT * FROM t_notifikasi  WHERE id+4 > :id ORDER BY id DESC"), array('id' => $id));
		else $daftar = DB::select( DB::raw("SELECT * FROM t_notifikasi  WHERE jenis = 2 AND id+4 > :id ORDER BY id DESC"), array('id' => $id));
		return $daftar;
	}
	
	public static function UpdateNotifikasi($id_profil, $id_notif){
		$update = DB::update( DB::raw("UPDATE profiles SET id_notif = :id_notif WHERE id = :id_profil"), array(
			'id_notif' => $id_notif, 'id_profil' => $id_profil
			)); 
		return $update;
	}
	
	public static function CreateNotifikasi($user, $jenis, $log){
		$created_at = \Carbon\Carbon::Now('Asia/Jakarta');
		$simpan = DB::insert(DB::raw("INSERT into t_notifikasi (jenis, log, user_create, created_at) values (:jenis, :log, :user, :created_at)"), array(
			'jenis' => $jenis, 'log' => $log, 'user' => $user, 'created_at' => $created_at
			));
		return $simpan;
	}
}

?>