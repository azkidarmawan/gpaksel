<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Desa extends Model
{
	public $incrementing=false;
	protected $table = 'm_guru';
    protected $primaryKey = 'kode_guru';
    protected $fillable = ['nama_guru', 'alamat_tinggal', 'alamat_sambung','no_telp', 'file'];
    public $timestamps = false;

	public static function DaftarPemasok(){
		$daftar = DB::select( DB::raw("SELECT * FROM m_pemasok"));
		return $daftar;
	}
	
	public static function DaftarPemasokAktif(){
		$daftar = DB::select( DB::raw("SELECT * FROM m_pemasok where status_aktif != '0' OR status_aktif is NULL"));
		return $daftar;
	}
	
	public static function LihatPemasok($id){
			$select = DB::select( DB::raw("SELECT * FROM m_pemasok WHERE kode_pemasok = :id"), array(
				'id' => $id,
				)); 
			return $select;
		}
		
		public static function CreatePemasok($kode_pemasok, $nama_pemasok, $alamat, $telp, $nama_kontak_person, $no_telp_kontak_person, $user_create, $user_date_create){
			$simpan = DB::insert(DB::raw("INSERT into m_pemasok (kode_pemasok, nama_pemasok, alamat, telp, nama_kontak_person, no_telp_kontak_person, user_create, user_date_create) values (:kode_pemasok, :nama_pemasok, :alamat, :telp, :nama_kontak_person, :no_telp_kontak_person, :user_create, :user_date_create)"), array(
				'kode_pemasok' => $kode_pemasok, 'nama_pemasok' => $nama_pemasok, 'alamat' => $alamat, 'telp' => $telp, 'nama_kontak_person' => $nama_kontak_person, 'no_telp_kontak_person' => $no_telp_kontak_person, 'user_create' => $user_create, 'user_date_create' => $user_date_create
				));
			return $simpan;

		}
		
		public static function EditPemasok($kode_pemasok, $nama_pemasok, $alamat, $telp, $nama_kontak_person, $no_telp_kontak_person, $status_aktif, $user_change, $user_date_change){
			$edit = DB::update( DB::raw("UPDATE m_pemasok SET nama_pemasok = :nama_pemasok, alamat = :alamat, telp = :telp, nama_kontak_person = :nama_kontak_person, no_telp_kontak_person = :no_telp_kontak_person, status_aktif = :status_aktif, user_change = :user_change, user_date_change = :user_date_change WHERE kode_pemasok = :kode_pemasok"), array(
				'kode_pemasok' => $kode_pemasok, 'nama_pemasok' => $nama_pemasok, 'alamat' => $alamat, 'telp' => $telp, 'nama_kontak_person' => $nama_kontak_person, 'no_telp_kontak_person' => $no_telp_kontak_person, 'status_aktif' => $status_aktif, 'user_change' => $user_change, 'user_date_change' => $user_date_change
				)); 
			return $edit;
		}
		public static function DeletePemasok($id){
			$delete = DB::delete( DB::raw("DELETE FROM m_pemasok WHERE kode_pemasok = :id"), array(
				'id' => $id
			)); 
			return $delete;
		}
}

