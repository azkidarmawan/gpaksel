<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Models\Master\Fakultas;
use App\Models\Master\Masa;
use App\Models\Master\Status_Aktif;
use App\Models\Tiras\Tiras_Header;
use App\Models\Tiras\Tiras_Detail;
use App\Notifikasi;


class MasaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $datas = DB::table('m_masa')
        ->join('m_status_aktif','m_masa.kode_status','=','m_status_aktif.kode_status')
        ->select('m_masa.*', 'm_status_aktif.*')
        ->get();
        return view('Master\Tiras\Masa.index')->with('datas', $datas);
    }

    public function create()
    {
        $statusaktif= \App\Models\Master\Status_Aktif::all();
        return view('Master\Tiras\Masa.add')->with('statusaktif', $statusaktif);
    }

    public function store(Request $request)
    {
        if($request['masa'] != '') {
            $check = \App\Models\Master\Masa::where('masa',$request['masa'])->first();
        if($check) {
            return redirect()->back()->with('error','Masa Sudah Ada');
            }
        $this->validate($request,[
        'kode_status'=>'required',
        'masa'=>'required',
        'tahun'=>'required',
        ]);

        $tambah= new \App\Models\Master\Masa();
        $tambah->masa=$request['masa'];
        $tambah->tahun=$request['tahun'];
        $tambah->kode_status=$request['kode_status'];
        $tambah->user_create=auth()->user()->name;
        $tambah->user_date_create=\Carbon\Carbon::Now ('Asia/Jakarta');
        $tambah->save();

        // Create New Notification
        $notif = new Notifikasi;
        $log = 'membuat masa '.$request['masa'];
        $notifikasi = $notif->CreateNotifikasi(auth()->user()->getProfile->name, 1, $log);

        return redirect()->to('/masa');
    }
}

    public function edit($masa)
    {
        // $masa = \App\Models\Master\Masa::where('masa',$masa)->firstOrfail();
        $checkInMasa = \App\Models\Tiras\Tiras_Header::where('masa', $masa)->first(); 
        $status = \App\Models\Master\Status_Aktif::all(); 
        $ubahdata = \App\Models\Master\Masa::where('masa',$masa)->firstOrfail(); 
        return view('Master\Tiras\Masa.edit', compact('status','ubahdata','masa','tahun' ,'checkInMasa'));
    }

    public function update(Request $request, $masa)
    {
        if($request['masa'] == '') {
            
            $update = [
                
                'kode_status' => $request['kode_status'],
                'tahun' => $request['tahun'],
                'user_change' => auth()->user()->name,
                'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
            ];

        } else {

            $update = [
                'masa' => $request['masa'],
                'kode_status' => $request['kode_status'],
                'tahun' => $request['tahun'],
                'user_change' => auth()->user()->name,
                'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
            ];

        }

        \App\Models\Master\Masa::where('masa', $masa)->update($update);
        // Create New Notification
        $notif = new Notifikasi;
        $log = 'mengubah masa '.$request['masa'];
        $notifikasi = $notif->CreateNotifikasi(auth()->user()->getProfile->name, 1, $log);
        return redirect()->to('masa');
    }


     public function destroy($masa)
    {

        $checkInMasa= \App\Models\Tiras\Tiras_Header::where('masa', $masa)->get();

        $checkInMasa= \App\Models\Tiras\Tiras_Header::where('masa', $masa)->get();

        if(count($checkInMasa)) {

            return redirect()->back()->with('error','Mohon Maaf Data Ini Terrelasi Ke Tabel Masa.');

        } else if(count($checkInMasa)) {

            return redirect()->back()->with('error','Mohon Maaf Data Ini Terrelasi Ke Tabel Barang');

        } else {

            $thisIs = \App\Models\Master\Masa::where('masa', $masa); 

            // Create New Notification
            $notif = new Notifikasi;
            $log = 'menghapus masa '.$masa;
            $notifikasi = $notif->CreateNotifikasi(auth()->user()->getProfile->name, 1, $log);

            $thisIs->delete();

            return redirect()->to('masa');
        }
    }


}
