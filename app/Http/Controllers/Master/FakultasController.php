<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Notifikasi;
use App\Models\Master\Fakultas;
use App\Models\Master\Masa;
use App\Models\Master\Status_Aktif;
use App\Models\Tiras\Tiras_Header;
use App\Models\Tiras\Tiras_Detail;
use PDF; // pdf namespace
use Excel; // Excel namespace

use App\fakultastoExcel;

class FakultasController extends Controller
{

	public function __construct() //Untuk Auth Login
    {
    	$this->middleware('auth');
    }

    public function index(Request $req) //Untuk Index
    {
		// show all data to index
      $blogs = fakultastoExcel::all();
      view()->share('blogs',$blogs);
      // if request has pdf
      if($req->has('downloadpdf')){
        $pdf = PDF::loadView('pdf')->setPaper('a4', 'landscape');
        return $pdf->download('pdf');
      }
      // if request has excel
	  $time = \Carbon\Carbon::Now('Asia/Jakarta');
	  $result = $time->format('d-m-Y');
      if($req->has('downloadexcel')){
        Excel::create('Fakultas '.$result, function($excel) use ($blogs) {
          $excel->sheet('Sheet 1', function($sheet) use ($blogs) {
            $sheet->fromArray($blogs);
          });
        })->export('xls');
      }
    	$fakultas = \App\Models\Master\Fakultas::orderBy('kode_fakultas')->get();
    	return view('Master\Tiras\Fakultas.index')->with('fakultas', $fakultas);
    }

    public function create() //Untuk Create Master Fakultas
    {
    	return view('Master\Tiras\Fakultas.add');
    }

    public function store(Request $request)
    {

        if($request['kode_fakultas'] != '') {
            $check = \App\Models\Master\Fakultas::where('kode_fakultas',$request['kode_fakultas'])->first();
        if($check) {
            return redirect()->back()->with('error','Kode fakultas Sudah Ada');
            }

    	$this->validate($request,[
    	'kode_fakultas'=>'required',
    	'nama_fakultas'=>'required',
    	'nama_pendek'=>'required',
    	]);

    	$tambah= new \App\Models\Master\Fakultas();
    	$tambah->kode_fakultas=$request['kode_fakultas'];
    	$tambah->nama_fakultas=$request['nama_fakultas'];
    	$tambah->nama_pendek=$request['nama_pendek'];
    	$tambah->user_create=auth()->user()->name;
    	$tambah->user_date_create=\Carbon\Carbon::Now ('Asia/Jakarta');
    	$tambah->save();

		// Create New Notification
		$notif = new Notifikasi;
		$log = 'membuat fakultas '.$request['kode_fakultas'];
		$notifikasi = $notif->CreateNotifikasi(auth()->user()->getProfile->name, 1, $log);
		
    	return redirect()->to('fakultas');
    }
}

    public function edit($kode_fakultas)//untuk edit
    {
    	$checkInTiras = \App\Models\Tiras\Tiras_Header::where('kode_fakultas', $kode_fakultas)->first();
        $checkInBarang = \App\Barang::where('kode_fakultas', $kode_fakultas)->first();
    	$ubahdata = \App\Models\Master\Fakultas::find($kode_fakultas);
    	return view('Master\Tiras\Fakultas.edit', compact('ubahdata', 'checkInTiras', 'checkInBarang'));
    }

    public function update(Request $request, $kode_fakultas)//Untuk Menjalankan Update
    {
        if($request['kode_fakultas'] == '') {
            
            $update = [
                'nama_fakultas' => $request['nama_fakultas'],
                'nama_pendek' => $request['nama_pendek'],
                'user_change' => auth()->user()->name,
                'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
            ];

        } else {

            $update = [
                'kode_fakultas' => $request['kode_fakultas'],
                'nama_pendek' => $request['nama_pendek'],
                'nama_fakultas' => $request['nama_fakultas'],
                'user_change' => auth()->user()->name,
                'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
            ];

        }

        \App\Models\Master\Fakultas::where('kode_fakultas', $kode_fakultas)->update($update);
    	// Create New Notification
		$notif = new Notifikasi;
		$log = 'mengubah fakultas '.$kode_fakultas;
		$notifikasi = $notif->CreateNotifikasi(auth()->user()->getProfile->name, 1, $log);
		
    	return redirect()->to('fakultas');
    }

    public function destroy($kode_fakultas)
    {

        $checkInTiras = \App\Models\Tiras\Tiras_Header::where('kode_fakultas', $kode_fakultas)->get();

        $checkInBarang = \App\Barang::where('kode_fakultas', $kode_fakultas)->get();

        if(count($checkInTiras)) {

            return redirect()->back()->with('error','Mohon Maaf Data Ini Terrelasi Ke Tiras Header.');

        } else if(count($checkInBarang)) {

            return redirect()->back()->with('error','Mohon Maaf Data Ini Terrelasi Ke Barang');

        } else {

            $thisIs = \App\Models\Master\Fakultas::find($kode_fakultas);
	
			// Create New Notification
			$notif = new Notifikasi;
			$log = 'menghapus fakultas '.$kode_fakultas;
			$notifikasi = $notif->CreateNotifikasi(auth()->user()->getProfile->name, 1, $log);
	
            $thisIs->delete();
			
			return redirect()->to('fakultas');
        }
    }


}
