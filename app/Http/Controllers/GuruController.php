<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Guru;
use App\Notifikasi;
use DB;
use PDF; // pdf namespace
use Excel; // Excel namespace

class GuruController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(Request $req)
	{
	    $datas = DB::select( DB::raw("SELECT a.kode_guru,a.nama_guru,a.jenis_kelamin,a.alamat_tinggal,c.nama_desa,b.nama_kelompok,a.no_telp,a.keterangan FROM m_guru a JOIN m_kelompok b ON a.kode_desa = b.kode_desa AND a.kode_kelompok = b.kode_kelompok JOIN m_desa c ON b.kode_desa = c.kode_desa Where a.status = 1"));
	    return view('guru.index',compact('datas'));
	}

	public function create(){
		$datas = DB::select( DB::raw("SELECT kode_desa,nama_desa FROM m_desa WHERE status = 1"));
		return view('guru.add',compact('datas'));
	}

	public function store(Request $request){
		$nama_guru = $request->input('nama_guru');
		$jenis_kelamin= $request->input('jenis_kelamin');
		$alamat_tinggal= $request->input('alamat_tinggal');
		$no_telp2 = explode("_", $request->input('no_telp'));
		$no_telp = $no_telp2[0];
		$keterangan= $request->input('keterangan');
		$kode_desa= $request->input('kode_desa');
		$kode_kelompok= $request->input('kode_kelompok');
		$user_create = auth()->user()->getProfile->name;
		$created_at= \Carbon\Carbon::Now('Asia/Jakarta');

		$datas = DB::select( DB::raw("INSERT INTO `m_guru` (`nama_guru`, `jenis_kelamin`, `alamat_tinggal`,`no_telp`,`keterangan`,`kode_desa`,`kode_kelompok`,`user_create`, `user_date_create`) VALUES (:nama_guru,:jenis_kelamin,:alamat_tinggal,:no_telp,:keterangan,:kode_desa,:kode_kelompok,:user_create,:created_at)"),array('nama_guru' => $nama_guru, 'jenis_kelamin' => $jenis_kelamin, 'alamat_tinggal' => $alamat_tinggal, 'no_telp' => $no_telp, 'keterangan' => $keterangan, 'kode_desa' => $kode_desa, 'kode_kelompok' => $kode_kelompok, 'user_create' => $user_create, 'created_at' => $created_at,));

		return redirect('guru');
	}

	public function edit($kode_guru){
		$desas = DB::select( DB::raw("SELECT kode_desa,nama_desa FROM m_desa WHERE status = 1"));
		$datas = DB::select( DB::raw("SELECT kode_guru,nama_guru,jenis_kelamin,alamat_tinggal,kode_desa,kode_kelompok,no_telp,keterangan FROM m_guru WHERE kode_guru = :kode_guru"),array('kode_guru' => $kode_guru));
		$kelompoks = DB::select( DB::raw("SELECT kode_kelompok,nama_kelompok FROM m_kelompok WHERE status = 1 AND kode_desa = :kode_desa ORDER BY nama_kelompok"),array('kode_desa' => $datas[0]->kode_desa));
	    return view('guru.edit',compact('datas','desas','kelompoks'));
	}

	public function update(Request $request,$kode_guru){
		$nama_guru = $request->input('nama_guru');
		$jenis_kelamin= $request->input('jenis_kelamin');
		$alamat_tinggal= $request->input('alamat_tinggal');
		$no_telp2 = explode("_", $request->input('no_telp'));
		$no_telp = $no_telp2[0];
		$kode_desa= $request->input('kode_desa');
		$kode_kelompok= $request->input('kode_kelompok');
		$keterangan= $request->input('keterangan');
		$user_change = auth()->user()->getProfile->name;
		$changed_at= \Carbon\Carbon::Now('Asia/Jakarta');

		$datas = DB::select( DB::raw("UPDATE `m_guru` SET nama_guru = :nama_guru, jenis_kelamin = :jenis_kelamin, alamat_tinggal = :alamat_tinggal, no_telp = :no_telp, kode_desa = :kode_desa, kode_kelompok = :kode_kelompok, keterangan = :keterangan, user_change = :user_change, user_date_change = :changed_at WHERE kode_guru = :kode_guru"),array('kode_guru' => $kode_guru, 'nama_guru' => $nama_guru, 'jenis_kelamin' => $jenis_kelamin, 'alamat_tinggal' => $alamat_tinggal, 'no_telp' => $no_telp, 'kode_desa' => $kode_desa, 'kode_kelompok' => $kode_kelompok, 'keterangan' => $keterangan, 'user_change' => $user_change, 'changed_at' => $changed_at,));

		return redirect('guru');
	}

	public function destroy($kode_guru){
		$user_change = auth()->user()->getProfile->name;
		$changed_at= \Carbon\Carbon::Now('Asia/Jakarta');
		$datas = DB::select( DB::raw("UPDATE `m_guru` SET status = 0, user_change = :user_change, user_date_change = :changed_at WHERE kode_guru = :kode_guru"),array('kode_guru' => $kode_guru, 'user_change' => $user_change, 'changed_at' => $changed_at,));

		return redirect('guru');
	}
}
