<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Notifikasi;
use DB;
use PDF; // pdf namespace
use Excel; // Excel namespace

class AbsenController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(Request $req)
	{
	    $datas = DB::select( DB::raw("SELECT a.*,b.nama_panggilan FROM t_absensi a JOIN m_murid b ON a.kode_murid = b.kode_murid"));
	    return view('absensi.index',compact('datas'));
	}

	public function create(){
		$murids = DB::select( DB::raw("SELECT kode_murid,nama_lengkap FROM m_murid WHERE status = 1 ORDER BY nama_lengkap"));
		return view('absensi.add',compact('murids'));
	}

	public function store(Request $request){
		$tahun = $request->input('tahun');
		$kode_murid = $request->input('kode_murid');
		
		$jan_m1 = $request->input('jan_minggu1');
		$jan_m2 = $request->input('jan_minggu2');
		$jan_m3 = $request->input('jan_minggu3');
		$jan_m4 = $request->input('jan_minggu4');
		$jan_m5 = $request->input('jan_minggu5');
		$januari = $jan_m1.$jan_m2.$jan_m3.$jan_m4.$jan_m5;
		$feb_m1 = $request->input('feb_minggu1');
		$feb_m2 = $request->input('feb_minggu2');
		$feb_m3 = $request->input('feb_minggu3');
		$feb_m4 = $request->input('feb_minggu4');
		$feb_m5 = $request->input('feb_minggu5');
		$februari = $feb_m1.$feb_m2.$feb_m3.$feb_m4.$feb_m5;
		$mar_m1 = $request->input('mar_minggu1');
		$mar_m2 = $request->input('mar_minggu2');
		$mar_m3 = $request->input('mar_minggu3');
		$mar_m4 = $request->input('mar_minggu4');
		$mar_m5 = $request->input('mar_minggu5');
		$maret = $mar_m1.$mar_m2.$mar_m3.$mar_m4.$mar_m5;
		$apr_m1 = $request->input('apr_minggu1');
		$apr_m2 = $request->input('apr_minggu2');
		$apr_m3 = $request->input('apr_minggu3');
		$apr_m4 = $request->input('apr_minggu4');
		$apr_m5 = $request->input('apr_minggu5');
		$april = $apr_m1.$apr_m2.$apr_m3.$apr_m4.$apr_m5;
		$mei_m1 = $request->input('mei_minggu1');
		$mei_m2 = $request->input('mei_minggu2');
		$mei_m3 = $request->input('mei_minggu3');
		$mei_m4 = $request->input('mei_minggu4');
		$mei_m5 = $request->input('mei_minggu5');
		$mei = $mei_m1.$mei_m2.$mei_m3.$mei_m4.$mei_m5;
		$jun_m1 = $request->input('jun_minggu1');
		$jun_m2 = $request->input('jun_minggu2');
		$jun_m3 = $request->input('jun_minggu3');
		$jun_m4 = $request->input('jun_minggu4');
		$jun_m5 = $request->input('jun_minggu5');
		$juni = $jun_m1.$jun_m2.$jun_m3.$jun_m4.$jun_m5;
		$jul_m1 = $request->input('jul_minggu1');
		$jul_m2 = $request->input('jul_minggu2');
		$jul_m3 = $request->input('jul_minggu3');
		$jul_m4 = $request->input('jul_minggu4');
		$jul_m5 = $request->input('jul_minggu5');
		$juli = $jul_m1.$jul_m2.$jul_m3.$jul_m4.$jul_m5;
		$agu_m1 = $request->input('agu_minggu1');
		$agu_m2 = $request->input('agu_minggu2');
		$agu_m3 = $request->input('agu_minggu3');
		$agu_m4 = $request->input('agu_minggu4');
		$agu_m5 = $request->input('agu_minggu5');
		$agustus = $agu_m1.$agu_m2.$agu_m3.$agu_m4.$agu_m5;
		$sep_m1 = $request->input('sep_minggu1');
		$sep_m2 = $request->input('sep_minggu2');
		$sep_m3 = $request->input('sep_minggu3');
		$sep_m4 = $request->input('sep_minggu4');
		$sep_m5 = $request->input('sep_minggu5');
		$september = $sep_m1.$sep_m2.$sep_m3.$sep_m4.$sep_m5;
		$okt_m1 = $request->input('okt_minggu1');
		$okt_m2 = $request->input('okt_minggu2');
		$okt_m3 = $request->input('okt_minggu3');
		$okt_m4 = $request->input('okt_minggu4');
		$okt_m5 = $request->input('okt_minggu5');
		$oktober = $okt_m1.$okt_m2.$okt_m3.$okt_m4.$okt_m5;
		$nov_m1 = $request->input('nov_minggu1');
		$nov_m2 = $request->input('nov_minggu2');
		$nov_m3 = $request->input('nov_minggu3');
		$nov_m4 = $request->input('nov_minggu4');
		$nov_m5 = $request->input('nov_minggu5');
		$november = $nov_m1.$nov_m2.$nov_m3.$nov_m4.$nov_m5;
		$des_m1 = $request->input('des_minggu1');
		$des_m2 = $request->input('des_minggu2');
		$des_m3 = $request->input('des_minggu3');
		$des_m4 = $request->input('des_minggu4');
		$des_m5 = $request->input('des_minggu5');
		$desember = $des_m1.$des_m2.$des_m3.$des_m4.$des_m5;

		$user_create = auth()->user()->getProfile->name;
		$created_at= \Carbon\Carbon::Now('Asia/Jakarta');

		$datas = DB::select( DB::raw("INSERT INTO `t_absensi` (`tahun`, `kode_murid`, `januari`, `februari`, `maret`, `april`, `mei`, `juni`, `juli`, `agustus`, `september`, `oktober`, `november`, `desember`,`user_create`, `user_date_create`) VALUES (:tahun,:kode_murid,:januari,:februari,:maret,:april,:mei,:juni,:juli,:agustus,:september,:oktober,:november,:desember,:user_create,:created_at)"),array('tahun' => $tahun, 'kode_murid' => $kode_murid, 'januari' => $januari, 'februari' => $februari, 'maret' => $maret, 'april' => $april, 'mei' => $mei, 'juni' => $juni, 'juli' => $juli, 'agustus' => $agustus, 'september' => $september, 'oktober' => $oktober, 'november' => $november, 'desember' => $desember, 'user_create' => $user_create, 'created_at' => $created_at,));

		return redirect('absensi');
	}

	public function edit($kode_absen,$kode_murid,$tahun){
		$murids = DB::select( DB::raw("SELECT m_murid.kode_murid,nama_lengkap FROM m_murid JOIN t_absensi ON m_murid.kode_murid = t_absensi.kode_murid WHERE kode_absen = :kode_absen AND t_absensi.kode_murid = :kode_murid AND tahun = :tahun"),array('kode_absen' => $kode_absen,'kode_murid' => $kode_murid,'tahun' => $tahun));
		$datas = DB::select( DB::raw("SELECT * FROM t_absensi WHERE kode_absen = :kode_absen AND kode_murid = :kode_murid AND tahun = :tahun"),array('kode_absen' => $kode_absen,'kode_murid' => $kode_murid,'tahun' => $tahun));
	    return view('absensi.edit',compact('datas','murids'));
	}

	public function update(Request $request,$kode_absen,$kode_murid,$tahun){
		$jan_m1 = $request->input('jan_minggu1');
		$jan_m2 = $request->input('jan_minggu2');
		$jan_m3 = $request->input('jan_minggu3');
		$jan_m4 = $request->input('jan_minggu4');
		$jan_m5 = $request->input('jan_minggu5');
		$januari = $jan_m1.$jan_m2.$jan_m3.$jan_m4.$jan_m5;
		$feb_m1 = $request->input('feb_minggu1');
		$feb_m2 = $request->input('feb_minggu2');
		$feb_m3 = $request->input('feb_minggu3');
		$feb_m4 = $request->input('feb_minggu4');
		$feb_m5 = $request->input('feb_minggu5');
		$februari = $feb_m1.$feb_m2.$feb_m3.$feb_m4.$feb_m5;
		$mar_m1 = $request->input('mar_minggu1');
		$mar_m2 = $request->input('mar_minggu2');
		$mar_m3 = $request->input('mar_minggu3');
		$mar_m4 = $request->input('mar_minggu4');
		$mar_m5 = $request->input('mar_minggu5');
		$maret = $mar_m1.$mar_m2.$mar_m3.$mar_m4.$mar_m5;
		$apr_m1 = $request->input('apr_minggu1');
		$apr_m2 = $request->input('apr_minggu2');
		$apr_m3 = $request->input('apr_minggu3');
		$apr_m4 = $request->input('apr_minggu4');
		$apr_m5 = $request->input('apr_minggu5');
		$april = $apr_m1.$apr_m2.$apr_m3.$apr_m4.$apr_m5;
		$mei_m1 = $request->input('mei_minggu1');
		$mei_m2 = $request->input('mei_minggu2');
		$mei_m3 = $request->input('mei_minggu3');
		$mei_m4 = $request->input('mei_minggu4');
		$mei_m5 = $request->input('mei_minggu5');
		$mei = $mei_m1.$mei_m2.$mei_m3.$mei_m4.$mei_m5;
		$jun_m1 = $request->input('jun_minggu1');
		$jun_m2 = $request->input('jun_minggu2');
		$jun_m3 = $request->input('jun_minggu3');
		$jun_m4 = $request->input('jun_minggu4');
		$jun_m5 = $request->input('jun_minggu5');
		$juni = $jun_m1.$jun_m2.$jun_m3.$jun_m4.$jun_m5;
		$jul_m1 = $request->input('jul_minggu1');
		$jul_m2 = $request->input('jul_minggu2');
		$jul_m3 = $request->input('jul_minggu3');
		$jul_m4 = $request->input('jul_minggu4');
		$jul_m5 = $request->input('jul_minggu5');
		$juli = $jul_m1.$jul_m2.$jul_m3.$jul_m4.$jul_m5;
		$agu_m1 = $request->input('agu_minggu1');
		$agu_m2 = $request->input('agu_minggu2');
		$agu_m3 = $request->input('agu_minggu3');
		$agu_m4 = $request->input('agu_minggu4');
		$agu_m5 = $request->input('agu_minggu5');
		$agustus = $agu_m1.$agu_m2.$agu_m3.$agu_m4.$agu_m5;
		$sep_m1 = $request->input('sep_minggu1');
		$sep_m2 = $request->input('sep_minggu2');
		$sep_m3 = $request->input('sep_minggu3');
		$sep_m4 = $request->input('sep_minggu4');
		$sep_m5 = $request->input('sep_minggu5');
		$september = $sep_m1.$sep_m2.$sep_m3.$sep_m4.$sep_m5;
		$okt_m1 = $request->input('okt_minggu1');
		$okt_m2 = $request->input('okt_minggu2');
		$okt_m3 = $request->input('okt_minggu3');
		$okt_m4 = $request->input('okt_minggu4');
		$okt_m5 = $request->input('okt_minggu5');
		$oktober = $okt_m1.$okt_m2.$okt_m3.$okt_m4.$okt_m5;
		$nov_m1 = $request->input('nov_minggu1');
		$nov_m2 = $request->input('nov_minggu2');
		$nov_m3 = $request->input('nov_minggu3');
		$nov_m4 = $request->input('nov_minggu4');
		$nov_m5 = $request->input('nov_minggu5');
		$november = $nov_m1.$nov_m2.$nov_m3.$nov_m4.$nov_m5;
		$des_m1 = $request->input('des_minggu1');
		$des_m2 = $request->input('des_minggu2');
		$des_m3 = $request->input('des_minggu3');
		$des_m4 = $request->input('des_minggu4');
		$des_m5 = $request->input('des_minggu5');
		$desember = $des_m1.$des_m2.$des_m3.$des_m4.$des_m5;

		$user_change = auth()->user()->getProfile->name;
		$changed_at= \Carbon\Carbon::Now('Asia/Jakarta');

		$datas = DB::select( DB::raw("UPDATE `t_absensi` SET januari = :januari, februari = :februari, maret = :maret, april = :april, mei = :mei, juni = :juni, juli = :juli, agustus = :agustus, september = :september, oktober = :oktober, november = :november, desember = :desember,user_change = :user_change, user_date_change = :changed_at WHERE kode_absen = :kode_absen AND kode_murid = :kode_murid AND tahun = :tahun"),array('tahun' => $tahun, 'kode_murid' => $kode_murid, 'kode_absen' => $kode_absen, 'januari' => $januari, 'februari' => $februari, 'maret' => $maret, 'april' => $april, 'mei' => $mei, 'juni' => $juni, 'juli' => $juli, 'agustus' => $agustus, 'september' => $september, 'oktober' => $oktober, 'november' => $november, 'desember' => $desember, 'user_change' => $user_change, 'changed_at' => $changed_at,));

		return redirect('absensi');
	}

	// public function destroy($kode_absen,$kode_murid,$tahun){
	// 	$user_change = auth()->user()->getProfile->name;
	// 	$changed_at= \Carbon\Carbon::Now('Asia/Jakarta');
	// 	$datas = DB::select( DB::raw("UPDATE `m_desa` SET status = 0, user_change = :user_change, user_date_change = :changed_at WHERE kode_desa = :kode_desa"),array('kode_desa' => $kode_desa, 'user_change' => $user_change, 'changed_at' => $changed_at,));

	// 	return redirect('absensi');
	// }
}
