<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use PDF; // pdf namespace
use Excel; // Excel namespace

use App\datatoPDF;
class PDFController extends Controller
{
    // show all data
    public function index(Request $req)
    {
      // show all data to index
      $blogs = datatoPDF::all();
      view()->share('blogs',$blogs);
      // if request has pdf
      if($req->has('downloadpdf')){
        $pdf = PDF::loadView('pdf')->setPaper('a4', 'landscape');
        return $pdf->download('pdf');
      }
      // if request has excel
      if($req->has('downloadexcel')){
        Excel::create('spc', function($excel) use ($blogs) {
          $excel->sheet('Sheet 1', function($sheet) use ($blogs) {
            $sheet->fromArray($blogs);
          });
        })->export('xls');
      }
      // return index page
      return view('index');
    }
}