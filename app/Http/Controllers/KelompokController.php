<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Kelompok;
use App\Notifikasi;
use DB;
use PDF; // pdf namespace
use Excel; // Excel namespace

class KelompokController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    public function getkelompok(Request $request){
        $kode_desa = $request->Input('kode_desa');
        $data = DB::select( DB::raw("SELECT kode_kelompok,nama_kelompok FROM m_kelompok WHERE kode_desa = :kode_desa ORDER BY nama_kelompok"),array('kode_desa' => $kode_desa));
    	return response()->json($data);
    }

	public function index(Request $req)
	{
	    $datas = DB::select( DB::raw("SELECT m_desa.kode_desa,nama_desa,kode_kelompok,nama_kelompok,kyai_kelompok,m_kelompok.no_telp,m_kelompok.keterangan FROM m_kelompok,m_desa WHERE m_kelompok.status = 1 AND m_desa.kode_desa = m_kelompok.kode_desa ORDER BY nama_kelompok"));
	    return view('kelompok.index',compact('datas'));
	}

	public function create(){
		$datas = DB::select( DB::raw("SELECT kode_desa,nama_desa FROM m_desa WHERE status = 1"));
		return view('kelompok.add',compact('datas'));
	}

	public function store(Request $request){
		$kode_desa = $request->input('kode_desa');
		$nama_kelompok = $request->input('nama_kelompok');
		$kyai_kelompok= $request->input('kyai_kelompok');
		$no_telp2 = explode("_", $request->input('no_telp'));
		$no_telp = $no_telp2[0];
		$keterangan= $request->input('keterangan');
		$user_create = auth()->user()->getProfile->name;
		$created_at= \Carbon\Carbon::Now('Asia/Jakarta');

		$datas = DB::select( DB::raw("INSERT INTO `m_kelompok` (`kode_desa`, `nama_kelompok`, `kyai_kelompok`, `no_telp`, `keterangan`,`user_create`, `user_date_create`) VALUES (:kode_desa,:nama_kelompok,:kyai_kelompok,:no_telp,:keterangan,:user_create,:created_at)"),array('kode_desa' => $kode_desa,'nama_kelompok' => $nama_kelompok, 'kyai_kelompok' => $kyai_kelompok, 'keterangan' => $keterangan, 'no_telp' => $no_telp, 'user_create' => $user_create, 'created_at' => $created_at,));

		return redirect('kelompok');
	}

	public function edit($kode_desa,$kode_kelompok){
		$desas = DB::select( DB::raw("SELECT kode_desa,nama_desa FROM m_desa WHERE status = 1"));
		$datas = DB::select( DB::raw("SELECT kode_desa,kode_kelompok,nama_kelompok,kyai_kelompok,no_telp,keterangan FROM m_kelompok WHERE kode_desa = :kode_desa AND kode_kelompok = :kode_kelompok"),array('kode_desa' => $kode_desa,'kode_kelompok' => $kode_kelompok));
	    return view('kelompok.edit',compact('datas','desas'));
	}

	public function update(Request $request, $kode_desa, $kode_kelompok){
		$kode_desa_baru = $request->input('kode_desa');
		$nama_kelompok = $request->input('nama_kelompok');
		$kyai_kelompok = $request->input('kyai_kelompok');
		$no_telp2 = explode("_", $request->input('no_telp'));
		$no_telp = $no_telp2[0];
		$keterangan= $request->input('keterangan');
		$user_change = auth()->user()->getProfile->name;
		$changed_at= \Carbon\Carbon::Now('Asia/Jakarta');

		$datas = DB::select( DB::raw("UPDATE `m_kelompok` SET kode_desa = :kode_desa_baru, nama_kelompok = :nama_kelompok, kyai_kelompok = :kyai_kelompok, no_telp = :no_telp, keterangan = :keterangan, user_change = :user_change, user_date_change = :changed_at WHERE kode_desa = :kode_desa AND kode_kelompok = :kode_kelompok"),array('kode_desa' => $kode_desa, 'kode_desa_baru' => $kode_desa_baru, 'kode_kelompok' => $kode_kelompok, 'nama_kelompok' => $nama_kelompok, 'kyai_kelompok' => $kyai_kelompok, 'no_telp' => $no_telp, 'keterangan' => $keterangan, 'user_change' => $user_change, 'changed_at' => $changed_at,));

		return redirect('kelompok');
	}

	public function destroy($kode_desa,$kode_kelompok){
		$user_change = auth()->user()->getProfile->name;
		$changed_at= \Carbon\Carbon::Now('Asia/Jakarta');
		$datas = DB::select( DB::raw("UPDATE `m_kelompok` SET status = 0, user_change = :user_change, user_date_change = :changed_at WHERE kode_desa = :kode_desa AND kode_kelompok = :kode_kelompok"),array('kode_desa' => $kode_desa, 'kode_kelompok' => $kode_kelompok, 'user_change' => $user_change, 'changed_at' => $changed_at,));

		return redirect('kelompok');
	}
}
