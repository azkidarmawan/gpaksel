<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Notifikasi;
use App\Murid;
use DB;
use PDF; // pdf namespace
use Excel; // Excel namespace

class MuridController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
   
	public function getimg(Request $request){
        $kode_murid = $request->Input('kode_murid');
        $data = DB::select( DB::raw("SELECT m_murid.*,b.nama_kelompok,c.nama_desa FROM m_murid JOIN m_kelompok b ON m_murid.kode_desa = b.kode_desa AND m_murid.kode_kelompok = b.kode_kelompok JOIN m_desa c ON b.kode_desa = c.kode_desa  WHERE kode_murid = :kode_murid"),array('kode_murid' => $kode_murid));
    	return response()->json($data);
    }

	public function index(Request $req)
	{
	    $datas = DB::select( DB::raw("SELECT a.*,b.nama_kelompok,c.nama_desa FROM m_murid a JOIN m_kelompok b ON a.kode_desa = b.kode_desa AND a.kode_kelompok = b.kode_kelompok JOIN m_desa c ON b.kode_desa = c.kode_desa WHERE a.status = 1"));
	    return view('murid.index',compact('datas'));
	}

	public function create(){
		$datas = DB::select( DB::raw("SELECT kode_desa,nama_desa FROM m_desa WHERE status = 1"));
		return view('murid.add',compact('datas'));
	}

	public function store(Request $request){
		$target_dir = "uploads/foto/";
        $target_file = $target_dir . basename($_FILES["foto"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $filename = $request->input('nama_lengkap').'-'.$request->input('nama_panggilan');
        $target =  $target_dir . $filename . '.jpg';
        if($request['foto']){
            // Check if file already exists
            // if (file_exists($target)) {
            //     echo "File is already exists.";
            //     $uploadOk = 0;
            // }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "JPG") {
                return redirect()->back()->with('error','Maaf, tipe file yang diperbolehkan hanya .jpg');
                $uploadOk = 0;
            }
            // Check image size
            $image_info = getimagesize($_FILES["foto"]["tmp_name"]);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            if($image_width > 3000 || $image_height > 4000){
                return redirect()->back()->with('error','Maaf ukuran file terlalu besar.');
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target)) {
                    $nama_lengkap = $request->input('nama_lengkap');
					$nama_panggilan = $request->input('nama_panggilan');
					$kelas = $request->input('kelas');
					$jenis_kelamin= $request->input('jenis_kelamin');
					$tempat_lahir = $request->input('tempat_lahir');
					$tanggal_lahir = date('Y-m-d', strtotime($request->input('tanggal_lahir')));
					$kode_desa= $request->input('kode_desa');
					$kode_kelompok= $request->input('kode_kelompok');
					$alamat_tinggal= $request->input('alamat_tinggal');
					$no_hp_murid2 = explode("_", $request->input('no_hp_murid'));
					$no_hp_murid = $no_hp_murid2[0];
					$nama_wali= $request->input('nama_wali');
					$no_telp_wali2 = explode("_", $request->input('no_telp_wali'));
					$no_telp_wali = $no_telp_wali2[0];
					$tahun_masuk= $request->input('tahun_masuk');
					$keterangan= $request->input('keterangan');
					$foto = $filename;
					$user_create = auth()->user()->getProfile->name;
					$created_at= \Carbon\Carbon::Now('Asia/Jakarta');

					$datas = DB::select( DB::raw("INSERT INTO `m_murid` (`nama_lengkap`, `nama_panggilan`, `kelas`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `alamat_tinggal`,`no_hp_murid`, `nama_wali`, `no_telp_wali`, `tahun_masuk`,`keterangan`,`foto`,`kode_desa`,`kode_kelompok`,`user_create`, `user_date_create`) VALUES (:nama_lengkap,:nama_panggilan,:kelas,:jenis_kelamin,:tempat_lahir,:tanggal_lahir,:alamat_tinggal,:no_hp_murid,:nama_wali,:no_telp_wali,:tahun_masuk,:keterangan,:foto,:kode_desa,:kode_kelompok,:user_create,:created_at)"),array('nama_lengkap' => $nama_lengkap, 'nama_panggilan' => $nama_panggilan, 'kelas' => $kelas, 'jenis_kelamin' => $jenis_kelamin, 'tempat_lahir' => $tempat_lahir, 'tanggal_lahir' => $tanggal_lahir, 'alamat_tinggal' => $alamat_tinggal, 'no_hp_murid' => $no_hp_murid, 'nama_wali' => $nama_wali, 'no_telp_wali' => $no_telp_wali, 'tahun_masuk' => $tahun_masuk, 'keterangan' => $keterangan, 'foto' => $foto, 'kode_desa' => $kode_desa, 'kode_kelompok' => $kode_kelompok, 'user_create' => $user_create, 'created_at' => $created_at,));

					return redirect('murid');
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
        } else {
            $nama_lengkap = $request->input('nama_lengkap');
			$nama_panggilan = $request->input('nama_panggilan');
			$kelas = $request->input('kelas');
			$jenis_kelamin= $request->input('jenis_kelamin');
			$tempat_lahir = $request->input('tempat_lahir');
			$tanggal_lahir = date('Y-m-d', strtotime($request->input('tanggal_lahir')));
			$kode_desa= $request->input('kode_desa');
			$kode_kelompok= $request->input('kode_kelompok');
			$alamat_tinggal= $request->input('alamat_tinggal');
			$no_hp_murid2 = explode("_", $request->input('no_hp_murid'));
			$no_hp_murid = $no_hp_murid2[0];
			$nama_wali= $request->input('nama_wali');
			$no_telp_wali2 = explode("_", $request->input('no_telp_wali'));
			$no_telp_wali = $no_telp_wali2[0];
			$tahun_masuk= $request->input('tahun_masuk');
			$keterangan= $request->input('keterangan');
			$user_create = auth()->user()->getProfile->name;
			$created_at= \Carbon\Carbon::Now('Asia/Jakarta');

			$datas = DB::select( DB::raw("INSERT INTO `m_murid` (`nama_lengkap`, `nama_panggilan`, `kelas`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `alamat_tinggal`,`no_hp_murid`, `nama_wali`, `no_telp_wali`, `tahun_masuk`,`keterangan`,`kode_desa`,`kode_kelompok`,`user_create`, `user_date_create`) VALUES (:nama_lengkap,:nama_panggilan,:kelas,:jenis_kelamin,:tempat_lahir,:tanggal_lahir,:alamat_tinggal,:no_hp_murid,:nama_wali,:no_telp_wali,:tahun_masuk,:keterangan,:kode_desa,:kode_kelompok,:user_create,:created_at)"),array('nama_lengkap' => $nama_lengkap, 'nama_panggilan' => $nama_panggilan, 'kelas' => $kelas, 'jenis_kelamin' => $jenis_kelamin, 'tempat_lahir' => $tempat_lahir, 'tanggal_lahir' => $tanggal_lahir, 'alamat_tinggal' => $alamat_tinggal, 'no_hp_murid' => $no_hp_murid, 'nama_wali' => $nama_wali, 'no_telp_wali' => $no_telp_wali, 'tahun_masuk' => $tahun_masuk, 'keterangan' => $keterangan, 'kode_desa' => $kode_desa, 'kode_kelompok' => $kode_kelompok, 'user_create' => $user_create, 'created_at' => $created_at,));

			return redirect('murid');
        }
	}

	public function edit($kode_murid){
		$desas = DB::select( DB::raw("SELECT kode_desa,nama_desa FROM m_desa WHERE status = 1"));
		$datas = DB::select( DB::raw("SELECT * FROM m_murid WHERE kode_murid = :kode_murid"),array('kode_murid' => $kode_murid));
		$kelompoks = DB::select( DB::raw("SELECT kode_kelompok,nama_kelompok FROM m_kelompok WHERE status = 1 AND kode_desa = :kode_desa ORDER BY nama_kelompok"),array('kode_desa' => $datas[0]->kode_desa));
	    return view('murid.edit',compact('datas','desas','kelompoks'));
	}

	public function update(Request $request,$kode_murid){
		$target_dir = "uploads/foto/";
        $target_file = $target_dir . basename($_FILES["foto"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $filename = $request->input('nama_lengkap').'-'.$request->input('nama_panggilan');
        $target =  $target_dir . $filename . '.jpg';
        if($request['foto']){
            // Check if file already exists
            // if (file_exists($target)) {
            //     echo "File is already exists.";
            //     $uploadOk = 0;
            // }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "JPG") {
                return redirect()->back()->with('error','Maaf, tipe file yang diperbolehkan hanya .jpg');
                $uploadOk = 0;
            }
            // Check image size
            $image_info = getimagesize($_FILES["foto"]["tmp_name"]);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            if($image_width > 3000 || $image_height > 4000){
                return redirect()->back()->with('error','Maaf ukuran file terlalu besar.');
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target)) {
                    $nama_lengkap = $request->input('nama_lengkap');
					$nama_panggilan = $request->input('nama_panggilan');
					$kelas = $request->input('kelas');
					$jenis_kelamin= $request->input('jenis_kelamin');
					$tempat_lahir = $request->input('tempat_lahir');
					$tanggal_lahir = date('Y-m-d', strtotime($request->input('tanggal_lahir')));
					$kode_desa= $request->input('kode_desa');
					$kode_kelompok= $request->input('kode_kelompok');
					$alamat_tinggal= $request->input('alamat_tinggal');
					$no_hp_murid2 = explode("_", $request->input('no_hp_murid'));
					$no_hp_murid = $no_hp_murid2[0];
					$nama_wali= $request->input('nama_wali');
					$no_telp_wali2 = explode("_", $request->input('no_telp_wali'));
					$no_telp_wali = $no_telp_wali2[0];
					$tahun_masuk= $request->input('tahun_masuk');
					$keterangan= $request->input('keterangan');
					$foto = $filename;
					$user_change = auth()->user()->getProfile->name;
					$changed_at= \Carbon\Carbon::Now('Asia/Jakarta');

					$datas = DB::select( DB::raw("UPDATE `m_murid` SET `nama_lengkap`=:nama_lengkap, `nama_panggilan`=:nama_panggilan, `kelas`=:kelas, `jenis_kelamin`=:jenis_kelamin, `tempat_lahir`=:tempat_lahir, `tanggal_lahir`=:tanggal_lahir, `alamat_tinggal`=:alamat_tinggal,`no_hp_murid`=:no_hp_murid, `nama_wali`=:nama_wali, `no_telp_wali`=:no_telp_wali, `tahun_masuk`=:tahun_masuk,`keterangan`=:keterangan,`foto`=:foto,`kode_desa`=:kode_desa,`kode_kelompok`=:kode_kelompok,`user_change`=:user_change, `user_date_change`=:changed_at  WHERE kode_murid = :kode_murid"),array('kode_murid' => $kode_murid, 'nama_lengkap' => $nama_lengkap, 'nama_panggilan' => $nama_panggilan, 'kelas' => $kelas, 'jenis_kelamin' => $jenis_kelamin, 'tempat_lahir' => $tempat_lahir, 'tanggal_lahir' => $tanggal_lahir, 'alamat_tinggal' => $alamat_tinggal, 'no_hp_murid' => $no_hp_murid, 'nama_wali' => $nama_wali, 'no_telp_wali' => $no_telp_wali, 'tahun_masuk' => $tahun_masuk, 'keterangan' => $keterangan, 'foto' => $foto, 'kode_desa' => $kode_desa, 'kode_kelompok' => $kode_kelompok, 'user_change' => $user_change, 'changed_at' => $changed_at,));

					return redirect('murid');
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
        } else {
            $nama_lengkap = $request->input('nama_lengkap');
			$nama_panggilan = $request->input('nama_panggilan');
			$kelas = $request->input('kelas');
			$jenis_kelamin= $request->input('jenis_kelamin');
			$tempat_lahir = $request->input('tempat_lahir');
			$tanggal_lahir = date('Y-m-d', strtotime($request->input('tanggal_lahir')));
			$kode_desa= $request->input('kode_desa');
			$kode_kelompok= $request->input('kode_kelompok');
			$alamat_tinggal= $request->input('alamat_tinggal');
			$no_hp_murid2 = explode("_", $request->input('no_hp_murid'));
			$no_hp_murid = $no_hp_murid2[0];
			$nama_wali= $request->input('nama_wali');
			$no_telp_wali2 = explode("_", $request->input('no_telp_wali'));
			$no_telp_wali = $no_telp_wali2[0];
			$tahun_masuk= $request->input('tahun_masuk');
			$keterangan= $request->input('keterangan');
			$user_change = auth()->user()->getProfile->name;
			$changed_at= \Carbon\Carbon::Now('Asia/Jakarta');

			$datas = DB::select( DB::raw("UPDATE `m_murid` SET `nama_lengkap`=:nama_lengkap, `nama_panggilan`=:nama_panggilan, `kelas`=:kelas, `jenis_kelamin`=:jenis_kelamin, `tempat_lahir`=:tempat_lahir, `tanggal_lahir`=:tanggal_lahir, `alamat_tinggal`=:alamat_tinggal,`no_hp_murid`=:no_hp_murid, `nama_wali`=:nama_wali, `no_telp_wali`=:no_telp_wali, `tahun_masuk`=:tahun_masuk,`keterangan`=:keterangan,`kode_desa`=:kode_desa,`kode_kelompok`=:kode_kelompok,`user_change`=:user_change, `user_date_change`=:changed_at  WHERE kode_murid = :kode_murid"),array('kode_murid' => $kode_murid, 'nama_lengkap' => $nama_lengkap, 'nama_panggilan' => $nama_panggilan, 'kelas' => $kelas, 'jenis_kelamin' => $jenis_kelamin, 'tempat_lahir' => $tempat_lahir, 'tanggal_lahir' => $tanggal_lahir, 'alamat_tinggal' => $alamat_tinggal, 'no_hp_murid' => $no_hp_murid, 'nama_wali' => $nama_wali, 'no_telp_wali' => $no_telp_wali, 'tahun_masuk' => $tahun_masuk, 'keterangan' => $keterangan, 'kode_desa' => $kode_desa, 'kode_kelompok' => $kode_kelompok, 'user_change' => $user_change, 'changed_at' => $changed_at,));

			return redirect('murid');
        }
	}

	public function destroy($kode_murid){
		$user_change = auth()->user()->getProfile->name;
		$changed_at= \Carbon\Carbon::Now('Asia/Jakarta');
		$datas = DB::select( DB::raw("UPDATE `m_murid` SET status = 0, user_change = :user_change, user_date_change = :changed_at WHERE kode_murid = :kode_murid"),array('kode_murid' => $kode_murid, 'user_change' => $user_change, 'changed_at' => $changed_at,));

		return redirect('murid');
	}
}
