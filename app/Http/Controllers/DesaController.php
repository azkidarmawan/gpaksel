<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Desa;
use App\Notifikasi;
use DB;
use PDF; // pdf namespace
use Excel; // Excel namespace

class DesaController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(Request $req)
	{
	    $datas = DB::select( DB::raw("SELECT kode_desa,nama_desa,kyai_desa,no_telp,keterangan FROM m_desa WHERE status = 1"));
	    return view('desa.index',compact('datas'));
	}

	public function create(){
		return view('desa.add');
	}

	public function store(Request $request){
		$nama_desa = $request->input('nama_desa');
		$kyai_desa= $request->input('kyai_desa');
		$no_telp2 = explode("_", $request->input('no_telp'));
		$no_telp = $no_telp2[0];
		$keterangan= $request->input('keterangan');
		$user_create = auth()->user()->getProfile->name;
		$created_at= \Carbon\Carbon::Now('Asia/Jakarta');

		$datas = DB::select( DB::raw("INSERT INTO `m_desa` (`nama_desa`, `kyai_desa`, `no_telp`, `keterangan`,`user_create`, `user_date_create`) VALUES (:nama_desa,:kyai_desa,:no_telp,:keterangan,:user_create,:created_at)"),array('nama_desa' => $nama_desa, 'kyai_desa' => $kyai_desa, 'no_telp' => $no_telp, 'keterangan' => $keterangan, 'user_create' => $user_create, 'created_at' => $created_at,));

		return redirect('desa');
	}

	public function edit($kode_desa){
		$datas = DB::select( DB::raw("SELECT kode_desa,nama_desa,kyai_desa,no_telp,keterangan FROM m_desa WHERE kode_desa = :kode_desa"),array('kode_desa' => $kode_desa));
	    return view('desa.edit',compact('datas'));
	}

	public function update(Request $request,$kode_desa){
		$nama_desa = $request->input('nama_desa');
		$kyai_desa= $request->input('kyai_desa');
		$no_telp2 = explode("_", $request->input('no_telp'));
		$no_telp = $no_telp2[0];
		$keterangan= $request->input('keterangan');
		$user_change = auth()->user()->getProfile->name;
		$changed_at= \Carbon\Carbon::Now('Asia/Jakarta');

		$datas = DB::select( DB::raw("UPDATE `m_desa` SET nama_desa = :nama_desa, kyai_desa = :kyai_desa, no_telp = :no_telp, keterangan = :keterangan, user_change = :user_change, user_date_change = :changed_at WHERE kode_desa = :kode_desa"),array('kode_desa' => $kode_desa, 'nama_desa' => $nama_desa, 'kyai_desa' => $kyai_desa, 'no_telp' => $no_telp, 'keterangan' => $keterangan, 'user_change' => $user_change, 'changed_at' => $changed_at,));

		return redirect('desa');
	}

	public function destroy($kode_desa){
		$user_change = auth()->user()->getProfile->name;
		$changed_at= \Carbon\Carbon::Now('Asia/Jakarta');
		$datas = DB::select( DB::raw("UPDATE `m_desa` SET status = 0, user_change = :user_change, user_date_change = :changed_at WHERE kode_desa = :kode_desa"),array('kode_desa' => $kode_desa, 'user_change' => $user_change, 'changed_at' => $changed_at,));

		return redirect('desa');
	}
}
