<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Notifikasi;
use DB;
use PDF; // pdf namespace
use Excel; // Excel namespace
use Mail;

class AbsenGuruController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(Request $req)
	{
    	$datas = DB::select( DB::raw("SELECT a.tahun,a.bulan,a.minggu,s1.sesi1,s2.sesi2,s3.sesi3,s4.sesi4,s5.sesi5,s6.sesi6 FROM t_absen_guru a JOIN
		(SELECT t_absen_guru.tahun,t_absen_guru.bulan,t_absen_guru.minggu, IFNULL(nama_guru,'-') sesi1 FROM m_guru RIGHT JOIN t_absen_guru ON m_guru.kode_guru = t_absen_guru.sesi1) s1 ON a.tahun = s1.tahun AND a.bulan = s1.bulan AND a.minggu = s1.minggu JOIN
		(SELECT t_absen_guru.tahun,t_absen_guru.bulan,t_absen_guru.minggu, IFNULL(nama_guru,'-') sesi2 FROM m_guru RIGHT JOIN t_absen_guru ON m_guru.kode_guru = t_absen_guru.sesi2) s2 ON a.tahun = s2.tahun AND a.bulan = s2.bulan AND a.minggu = s2.minggu JOIN
		(SELECT t_absen_guru.tahun,t_absen_guru.bulan,t_absen_guru.minggu, IFNULL(nama_guru,'-') sesi3 FROM m_guru RIGHT JOIN t_absen_guru ON m_guru.kode_guru = t_absen_guru.sesi3) s3 ON a.tahun = s3.tahun AND a.bulan = s3.bulan AND a.minggu = s3.minggu JOIN
		(SELECT t_absen_guru.tahun,t_absen_guru.bulan,t_absen_guru.minggu, IFNULL(nama_guru,'-') sesi4 FROM m_guru RIGHT JOIN t_absen_guru ON m_guru.kode_guru = t_absen_guru.sesi4) s4 ON a.tahun = s4.tahun AND a.bulan = s4.bulan AND a.minggu = s4.minggu JOIN
		(SELECT t_absen_guru.tahun,t_absen_guru.bulan,t_absen_guru.minggu, IFNULL(nama_guru,'-') sesi5 FROM m_guru RIGHT JOIN t_absen_guru ON m_guru.kode_guru = t_absen_guru.sesi5) s5 ON a.tahun = s5.tahun AND a.bulan = s5.bulan AND a.minggu = s5.minggu JOIN
		(SELECT t_absen_guru.tahun,t_absen_guru.bulan,t_absen_guru.minggu, IFNULL(nama_guru,'-') sesi6 FROM m_guru RIGHT JOIN t_absen_guru ON m_guru.kode_guru = t_absen_guru.sesi6) s6 ON a.tahun = s6.tahun AND a.bulan = s6.bulan AND a.minggu = s6.minggu ORDER BY a.tahun,a.user_date_create DESC"));
	    return view('absen-guru.index',compact('datas'));
	}

	public function create(){
		$gurus = DB::select( DB::raw("SELECT kode_guru,nama_guru FROM m_guru WHERE status = 1 ORDER BY nama_guru"));

		// $emails = ['ipa4.23azki@gmail.com'];

		// Mail::send('email', [], function($message) use ($emails)
		// {    
		//     $message->to($emails)->subject('Rapot GP Aksel');
		// });

		return view('absen-guru.add',compact('gurus'));
	}

	public function store(Request $request){
		$tahun = $request->input('tahun');
		$bulan = $request->input('bulan');
		$minggu = $request->input('minggu');
		$check = DB::select( DB::raw("SELECT tahun,bulan,minggu FROM t_absen_guru WHERE tahun = :tahun AND bulan = :bulan AND minggu = :minggu"),array('tahun' => $tahun,'bulan' => $bulan,'minggu' => $minggu));
		if(count($check)){
			return redirect()->back()->with('error','Minggu ke-'.$check[0]->minggu.' Bulan '.$check[0]->bulan.' Tahun '.$check[0]->tahun.' sudah memiliki data absensi guru.');
		}
		$sesi1 = $request->input('sesi1');
		$sesi2 = $request->input('sesi2');
		$sesi3 = $request->input('sesi3');
		$sesi4 = $request->input('sesi4');
		$sesi5 = $request->input('sesi5');
		$sesi6 = $request->input('sesi6');
		$user_create = auth()->user()->getProfile->name;
		$created_at= \Carbon\Carbon::Now('Asia/Jakarta');

		$datas = DB::select( DB::raw("INSERT INTO `t_absen_guru` (`tahun`, `bulan`, `minggu`, `sesi1`, `sesi2`, `sesi3`, `sesi4`, `sesi5`, `sesi6`,`user_create`, `user_date_create`) VALUES
		(:tahun,:bulan,:minggu,:sesi1,:sesi2,:sesi3,:sesi4,:sesi5,:sesi6,:user_create,:created_at)"),array('tahun' => $tahun, 'bulan' => $bulan, 'minggu' => $minggu, 'sesi1' => $sesi1, 'sesi2' => $sesi2, 'sesi3' => $sesi3, 'sesi4' => $sesi4, 'sesi5' => $sesi5, 'sesi6' => $sesi6, 'user_create' => $user_create, 'created_at' => $created_at,));

		return redirect('absen-guru');
	}

	public function edit($tahun,$bulan,$minggu){
		$datas = DB::select( DB::raw("SELECT * FROM t_absen_guru WHERE tahun = :tahun AND bulan = :bulan AND minggu = :minggu"),array('tahun' => $tahun,'bulan' => $bulan,'minggu' => $minggu));
		$gurus = DB::select( DB::raw("SELECT kode_guru,nama_guru FROM m_guru WHERE status = 1 ORDER BY nama_guru"));
		return view('absen-guru.edit',compact('datas','gurus'));
	}

	public function update(Request $request,$tahun2,$bulan2,$minggu2){
		$tahun = $request->input('tahun');
		$bulan = $request->input('bulan');
		$minggu = $request->input('minggu');
		$check = DB::select( DB::raw("SELECT tahun,bulan,minggu FROM t_absen_guru WHERE tahun = :tahun AND bulan = :bulan AND minggu = :minggu AND (tahun != :tahun2 OR bulan != :bulan2 OR minggu != :minggu2)"),array('tahun' => $tahun,'bulan' => $bulan,'minggu' => $minggu,'tahun2' => $tahun2,'bulan2' => $bulan2,'minggu2' => $minggu2));
		if(count($check)){
			return redirect()->back()->with('error','Minggu ke-'.$check[0]->minggu.' Bulan '.$check[0]->bulan.' Tahun '.$check[0]->tahun.' sudah memiliki data absensi guru.');
		}
		$delete = DB::select( DB::raw("DELETE FROM `t_absen_guru` WHERE tahun = :tahun AND bulan = :bulan AND minggu = :minggu"),array('tahun' => $tahun2,'bulan' => $bulan2,'minggu' => $minggu2));
		$sesi1 = $request->input('sesi1');
		$sesi2 = $request->input('sesi2');
		$sesi3 = $request->input('sesi3');
		$sesi4 = $request->input('sesi4');
		$sesi5 = $request->input('sesi5');
		$sesi6 = $request->input('sesi6');
		$user_create = auth()->user()->getProfile->name;
		$created_at= \Carbon\Carbon::Now('Asia/Jakarta');

		$datas = DB::select( DB::raw("INSERT INTO `t_absen_guru` (`tahun`, `bulan`, `minggu`, `sesi1`, `sesi2`, `sesi3`, `sesi4`, `sesi5`, `sesi6`,`user_create`, `user_date_create`) VALUES
		(:tahun,:bulan,:minggu,:sesi1,:sesi2,:sesi3,:sesi4,:sesi5,:sesi6,:user_create,:created_at)"),array('tahun' => $tahun, 'bulan' => $bulan, 'minggu' => $minggu, 'sesi1' => $sesi1, 'sesi2' => $sesi2, 'sesi3' => $sesi3, 'sesi4' => $sesi4, 'sesi5' => $sesi5, 'sesi6' => $sesi6, 'user_create' => $user_create, 'created_at' => $created_at,));

		return redirect('absen-guru');
	}

	public function destroy($tahun,$bulan,$minggu){
		$delete = DB::select( DB::raw("DELETE FROM `t_absen_guru` WHERE tahun = :tahun AND bulan = :bulan AND minggu = :minggu"),array('tahun' => $tahun,'bulan' => $bulan,'minggu' => $minggu));

		return redirect('absen-guru');
	}
}
