<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Notifikasi;
use DB;
use PDF; // pdf namespace
use Excel; // Excel namespace

class PenilaianController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(Request $req)
	{
	    $datas = DB::select( DB::raw("SELECT a.kode_murid,a.nama_lengkap,b.nama_kelompok,c.nama_desa,d.wali_kelas,d.keterangan,d.semester,e.nama_guru FROM m_murid a JOIN m_kelompok b ON a.kode_desa = b.kode_desa AND a.kode_kelompok = b.kode_kelompok JOIN m_desa c ON b.kode_desa = c.kode_desa JOIN t_nilai_detail d ON d.kode_murid = a.kode_murid JOIN m_guru e ON e.kode_guru = d.wali_kelas GROUP BY a.kode_murid,a.nama_lengkap,b.nama_kelompok,c.nama_desa,d.wali_kelas,d.keterangan,d.semester,e.nama_guru ORDER BY a.nama_lengkap,d.semester"));
	    return view('penilaian.index',compact('datas'));
	}

	public function reset(){
		$delete = DB::select( DB::raw("DELETE FROM `m_doa`"));
		$delete = DB::select( DB::raw("DELETE FROM `m_dalil`"));
		$delete = DB::select( DB::raw("DELETE FROM `m_surat`"));
		$delete = DB::select( DB::raw("DELETE FROM `m_quran`"));
		$delete = DB::select( DB::raw("DELETE FROM `m_hadits`"));
		$delete = DB::select( DB::raw("DELETE FROM `m_tambahan`"));
		$delete = DB::select( DB::raw("DELETE FROM `m_akhlaq`"));
		return redirect('penilaian/add');
	}

	public function create(){
		$doa = DB::select(DB::raw("SELECT * FROM m_doa"));
		$dalil = DB::select(DB::raw("SELECT * FROM m_dalil"));
		$surat = DB::select(DB::raw("SELECT * FROM m_surat"));
		$quran = DB::select(DB::raw("SELECT * FROM m_quran"));
		$hadits = DB::select(DB::raw("SELECT * FROM m_hadits"));
		$tambahan = DB::select(DB::raw("SELECT * FROM m_tambahan"));
		$akhlaq = DB::select(DB::raw("SELECT * FROM m_akhlaq"));
		$gurus = DB::select( DB::raw("SELECT kode_guru,nama_guru FROM m_guru WHERE status = 1 ORDER BY nama_guru"));
		$murids = DB::select( DB::raw("SELECT kode_murid,nama_lengkap FROM m_murid WHERE status = 1 ORDER BY nama_lengkap"));
		return view('penilaian.add',compact('murids','gurus','doa','dalil','surat','quran','hadits','tambahan','akhlaq'));
	}

	public function store(Request $request){
		$delete = DB::select( DB::raw("DELETE FROM `m_doa`"));
		$delete = DB::select( DB::raw("DELETE FROM `m_dalil`"));
		$delete = DB::select( DB::raw("DELETE FROM `m_surat`"));
		$delete = DB::select( DB::raw("DELETE FROM `m_quran`"));
		$delete = DB::select( DB::raw("DELETE FROM `m_hadits`"));
		$delete = DB::select( DB::raw("DELETE FROM `m_tambahan`"));
		$delete = DB::select( DB::raw("DELETE FROM `m_akhlaq`"));
		$semester= $request->input('semester');
		$kode_murid= $request->input('kode_murid');
		$check_rapot = DB::select(DB::raw("SELECT kode_murid,semester FROM t_nilai_detail WHERE kode_murid = :kode_murid AND semester = :semester"),array('kode_murid' => $kode_murid, 'semester' => $semester));
		if(count($check_rapot)){
			return redirect()->back()->with('error','Murid tersebut sudah memiliki nilai untuk Semester '.$check_rapot[0]->semester.'.');
		}
		$wali_kelas= $request->input('kode_guru');
		$fashohah= $request->input('fashohah');
		$tajwid= $request->input('tajwid');
		$ghorib_muskhilat= $request->input('ghorib_muskhilat');
		$suara_lagu= $request->input('suara_lagu');
		$catatan= $request->input('catatan');
		$keterangan= $request->input('keterangan');
		$id_doa = (int)$request->input('id_doa')+1;
		$nilai_doa = 0;
		$prestasi_doa = '';
		$keterangan_doa = '';
		for($i = 0;$i < $id_doa;$i++){
			$nilai_doa+= $request->input('nilai_doa'.$i);
			if($request->input('doa'.$i) != ''){
				$prestasi_doa = $prestasi_doa.$request->input('doa'.$i).',';
				$keterangan_doa = $keterangan_doa.$request->input('status_doa'.$i).',';
				$m_doa = DB::select( DB::raw("INSERT INTO `m_doa`(`doa`) VALUES (:doa)"),array('doa' => $request->input('doa'.$i)));
			} else {
				$id_doa = $i;
				break;
			}
		}
		$nilai_doa = number_format((float)((int)$nilai_doa) / ((int)$id_doa), 2, '.', '');
		$id_dalil = (int)$request->input('id_dalil')+1;
		$nilai_dalil = 0;
		$prestasi_dalil = '';
		$keterangan_dalil = '';
		for($i = 0;$i < $id_dalil;$i++){
			$nilai_dalil+= $request->input('nilai_dalil'.$i);
			if($request->input('dalil'.$i) != ''){
				$prestasi_dalil = $prestasi_dalil.$request->input('dalil'.$i).'#'.$request->input('jml_dalil'.$i).',';
				$keterangan_dalil = $keterangan_dalil.$request->input('status_dalil'.$i).',';
				$m_dalil = DB::select( DB::raw("INSERT INTO `m_dalil`(`dalil`,`jumlah`) VALUES (:dalil,:jumlah)"),array('dalil' => $request->input('dalil'.$i), 'jumlah' => $request->input('jml_dalil'.$i)));
			} else {
				$id_dalil = $i;
				break;
			}
		}
		$nilai_dalil = number_format((float)((int)$nilai_dalil) / ((int)$id_dalil), 2, '.', '');
		$id_surat = (int)$request->input('id_surat')+1;
		$nilai_surat = 0;
		$prestasi_surat = '';
		$keterangan_surat = '';
		for($i = 0;$i < $id_surat;$i++){
			$nilai_surat+= $request->input('nilai_surat'.$i);
			if($request->input('surat'.$i) != ''){
				$prestasi_surat = $prestasi_surat.$request->input('surat'.$i).',';
				$keterangan_surat = $keterangan_surat.$request->input('status_surat'.$i).',';
				$m_surat = DB::select( DB::raw("INSERT INTO `m_surat`(`surat`) VALUES (:surat)"),array('surat' => $request->input('surat'.$i)));
			} else {
				$id_surat = $i;
				break;
			}
		}
		$nilai_surat = number_format((float)((int)$nilai_surat) / ((int)$id_surat), 2, '.', '');
		$id_quran = (int)$request->input('id_quran')+1;
		$nilai_quran = 0;
		$prestasi_quran = '';
		$keterangan_quran = '';
		for($i = 0;$i < $id_quran;$i++){
			$nilai_quran+= $request->input('nilai_quran'.$i);
			if($request->input('quran'.$i) != ''){
				$prestasi_quran = $prestasi_quran.$request->input('quran'.$i).',';
				$keterangan_quran = $keterangan_quran.$request->input('status_quran'.$i).',';
				$m_quran = DB::select( DB::raw("INSERT INTO `m_quran`(`quran`) VALUES (:quran)"),array('quran' => $request->input('quran'.$i)));
			} else {
				$id_quran = $i;
				break;
			}
		}
		$nilai_quran = number_format((float)((int)$nilai_quran) / ((int)$id_quran), 2, '.', '');
		$id_hadits = (int)$request->input('id_hadits')+1;
		$nilai_hadits = 0;
		$prestasi_hadits = '';
		$keterangan_hadits = '';
		for($i = 0;$i < $id_hadits;$i++){
			$nilai_hadits+= $request->input('nilai_hadits'.$i);
			if($request->input('hadits'.$i) != ''){
				$prestasi_hadits = $prestasi_hadits.$request->input('hadits'.$i).',';
				$keterangan_hadits = $keterangan_hadits.$request->input('status_hadits'.$i).',';
				$m_hadits = DB::select( DB::raw("INSERT INTO `m_hadits`(`hadits`) VALUES (:hadits)"),array('hadits' => $request->input('hadits'.$i)));
			} else {
				$id_hadits = $i;
				break;
			}
		}
		$nilai_hadits = number_format((float)((int)$nilai_hadits) / ((int)$id_hadits), 2, '.', '');
		$id_tambahan = (int)$request->input('id_tambahan')+1;
		$nilai_tambahan = 0;
		$prestasi_tambahan = '';
		$keterangan_tambahan = '';
		for($i = 0;$i < $id_tambahan;$i++){
			$nilai_tambahan+= $request->input('nilai_tambahan'.$i);
			if($request->input('tambahan'.$i) != ''){
				$prestasi_tambahan = $prestasi_tambahan.$request->input('tambahan'.$i).',';
				$keterangan_tambahan = $keterangan_tambahan.$request->input('status_tambahan'.$i).',';
				$m_tambahan = DB::select( DB::raw("INSERT INTO `m_tambahan`(`tambahan`) VALUES (:tambahan)"),array('tambahan' => $request->input('tambahan'.$i)));
			} else {
				$id_tambahan = $i;
				break;
			}
		}
		if($id_tambahan == 0)
			$id_tambahan++;
		$nilai_tambahan = number_format((float)((int)$nilai_tambahan) / ((int)$id_tambahan), 2, '.', '');
		$id_akhlaq = (int)$request->input('id_akhlaq')+1;
		$nilai_akhlaq = 0;
		$prestasi_akhlaq = '';
		$keterangan_akhlaq = '';
		for($i = 0;$i < $id_akhlaq;$i++){
			$nilai_akhlaq+= $request->input('nilai_akhlaq'.$i);
			if($request->input('akhlaq'.$i) != ''){
				$prestasi_akhlaq = $prestasi_akhlaq.$request->input('akhlaq'.$i).',';
				$keterangan_akhlaq = $keterangan_akhlaq.$request->input('status_akhlaq'.$i).',';
				$m_akhlaq = DB::select( DB::raw("INSERT INTO `m_akhlaq`(`akhlaq`) VALUES (:akhlaq)"),array('akhlaq' => $request->input('akhlaq'.$i)));
			} else {
				$id_akhlaq = $i;
				break;
			}
		}
		$nilai_akhlaq = number_format((float)((int)$nilai_akhlaq) / ((int)$id_akhlaq), 2, '.', '');
		$user_create = auth()->user()->getProfile->name;
		$created_at= \Carbon\Carbon::Now('Asia/Jakarta');

		$bacaan = DB::select( DB::raw("INSERT INTO `t_bacaan`(`fashohah`, `tajwid`, `ghorib_muskhilat`, `suara_lagu`, `user_create`, `user_date_create`) VALUES (:fashohah,:tajwid,:ghorib_muskhilat,:suara_lagu,:user_create,:created_at)"),array('fashohah' => $fashohah, 'tajwid' => $tajwid, 'ghorib_muskhilat' => $ghorib_muskhilat, 'suara_lagu' => $suara_lagu, 'user_create' => $user_create, 'created_at' => $created_at,));
		$hafalan = DB::select( DB::raw("INSERT INTO `t_hafalan`(`nilai_doa`, `nilai_dalil`, `nilai_surat`, `prestasi_doa`, `prestasi_dalil`, `prestasi_surat`, `keterangan_doa`, `keterangan_dalil`, `keterangan_surat`, `user_create`, `user_date_create`) VALUES (:nilai_doa,:nilai_dalil,:nilai_surat,:prestasi_doa,:prestasi_dalil,:prestasi_surat,:keterangan_doa,:keterangan_dalil,:keterangan_surat,:user_create,:created_at)"),array('nilai_doa' => $nilai_doa, 'nilai_dalil' => $nilai_dalil, 'nilai_surat' => $nilai_surat, 'prestasi_doa' => $prestasi_doa, 'prestasi_dalil' => $prestasi_dalil, 'prestasi_surat' => $prestasi_surat, 'keterangan_doa' => $keterangan_doa, 'keterangan_dalil' => $keterangan_dalil, 'keterangan_surat' => $keterangan_surat, 'user_create' => $user_create, 'created_at' => $created_at,));
		$manqulan = DB::select( DB::raw("INSERT INTO `t_manqulan`(`nilai_quran`, `nilai_hadits`, `nilai_tambahan`, `prestasi_quran`, `prestasi_hadits`, `prestasi_tambahan`, `keterangan_quran`, `keterangan_hadits`, `keterangan_tambahan`, `user_create`, `user_date_create`) VALUES (:nilai_quran,:nilai_hadits,:nilai_tambahan,:prestasi_quran,:prestasi_hadits,:prestasi_tambahan,:keterangan_quran,:keterangan_hadits,:keterangan_tambahan,:user_create,:created_at)"),array('nilai_quran' => $nilai_quran, 'nilai_hadits' => $nilai_hadits, 'nilai_tambahan' => $nilai_tambahan, 'prestasi_quran' => $prestasi_quran, 'prestasi_hadits' => $prestasi_hadits, 'prestasi_tambahan' => $prestasi_tambahan, 'keterangan_quran' => $keterangan_quran, 'keterangan_hadits' => $keterangan_hadits, 'keterangan_tambahan' => $keterangan_tambahan, 'user_create' => $user_create, 'created_at' => $created_at,));
		$akhlaq = DB::select( DB::raw("INSERT INTO `t_akhlaq`(`prestasi_akhlaq`, `nilai_akhlaq`, `keterangan_akhlaq`, `user_create`, `user_date_create`) VALUES (:prestasi_akhlaq,:nilai_akhlaq,:keterangan_akhlaq,:user_create,:created_at)"),array('prestasi_akhlaq' => $prestasi_akhlaq, 'nilai_akhlaq' => $nilai_akhlaq, 'keterangan_akhlaq' => $keterangan_akhlaq, 'user_create' => $user_create, 'created_at' => $created_at,));

		$kode_bacaan = DB::select( DB::raw("SELECT kode_bacaan FROM t_bacaan ORDER BY kode_bacaan DESC LIMIT 1"));
		$kode_hafalan = DB::select( DB::raw("SELECT kode_hafalan FROM t_hafalan ORDER BY kode_hafalan DESC LIMIT 1"));
		$kode_manqulan = DB::select( DB::raw("SELECT kode_manqulan FROM t_manqulan ORDER BY kode_manqulan DESC LIMIT 1"));
		$kode_akhlaq = DB::select( DB::raw("SELECT kode_akhlaq FROM t_akhlaq ORDER BY kode_akhlaq DESC LIMIT 1"));
			
		$datas = DB::select( DB::raw("INSERT INTO `t_nilai_detail`(`kode_murid`, `semester`, `wali_kelas`, `kode_bacaan`, `kode_hafalan`, `kode_manqulan`, `kode_akhlaq`, `catatan`, `keterangan`, `user_create`, `user_date_create`) VALUES (:kode_murid,:semester,:wali_kelas,:kode_bacaan,:kode_hafalan,:kode_manqulan,:kode_akhlaq,:catatan,:keterangan,:user_create,:created_at)"),array('kode_murid' => $kode_murid, 'semester' => $semester, 'wali_kelas' => $wali_kelas, 'kode_bacaan' => $kode_bacaan[0]->kode_bacaan, 'kode_hafalan' => $kode_hafalan[0]->kode_hafalan, 'kode_manqulan' => $kode_manqulan[0]->kode_manqulan, 'kode_akhlaq' => $kode_akhlaq[0]->kode_akhlaq, 'catatan' => $catatan, 'keterangan' => $keterangan, 'user_create' => $user_create, 'created_at' => $created_at,));

		return redirect('penilaian');
	}

	public function edit($kode_murid,$semester){
		$doa = DB::select(DB::raw("SELECT * FROM m_doa"));
		$dalil = DB::select(DB::raw("SELECT * FROM m_dalil"));
		$surat = DB::select(DB::raw("SELECT * FROM m_surat"));
		$quran = DB::select(DB::raw("SELECT * FROM m_quran"));
		$hadits = DB::select(DB::raw("SELECT * FROM m_hadits"));
		$tambahan = DB::select(DB::raw("SELECT * FROM m_tambahan"));
		$akhlaq = DB::select(DB::raw("SELECT * FROM m_akhlaq"));
		$gurus = DB::select( DB::raw("SELECT kode_guru,nama_guru FROM m_guru WHERE status = 1 ORDER BY nama_guru"));
		$murids = DB::select( DB::raw("SELECT kode_murid,nama_lengkap FROM m_murid WHERE status = 1 ORDER BY nama_lengkap"));
		$datas = DB::select( DB::raw("SELECT * FROM t_nilai_detail WHERE kode_murid = :kode_murid AND semester = :semester"),array('kode_murid' => $kode_murid,'semester' => $semester));
		return view('penilaian.edit',compact('datas','murids','gurus','doa','dalil','surat','quran','hadits','tambahan','akhlaq'));
	}

	public function update(Request $request,$kode_murid2,$semester2){
		$data_nilai = DB::select( DB::raw("SELECT * FROM t_nilai_detail WHERE kode_murid = :kode_murid AND semester = :semester"),array('kode_murid' => $kode_murid2,'semester' => $semester2));
		$delete = DB::select( DB::raw("DELETE FROM `t_akhlaq` WHERE kode_akhlaq = :kode_akhlaq"),array('kode_akhlaq' => $data_nilai[0]->kode_akhlaq));
		$delete = DB::select( DB::raw("DELETE FROM `t_bacaan` WHERE kode_bacaan = :kode_bacaan"),array('kode_bacaan' => $data_nilai[0]->kode_bacaan));
		$delete = DB::select( DB::raw("DELETE FROM `t_hafalan` WHERE kode_hafalan = :kode_hafalan"),array('kode_hafalan' => $data_nilai[0]->kode_hafalan));
		$delete = DB::select( DB::raw("DELETE FROM `t_manqulan` WHERE kode_manqulan = :kode_manqulan"),array('kode_manqulan' => $data_nilai[0]->kode_manqulan));
		$delete = DB::select( DB::raw("DELETE FROM `t_nilai_detail` WHERE kode_murid = :kode_murid AND semester = :semester"),array('kode_murid' => $kode_murid2,'semester' => $semester2));

		$semester= $request->input('semester');
		$kode_murid= $request->input('kode_murid');
		$wali_kelas= $request->input('kode_guru');
		$fashohah= $request->input('fashohah');
		$tajwid= $request->input('tajwid');
		$ghorib_muskhilat= $request->input('ghorib_muskhilat');
		$suara_lagu= $request->input('suara_lagu');
		$catatan= $request->input('catatan');
		$keterangan= $request->input('keterangan');
		$id_doa = (int)$request->input('id_doa')+1;
		$nilai_doa = 0;
		$prestasi_doa = '';
		$keterangan_doa = '';
		for($i = 0;$i < $id_doa;$i++){
			$nilai_doa+= $request->input('nilai_doa'.$i);
			if($request->input('doa'.$i) != ''){
				$prestasi_doa = $prestasi_doa.$request->input('doa'.$i).',';
				$keterangan_doa = $keterangan_doa.$request->input('status_doa'.$i).',';
			} else {
				$id_doa = $i;
				break;
			}
		}
		$nilai_doa = number_format((float)((int)$nilai_doa) / ((int)$id_doa), 2, '.', '');
		$id_dalil = (int)$request->input('id_dalil')+1;
		$nilai_dalil = 0;
		$prestasi_dalil = '';
		$keterangan_dalil = '';
		for($i = 0;$i < $id_dalil;$i++){
			$nilai_dalil+= $request->input('nilai_dalil'.$i);
			if($request->input('dalil'.$i) != ''){
				$prestasi_dalil = $prestasi_dalil.$request->input('dalil'.$i).'#'.$request->input('jml_dalil'.$i).',';
				$keterangan_dalil = $keterangan_dalil.$request->input('status_dalil'.$i).',';
			} else {
				$id_dalil = $i;
				break;
			}
		}
		$nilai_dalil = number_format((float)((int)$nilai_dalil) / ((int)$id_dalil), 2, '.', '');
		$id_surat = (int)$request->input('id_surat')+1;
		$nilai_surat = 0;
		$prestasi_surat = '';
		$keterangan_surat = '';
		for($i = 0;$i < $id_surat;$i++){
			$nilai_surat+= $request->input('nilai_surat'.$i);
			if($request->input('surat'.$i) != ''){
				$prestasi_surat = $prestasi_surat.$request->input('surat'.$i).',';
				$keterangan_surat = $keterangan_surat.$request->input('status_surat'.$i).',';
			} else {
				$id_surat = $i;
				break;
			}
		}
		$nilai_surat = number_format((float)((int)$nilai_surat) / ((int)$id_surat), 2, '.', '');
		$id_quran = (int)$request->input('id_quran')+1;
		$nilai_quran = 0;
		$prestasi_quran = '';
		$keterangan_quran = '';
		for($i = 0;$i < $id_quran;$i++){
			$nilai_quran+= $request->input('nilai_quran'.$i);
			if($request->input('quran'.$i) != ''){
				$prestasi_quran = $prestasi_quran.$request->input('quran'.$i).',';
				$keterangan_quran = $keterangan_quran.$request->input('status_quran'.$i).',';
			} else {
				$id_quran = $i;
				break;
			}
		}
		$nilai_quran = number_format((float)((int)$nilai_quran) / ((int)$id_quran), 2, '.', '');
		$id_hadits = (int)$request->input('id_hadits')+1;
		$nilai_hadits = 0;
		$prestasi_hadits = '';
		$keterangan_hadits = '';
		for($i = 0;$i < $id_hadits;$i++){
			$nilai_hadits+= $request->input('nilai_hadits'.$i);
			if($request->input('hadits'.$i) != ''){
				$prestasi_hadits = $prestasi_hadits.$request->input('hadits'.$i).',';
				$keterangan_hadits = $keterangan_hadits.$request->input('status_hadits'.$i).',';
			} else {
				$id_hadits = $i;
				break;
			}
		}
		$nilai_hadits = number_format((float)((int)$nilai_hadits) / ((int)$id_hadits), 2, '.', '');
		$id_tambahan = (int)$request->input('id_tambahan')+1;
		$nilai_tambahan = 0;
		$prestasi_tambahan = '';
		$keterangan_tambahan = '';
		for($i = 0;$i < $id_tambahan;$i++){
			$nilai_tambahan+= $request->input('nilai_tambahan'.$i);
			if($request->input('tambahan'.$i) != ''){
				$prestasi_tambahan = $prestasi_tambahan.$request->input('tambahan'.$i).',';
				$keterangan_tambahan = $keterangan_tambahan.$request->input('status_tambahan'.$i).',';
			} else {
				$id_tambahan = $i;
				break;
			}
		}
		if($id_tambahan == 0)
			$id_tambahan++;
		$nilai_tambahan = number_format((float)((int)$nilai_tambahan) / ((int)$id_tambahan), 2, '.', '');
		$id_akhlaq = (int)$request->input('id_akhlaq')+1;
		$nilai_akhlaq = 0;
		$prestasi_akhlaq = '';
		$keterangan_akhlaq = '';
		for($i = 0;$i < $id_akhlaq;$i++){
			$nilai_akhlaq+= $request->input('nilai_akhlaq'.$i);
			if($request->input('akhlaq'.$i) != ''){
				$prestasi_akhlaq = $prestasi_akhlaq.$request->input('akhlaq'.$i).',';
				$keterangan_akhlaq = $keterangan_akhlaq.$request->input('status_akhlaq'.$i).',';
			} else {
				$id_akhlaq = $i;
				break;
			}
		}
		$nilai_akhlaq = number_format((float)((int)$nilai_akhlaq) / ((int)$id_akhlaq), 2, '.', '');
		$user_create = auth()->user()->getProfile->name;
		$created_at= \Carbon\Carbon::Now('Asia/Jakarta');

		$bacaan = DB::select( DB::raw("INSERT INTO `t_bacaan`(`fashohah`, `tajwid`, `ghorib_muskhilat`, `suara_lagu`, `user_create`, `user_date_create`) VALUES (:fashohah,:tajwid,:ghorib_muskhilat,:suara_lagu,:user_create,:created_at)"),array('fashohah' => $fashohah, 'tajwid' => $tajwid, 'ghorib_muskhilat' => $ghorib_muskhilat, 'suara_lagu' => $suara_lagu, 'user_create' => $user_create, 'created_at' => $created_at,));
		$hafalan = DB::select( DB::raw("INSERT INTO `t_hafalan`(`nilai_doa`, `nilai_dalil`, `nilai_surat`, `prestasi_doa`, `prestasi_dalil`, `prestasi_surat`, `keterangan_doa`, `keterangan_dalil`, `keterangan_surat`, `user_create`, `user_date_create`) VALUES (:nilai_doa,:nilai_dalil,:nilai_surat,:prestasi_doa,:prestasi_dalil,:prestasi_surat,:keterangan_doa,:keterangan_dalil,:keterangan_surat,:user_create,:created_at)"),array('nilai_doa' => $nilai_doa, 'nilai_dalil' => $nilai_dalil, 'nilai_surat' => $nilai_surat, 'prestasi_doa' => $prestasi_doa, 'prestasi_dalil' => $prestasi_dalil, 'prestasi_surat' => $prestasi_surat, 'keterangan_doa' => $keterangan_doa, 'keterangan_dalil' => $keterangan_dalil, 'keterangan_surat' => $keterangan_surat, 'user_create' => $user_create, 'created_at' => $created_at,));
		$manqulan = DB::select( DB::raw("INSERT INTO `t_manqulan`(`nilai_quran`, `nilai_hadits`, `nilai_tambahan`, `prestasi_quran`, `prestasi_hadits`, `prestasi_tambahan`, `keterangan_quran`, `keterangan_hadits`, `keterangan_tambahan`, `user_create`, `user_date_create`) VALUES (:nilai_quran,:nilai_hadits,:nilai_tambahan,:prestasi_quran,:prestasi_hadits,:prestasi_tambahan,:keterangan_quran,:keterangan_hadits,:keterangan_tambahan,:user_create,:created_at)"),array('nilai_quran' => $nilai_quran, 'nilai_hadits' => $nilai_hadits, 'nilai_tambahan' => $nilai_tambahan, 'prestasi_quran' => $prestasi_quran, 'prestasi_hadits' => $prestasi_hadits, 'prestasi_tambahan' => $prestasi_tambahan, 'keterangan_quran' => $keterangan_quran, 'keterangan_hadits' => $keterangan_hadits, 'keterangan_tambahan' => $keterangan_tambahan, 'user_create' => $user_create, 'created_at' => $created_at,));
		$akhlaq = DB::select( DB::raw("INSERT INTO `t_akhlaq`(`prestasi_akhlaq`, `nilai_akhlaq`, `keterangan_akhlaq`, `user_create`, `user_date_create`) VALUES (:prestasi_akhlaq,:nilai_akhlaq,:keterangan_akhlaq,:user_create,:created_at)"),array('prestasi_akhlaq' => $prestasi_akhlaq, 'nilai_akhlaq' => $nilai_akhlaq, 'keterangan_akhlaq' => $keterangan_akhlaq, 'user_create' => $user_create, 'created_at' => $created_at,));

		$kode_bacaan = DB::select( DB::raw("SELECT kode_bacaan FROM t_bacaan ORDER BY kode_bacaan DESC LIMIT 1"));
		$kode_hafalan = DB::select( DB::raw("SELECT kode_hafalan FROM t_hafalan ORDER BY kode_hafalan DESC LIMIT 1"));
		$kode_manqulan = DB::select( DB::raw("SELECT kode_manqulan FROM t_manqulan ORDER BY kode_manqulan DESC LIMIT 1"));
		$kode_akhlaq = DB::select( DB::raw("SELECT kode_akhlaq FROM t_akhlaq ORDER BY kode_akhlaq DESC LIMIT 1"));
			
		$datas = DB::select( DB::raw("INSERT INTO `t_nilai_detail`(`kode_murid`, `semester`, `wali_kelas`, `kode_bacaan`, `kode_hafalan`, `kode_manqulan`, `kode_akhlaq`, `catatan`, `keterangan`, `user_create`, `user_date_create`) VALUES (:kode_murid,:semester,:wali_kelas,:kode_bacaan,:kode_hafalan,:kode_manqulan,:kode_akhlaq,:catatan,:keterangan,:user_create,:created_at)"),array('kode_murid' => $kode_murid, 'semester' => $semester, 'wali_kelas' => $wali_kelas, 'kode_bacaan' => $kode_bacaan[0]->kode_bacaan, 'kode_hafalan' => $kode_hafalan[0]->kode_hafalan, 'kode_manqulan' => $kode_manqulan[0]->kode_manqulan, 'kode_akhlaq' => $kode_akhlaq[0]->kode_akhlaq, 'catatan' => $catatan, 'keterangan' => $keterangan, 'user_create' => $user_create, 'created_at' => $created_at,));

		return redirect('penilaian');
	}

	public function destroy($kode_murid2,$semester2){
		$data_nilai = DB::select( DB::raw("SELECT * FROM t_nilai_detail WHERE kode_murid = :kode_murid AND semester = :semester"),array('kode_murid' => $kode_murid2,'semester' => $semester2));
		$delete = DB::select( DB::raw("DELETE FROM `t_akhlaq` WHERE kode_akhlaq = :kode_akhlaq"),array('kode_akhlaq' => $data_nilai[0]->kode_akhlaq));
		$delete = DB::select( DB::raw("DELETE FROM `t_bacaan` WHERE kode_bacaan = :kode_bacaan"),array('kode_bacaan' => $data_nilai[0]->kode_bacaan));
		$delete = DB::select( DB::raw("DELETE FROM `t_hafalan` WHERE kode_hafalan = :kode_hafalan"),array('kode_hafalan' => $data_nilai[0]->kode_hafalan));
		$delete = DB::select( DB::raw("DELETE FROM `t_manqulan` WHERE kode_manqulan = :kode_manqulan"),array('kode_manqulan' => $data_nilai[0]->kode_manqulan));
		$delete = DB::select( DB::raw("DELETE FROM `t_nilai_detail` WHERE kode_murid = :kode_murid AND semester = :semester"),array('kode_murid' => $kode_murid2,'semester' => $semester2));

		return redirect('penilaian');
	}

	public function cetak($kode_murid,$semester){
		$data = DB::select( DB::raw("SELECT a.semester,a.catatan,a.keterangan,b.nama_guru,c.fashohah,c.tajwid,c.ghorib_muskhilat,c.suara_lagu,d.*,e.*,f.prestasi_akhlaq,f.nilai_akhlaq,f.keterangan_akhlaq,g.*,h.nama_kelompok,i.nama_desa FROM t_nilai_detail a JOIN m_guru b ON a.wali_kelas = b.kode_guru JOIN t_bacaan c ON a.kode_bacaan = c.kode_bacaan JOIN t_hafalan d ON a.kode_hafalan = d.kode_hafalan JOIN t_manqulan e ON a.kode_manqulan = e.kode_manqulan JOIN t_akhlaq f ON a.kode_akhlaq = f.kode_akhlaq JOIN m_murid g ON a.kode_murid = g.kode_murid JOIN m_kelompok h ON g.kode_kelompok = h.kode_kelompok AND g.kode_desa = h.kode_desa JOIN m_desa i ON h.kode_desa = i.kode_desa WHERE a.kode_murid = :kode_murid AND a.semester = :semester"),array('kode_murid' => $kode_murid,'semester' => $semester));
		if($semester % 2 == 1){
			// $absen = DB::select( DB::raw("SELECT januari,februari,maret,april,mei,juni FROM t_absensi WHERE kode_murid = :kode_murid AND tahun = :tahun"),array('kode_murid' => $kode_murid,'tahun' => date("Y")));
			$absen1 = DB::select( DB::raw("SELECT SUM(LENGTH(januari) - LENGTH(REPLACE(januari, '0', ''))) libur, SUM(LENGTH(januari) - LENGTH(REPLACE(januari, '1', ''))) hadir, SUM(LENGTH(januari) - LENGTH(REPLACE(januari, '2', ''))) izin, SUM(LENGTH(januari) - LENGTH(REPLACE(januari, '3', ''))) alfa FROM t_absensi WHERE tahun = :year1 AND kode_murid = :kode_murid"),array('year1' => date("Y"),'kode_murid' => $kode_murid));
			$absen2 = DB::select( DB::raw("SELECT SUM(LENGTH(februari) - LENGTH(REPLACE(februari, '0', ''))) libur, SUM(LENGTH(februari) - LENGTH(REPLACE(februari, '1', ''))) hadir, SUM(LENGTH(februari) - LENGTH(REPLACE(februari, '2', ''))) izin, SUM(LENGTH(februari) - LENGTH(REPLACE(februari, '3', ''))) alfa FROM t_absensi WHERE tahun = :year1 AND kode_murid = :kode_murid"),array('year1' => date("Y"),'kode_murid' => $kode_murid));
			$absen3 = DB::select( DB::raw("SELECT SUM(LENGTH(maret) - LENGTH(REPLACE(maret, '0', ''))) libur, SUM(LENGTH(maret) - LENGTH(REPLACE(maret, '1', ''))) hadir, SUM(LENGTH(maret) - LENGTH(REPLACE(maret, '2', ''))) izin, SUM(LENGTH(maret) - LENGTH(REPLACE(maret, '3', ''))) alfa FROM t_absensi WHERE tahun = :year1 AND kode_murid = :kode_murid"),array('year1' => date("Y"),'kode_murid' => $kode_murid));
			$absen4 = DB::select( DB::raw("SELECT SUM(LENGTH(april) - LENGTH(REPLACE(april, '0', ''))) libur, SUM(LENGTH(april) - LENGTH(REPLACE(april, '1', ''))) hadir, SUM(LENGTH(april) - LENGTH(REPLACE(april, '2', ''))) izin, SUM(LENGTH(april) - LENGTH(REPLACE(april, '3', ''))) alfa FROM t_absensi WHERE tahun = :year1 AND kode_murid = :kode_murid"),array('year1' => date("Y"),'kode_murid' => $kode_murid));
			$absen5 = DB::select( DB::raw("SELECT SUM(LENGTH(mei) - LENGTH(REPLACE(mei, '0', ''))) libur, SUM(LENGTH(mei) - LENGTH(REPLACE(mei, '1', ''))) hadir, SUM(LENGTH(mei) - LENGTH(REPLACE(mei, '2', ''))) izin, SUM(LENGTH(mei) - LENGTH(REPLACE(mei, '3', ''))) alfa FROM t_absensi WHERE tahun = :year1 AND kode_murid = :kode_murid"),array('year1' => date("Y"),'kode_murid' => $kode_murid));
			$absen6 = DB::select( DB::raw("SELECT SUM(LENGTH(juni) - LENGTH(REPLACE(juni, '0', ''))) libur, SUM(LENGTH(juni) - LENGTH(REPLACE(juni, '1', ''))) hadir, SUM(LENGTH(juni) - LENGTH(REPLACE(juni, '2', ''))) izin, SUM(LENGTH(juni) - LENGTH(REPLACE(juni, '3', ''))) alfa FROM t_absensi WHERE tahun = :year1 AND kode_murid = :kode_murid"),array('year1' => date("Y"),'kode_murid' => $kode_murid));
		} else {
			// $absen = DB::select( DB::raw("SELECT juli,agustus,september,oktober,november,desember FROM t_absensi WHERE kode_murid = :kode_murid AND tahun = :tahun"),array('kode_murid' => $kode_murid,'tahun' => ((int)date("Y")-1)));
			$absen1 = DB::select( DB::raw("SELECT SUM(LENGTH(juli) - LENGTH(REPLACE(juli, '0', ''))) libur, SUM(LENGTH(juli) - LENGTH(REPLACE(juli, '1', ''))) hadir, SUM(LENGTH(juli) - LENGTH(REPLACE(juli, '2', ''))) izin, SUM(LENGTH(juli) - LENGTH(REPLACE(juli, '3', ''))) alfa FROM t_absensi WHERE tahun = :year1 AND kode_murid = :kode_murid"),array('year1' => date("Y"),'kode_murid' => $kode_murid));
			$absen2 = DB::select( DB::raw("SELECT SUM(LENGTH(agustus) - LENGTH(REPLACE(agustus, '0', ''))) libur, SUM(LENGTH(agustus) - LENGTH(REPLACE(agustus, '1', ''))) hadir, SUM(LENGTH(agustus) - LENGTH(REPLACE(agustus, '2', ''))) izin, SUM(LENGTH(agustus) - LENGTH(REPLACE(agustus, '3', ''))) alfa FROM t_absensi WHERE tahun = :year1 AND kode_murid = :kode_murid"),array('year1' => date("Y"),'kode_murid' => $kode_murid));
			$absen3 = DB::select( DB::raw("SELECT SUM(LENGTH(september) - LENGTH(REPLACE(september, '0', ''))) libur, SUM(LENGTH(september) - LENGTH(REPLACE(september, '1', ''))) hadir, SUM(LENGTH(september) - LENGTH(REPLACE(september, '2', ''))) izin, SUM(LENGTH(september) - LENGTH(REPLACE(september, '3', ''))) alfa FROM t_absensi WHERE tahun = :year1 AND kode_murid = :kode_murid"),array('year1' => date("Y"),'kode_murid' => $kode_murid));
			$absen4 = DB::select( DB::raw("SELECT SUM(LENGTH(oktober) - LENGTH(REPLACE(oktober, '0', ''))) libur, SUM(LENGTH(oktober) - LENGTH(REPLACE(oktober, '1', ''))) hadir, SUM(LENGTH(oktober) - LENGTH(REPLACE(oktober, '2', ''))) izin, SUM(LENGTH(oktober) - LENGTH(REPLACE(oktober, '3', ''))) alfa FROM t_absensi WHERE tahun = :year1 AND kode_murid = :kode_murid"),array('year1' => date("Y"),'kode_murid' => $kode_murid));
			$absen5 = DB::select( DB::raw("SELECT SUM(LENGTH(november) - LENGTH(REPLACE(november, '0', ''))) libur, SUM(LENGTH(november) - LENGTH(REPLACE(november, '1', ''))) hadir, SUM(LENGTH(november) - LENGTH(REPLACE(november, '2', ''))) izin, SUM(LENGTH(november) - LENGTH(REPLACE(november, '3', ''))) alfa FROM t_absensi WHERE tahun = :year1 AND kode_murid = :kode_murid"),array('year1' => date("Y"),'kode_murid' => $kode_murid));
			$absen6 = DB::select( DB::raw("SELECT SUM(LENGTH(desember) - LENGTH(REPLACE(desember, '0', ''))) libur, SUM(LENGTH(desember) - LENGTH(REPLACE(desember, '1', ''))) hadir, SUM(LENGTH(desember) - LENGTH(REPLACE(desember, '2', ''))) izin, SUM(LENGTH(desember) - LENGTH(REPLACE(desember, '3', ''))) alfa FROM t_absensi WHERE tahun = :year1 AND kode_murid = :kode_murid"),array('year1' => date("Y"),'kode_murid' => $kode_murid));
		}
		
		view()->share(compact('data','absen1','absen2','absen3','absen4','absen5','absen6'));

        $pdf = PDF::loadView('cetakrapot')->setPaper('a4', 'potrait');
        return $pdf->stream($data[0]->nama_lengkap.'-'.$semester.('.pdf')) ;
	}
}
