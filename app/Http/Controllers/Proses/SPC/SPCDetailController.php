<?php

namespace App\Http\Controllers\Proses\SPC;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Detail;
use App\Transaksi;
use App\Crud;
use App\Lelang;
use App\Masa;
use App\Barang;
use App\JenisBarang;
use App\StatusAktif;
use DB;

class SPCDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function create($id_spc) 
    {

      	$barang = Barang::all();
        $transaksi = Transaksi::where('id_spc',$id_spc)->firstOrfail();
		$jenis = JenisBarang::all();
        return view('Proses/SPC/Detail.add', compact( 'id_spc' ,'transaksi','barang', 'pemasoks', 'jenis', 'masa'));
    }


    public function getbarang(Request $request)
    {
        $kode = $request->Input('kode_jenis_barang'); 
        $datas = new Barang;
        $data = $datas->getBarangDoang($kode);
        return response()->json($data);
    }


    public function getedisi(Request $request)
    {
    	$kode = $request->Input('kode_barang'); 
        $kode_jenis_barang = $request->Input('kode_jenis_barang'); 
        $datas = new Barang;
        $data = $datas->getEdisiDoang($kode,$kode_jenis_barang);
        return response()->json($data);
    }


     public function store(Request $request)
    {

        // if($request['kode_barang'] != '') {
        //     $check = \App\Detail::where('kode_barang',$request['kode_barang'])->where('id_spc',$request['id_spc'])->first();
        //     $result = $check->kode_barang;
        //     if($check) {
        //         return redirect()->back()->with('error','Kode Barang' .' '. $check->kode_barang.' '.'Sudah Ada');
        //     }

        $this->validate($request, [
        'id_spc' => 'required',
        'kode_barang' => 'required',
        'jumlah_pesan_cetak' => 'required',
         ]);

        $tambah = new Detail();
        $tambah->id_spc = $request['id_spc'];
        $tambah->kode_barang = $request['kode_barang'];
        $tambah->kode_jenis_barang = $request['kode_jenis_barang'];
        $tambah->edisi = $request['edisi'];
        $tambah->jumlah_pesan_cetak = $request['jumlah_pesan_cetak'];
        $tambah->user_create = auth()->user()->name;
        $tambah->user_date_create = \Carbon\Carbon::Now('Asia/Jakarta');
        $tambah->save();

        return redirect()->to('/spc/tampil/'.$request['id_spc']);
		}
	
     
    public function edit($id_spc,$kode_barang)
    {
        $editspc = Detail::where('id_spc',$id_spc)->where('kode_barang', $kode_barang)->first();
        $transaksi = Transaksi::where('id_spc',$id_spc)->firstOrfail();
        $barang = Barang::all(); 
        $jenis = JenisBarang::all();
 
        return view('Proses/SPC/Detail.edit',compact('editspc','transaksi','barang','jenis'));

    }

 public function update( Request $request, $id_spc)
    {
        // dd($request->all());
        if($request['id_spc'] == '')
        {
            
            $update = [
            
            // 'kode_barang'=> $request ['kode_barang'],
            // 'kode_jenis_barang'=> $request ['kode_jenis_barang'],
            // 'edisi'=> $request ['edisi'],
            'jumlah_pesan_cetak'=> $request ['jumlah_pesan_cetak'],
            'user_change' => auth()->user()->name,
            'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
            ];
        }
        
        else
        {
            $update=[
            'id_spc'=> $request ['id_spc'],
			'kode_barang'=> $request ['kode_barang'],
            'kode_jenis_barang'=> $request ['kode_jenis_barang'],
            'edisi'=> $request ['edisi'],
            'jumlah_pesan_cetak'=> $request ['jumlah_pesan_cetak'],
            'user_change' => auth()->user()->name,
            'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
            ];
        }

        \App\Detail::where('id_spc', $id_spc)->update($update);
        return redirect()->to('/spc/tampil/'. $id_spc);
    }

       public function destroy($id_spc,$kode_barang, $kode_jenis_barang, $edisi)
    {
        $checkInDetail = \App\SPM::where('kode_barang', $kode_barang)->get();

        $checkInHeader = \App\SPM::where('kode_barang', $kode_barang)->get();

        if(count($checkInDetail)) {

            return redirect()->back()->with('error','Mohon Maaf Data Ini Terrelasi Ke Tiras Header.');

        } else if(count($checkInHeader)) {

            return redirect()->back()->with('error','Mohon Maaf Data Ini Terrelasi Ke SPM Header');

        } else {

            $thisIs = \App\Detail::where('id_spc',$id_spc)->where('kode_barang',$kode_barang)->where('kode_jenis_barang', $kode_jenis_barang)->where('edisi', $edisi);
  
      // Create New Notification
      // $notif = new Notifikasi;
      // $log = 'menghapus fakultas '.$kode_barang;
      // $notifikasi = $notif->CreateNotifikasi(auth()->user()->getProfile->name, 1, $log);
  
            $thisIs->delete();
      
      return redirect()->to('spc');
        }
}


}
