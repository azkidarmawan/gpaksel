<?php

namespace App\Http\Controllers\Proses\SPC;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaksi;
use App\Crud;
use App\Lelang;
use App\Masa;
use App\SpmHeader;
use App\Notifikasi;
use DB;
use PDF; // pdf namespace
use Excel; // Excel namespace

use App\spctoExcel;

class SPCController extends Controller
{
        public function __construct()
    {
        $this->middleware('auth');
    }
	public function index(Request $req)
    {
	// show all data to index
      $blogs = spctoExcel::all();
      view()->share('blogs',$blogs);
      // if request has pdf
      if($req->has('downloadpdf')){
        $pdf = PDF::loadView('pdf')->setPaper('a4', 'landscape');
        return $pdf->download('pdf');
      }
      // if request has excel
	  $time = \Carbon\Carbon::Now('Asia/Jakarta');
	  $result = $time->format('d-m-Y');
      if($req->has('downloadexcel')){
        Excel::create('SPC '.$result, function($excel) use ($blogs) {
          $excel->sheet('Sheet 1', function($sheet) use ($blogs) {
            $sheet->fromArray($blogs);
          });
        })->export('xls');
      }
	
    $datas = DB::table('t_spc_header')
	->join('m_pemasok','t_spc_header.kode_pemasok','=','m_pemasok.kode_pemasok')
	->join('m_status_lelang','t_spc_header.kode_status_lelang','=','m_status_lelang.kode_status_lelang')
    ->join('m_masa','t_spc_header.masa','=','m_masa.masa')
    ->join('t_spm_header','t_spc_header.id_spm','=','t_spm_header.id_spm')
	->select('t_spc_header.*', 'm_pemasok.*', 'm_status_lelang.*', 'm_masa.*')
    
	->get();

	// Mengupdate id_notif pada user
	$data = new Notifikasi;
	$daftar = $data->DaftarNotifikasi(auth()->user()->getProfile->id_notif,auth()->user()->getProfile->notifikasi);
	$jumlah = count($daftar);
	if($jumlah > 0)
	$update = $data->UpdateNotifikasi(auth()->user()->getProfile->id,$daftar[0]->id);
	
    return view('Proses\SPC\Header.index', compact('datas'));	  
	 
	}

    public function create()
    {

           
        $datakode = Transaksi::max('nomor_spc');
        if($datakode){
        $nilaikode = substr($datakode, 3);
        // menjadikan $nilaikode ( int )
        $kode = (int) $nilaikode;
        // setiap $kode di tambah 1
        $kode = $kode + 1;
        // hasil untuk menambahkan kode 
        // angka 5 untuk menambahkan tiga angka setelah C dan angka 0 angka yang berada di tengah
        // atau angka sebelum $kode
        $newKode = "SPC".str_pad($kode, 5, "0", STR_PAD_LEFT);
        } else {
          $newKode = "SPC00001";
        }

        $pemasoks = Crud::all(); 
        $lelang = Lelang::all(); 
        $masa = Masa::all();
        $spm = SpmHeader::all();

        $spm = new Transaksi;
        $last_spm = $spm->LastSpm();
        // cek apakah sudah ada SPM pd DB atau blm
        if(count($last_spm) == 0){
            $nomor_spc = '001/UN.31.3/BA/'.date("Y");
        } else {
            $surat = explode("/", $last_spm[0]->nomor_spc);
            // cek apakah sudah berganti tahun
            if($surat[3] != date("Y")){
                $nomor_spc = '001/UN.31.3/BA/'.date("Y");
            } else {
                $kode = (int) $surat[0];
                $kode = $kode + 1;
                if($kode < 10)
                    $nomor_spc = '00'.$kode.'/UN.31.3/BA/'.date("Y");
                else if($kode > 99)
                    $nomor_spc = $kode.'/UN.31.3/BA/'.date("Y");
                else
                    $nomor_spc = '0'.$kode.'/UN.31.3/BA/'.date("Y");
            }
            
        }
        return view('Proses\SPC\Header.add', compact('pemasoks', 'lelang', 'masa', 'nomor_spc', 'spm'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());


        $this->validate($request, [
        
                
        'no_spc' => 'required',

        'tanggal_spc' => 'required',
        'biaya' => 'required',
        'tanggal_selesai_cetak' => 'required',
        
            ]);

        $tambah = new Transaksi();
        
        $tambah->no_spc = $request['no_spc'];
        $tambah->kode_pemasok = $request['kode_pemasok'];
        $tambah->kode_status_lelang = $request['kode_status_lelang'];
        $tambah->masa = $request['masa'];
        $tambah->tanggal_spc = $request['tanggal_spc'];
        $tambah->biaya = $request['biaya'];
        $tambah->tanggal_selesai_cetak = $request['tanggal_selesai_cetak'];
        $tambah->user_create = auth()->user()->name;
        $tambah->user_date_create = \Carbon\Carbon::Now('Asia/Jakarta');



        $tambah->save();

        return redirect()->to('/spc');

    }

           //Untuk Edit
    public function edit($no_spc)
    {
        $checkInSPM = \App\Detail::where('id_spc', $no_spc)->first();

        $checkInHeader = \App\TandaTerima::where('id_spc', $no_spc)->first();

        $tampiledit = Transaksi::find($no_spc);
        $pemasoks = Crud::all();
        $lelang = Lelang::all(); 
        $masa = Masa::all(); 
        return view('Proses\SPC\Header.edit',compact('tampiledit','pemasoks','lelang', 'masa','checkInSPM','checkInHeader'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_spc)
    {

        if($request['id_spc'] == '') {
            
            $update = [
                'tanggal_spc' => $request['tanggal_spc'],
                'kode_pemasok' => $request['kode_pemasok'],
                'kode_status_lelang' => $request['kode_status_lelang'],
                'tanggal_selesai_cetak' => $request['tanggal_selesai_cetak'],
                'biaya' => $request['biaya'],
                'masa' => $request['masa'],
                'user_change' => auth()->user()->name,
                'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
            ];

        } else {

            $update = [
                'id_spc' => $request['id_spc'],
                'tanggal_spc' => $request['tanggal_spc'],
                'kode_pemasok' => $request['kode_pemasok'],
                'kode_status_lelang' => $request['kode_status_lelang'],
                'tanggal_selesai_cetak' => $request['tanggal_selesai_cetak'],
                'biaya' => $request['biaya'],
                'masa' => $request['masa'],
                'user_change' => auth()->user()->name,
                'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
            ];

        }

        \App\Transaksi::where('id_spc', $id_spc)->update($update);


        return redirect()->to('/spc');
        
    }

    public function destroy($id_spc)
    {
        $checkInDetail = \App\Detail::where('id_spc', $id_spc)->get();

        $checkInHeader = \App\TandaTerima::where('id_spc', $id_spc)->get();

        if(count($checkInDetail)) {

            return redirect()->back()->with('error','Mohon Maaf Data Ini Memiliki Detail.');

        } else if(count($checkInHeader)) {

            return redirect()->back()->with('error','Mohon Maaf Data Ini Memiliki Header.');

        } else {

            $thisIs = \App\Transaksi::find($id_spc);

            $thisIs->delete();

            return redirect()->to('/spc');
        }
    }


                    //UNTUK DETAIL
    public function show($id_spc)
    
    {
        $tampilkan = DB::table('t_spc_detail')
                    ->join('t_barang','t_barang.kode_barang','=','t_spc_detail.kode_barang')
                    ->select('t_spc_detail.*','t_barang.*')
                    ->where('id_spc',$id_spc)
                    ->get();

        $id_spc = $id_spc;
        $daftar = DB::select( DB::raw("SELECT * FROM t_spc_header WHERE id_spc = :id_spc"), array(
                'id_spc' => $id_spc,
                ));
        return view('Proses\SPC\Header.tampil', compact('tampilkan','id_spc', 'daftar'));
    }
}
