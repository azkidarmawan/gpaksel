<?php

namespace App\Http\Controllers\Proses\Tiras;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Excel;
use PDF;
use App\Models\Master\Fakultas;
use App\Models\Master\Masa;
use App\Models\Master\Status_Aktif;
use App\Models\Tiras\Tiras_Header;
use App\Models\Tiras\Tiras_Detail;
use App\JenisBarang;
use App\BahanAjar;
use App\Notifikasi;

class TirasDetailController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function PDF($no_surat)
    {

        $daftar=DB::table('t_tiras_header')
        ->join('m_fakultas', 'm_fakultas.kode_fakultas','=','t_tiras_header.kode_fakultas')
        ->join('m_masa', 'm_masa.masa','=','t_tiras_header.masa')
        ->select('t_tiras_header.*', 'm_fakultas.nama_pendek','m_masa.masa')
        ->where('id_tiras', $no_surat)
        ->get();

        $data=DB::table('t_tiras_detail')
        ->join('m_jenis_barang', 'm_jenis_barang.kode_jenis_barang','=','t_tiras_detail.kode_jenis_barang')
        ->join('m_barang', 'm_barang.kode_barang','=','t_tiras_detail.kode_barang')
        ->select('t_tiras_detail.*', 'm_jenis_barang.nama_jenis_barang', 'm_barang.nama_barang')
        ->where('id_tiras', $no_surat)
        ->get();

      view()->share(compact('daftar','data'));

        $pdf = PDF::loadView('pdf')->setPaper('a4', 'potrait');
        return $pdf->stream('Tiras');
    }

    public function create($no_surat) 
    { 
        $fakultas = \App\Models\Master\Fakultas::all(); 
        $masa = \App\Models\Master\Masa::all(); 
        $Jenisbarang = \App\JenisBarang::all(); 
        $statusaktif = \App\Models\Master\Status_Aktif::all(); 
        $barang = \App\BahanAjar::where('kode_status',1)->get(); 
        $daftar = DB::select( DB::raw("SELECT distinct kode_barang FROM t_barang WHERE kode_status = 1"
                ));
        // $edisi = DB::select( DB::raw("SELECT distinct nama_jenis_barang FROM m_jenis_barang WHERE kode_jenis_barang = :kode_jenis_barang"
        //         ), array('kode_jenis_barang' => ));

        $tirasheader = \App\Models\Tiras\Tiras_Header::where('id_tiras',$no_surat)->firstOrfail();
        $tirasdetail = \App\Models\Tiras\Tiras_Detail::all(); 
        return view('Proses\Tiras\Detail.add', compact('fakultas','masa','statusaktif','tirasheader','tirasdetail','barang','Jenisbarang','no_surat', 'daftar'));
    }

    public function getbarang(Request $request)
    {
        $kode = $request->Input('kode_barang'); 
        $datas = new BahanAjar;
        $data = DB::table('t_barang')
        ->join('m_jenis_barang','t_barang.kode_jenis_barang','=','m_jenis_barang.kode_jenis_barang')
        ->select('t_barang.*', 'm_jenis_barang.nama_jenis_barang')
        ->where('t_barang.kode_barang', $kode)
        ->get();
        return response()->json($data);
    }

    public function getedisi(Request $request)
    {
        $kode = $request->Input('kode_barang'); 
        $jenis = $request->Input('kode_jenis_barang'); 
        $datas = new BahanAjar;
        $data = $datas->getEdisiDoang($kode,$jenis); 
        return response()->json($data);
    }

    public function getcetakan(Request $request)
    {
        $kode = $request->Input('kode_barang'); 
        $edisi = $request->Input('edisi');
        $datas = new BahanAjar;
        $data = $datas->getCetakanDoang($kode,$edisi);
        return response()->json($data);
    }
    public function store(Request $request)
    {
        if($request['id_tiras'] != '')
        if($request['kode_jenis_barang'] != '') 
        if($request['kode_barang'] != '')
        if($request['edisi'] != '')
        {
        $check =\App\Models\Tiras\Tiras_Detail::where('id_tiras',$request['id_tiras'])
            ->where('kode_jenis_barang',$request['kode_jenis_barang'])
            ->where('kode_barang',$request['kode_barang'])
            ->where('edisi',$request['edisi'])
            ->first();
        if ($check) {
            return redirect()->back()->with('error','Barang Tersebut Sudah Ada');
        }
       
            // dd($cetakan);
            $tambah = new \App\Models\Tiras\Tiras_Detail();
            $tambah->id_tiras = $request['id_tiras'];
            $tambah->kode_barang = $request['kode_barang'];
            $tambah->kode_jenis_barang = $request['kode_jenis_barang'];
            $tambah->edisi = $request['edisi'];
            $tambah->keterangan = $request['keterangan'];
            $tambah->stok = $request['stok'];
            $tambah->usulan_tiras = $request['usulan_tiras'];
            $tambah->jml_tiras = $request['jml_tiras'];
            $tambah->cetakan = $request['cetakan']; 
            $tambah->user_create =auth()->user()->name;
            $tambah->user_date_create =\Carbon\Carbon::Now('Asia/Jakarta');
            $save = $tambah->save();

        return redirect()->to('tiras/tampil/'.$request['id_tiras']);
    }
    }
    public function edit($id_tiras,$kode_barang,$kode_jenis_barang,$edisi)
    {
        $masa = \App\Models\Master\Masa::all(); 
        $Jenisbarang = \App\JenisBarang::all(); 
        $statusaktif = \App\Models\Master\Status_Aktif::all(); 
        $barang = \App\Models\Master\Barang::all();
        $tirasheader = \App\Models\Tiras\Tiras_Header::where('id_tiras',$id_tiras)->firstOrfail();
        $tirasdetail = \App\Models\Tiras\Tiras_Detail::where('id_tiras',$id_tiras)->where('kode_barang',$kode_barang)->where('kode_jenis_barang',$kode_jenis_barang)->where('edisi',$edisi)->firstOrfail();
        $daftar = DB::select( DB::raw("SELECT distinct kode_barang FROM t_barang WHERE kode_status = 1"
                ));
        $checkspm = \App\SpmDetail::where('kode_barang',$kode_barang)->where('kode_jenis_barang',$kode_jenis_barang)->where('edisi',$edisi)->first();
        return view('Proses\Tiras\Detail.edit', compact('masa','statusaktif','tirasheader','tirasdetail','barang','Jenisbarang','checkInSpm', 'daftar','checkspm'));
    }
    public function update( Request $request, $id_tiras,$kode_barang,$kode_jenis_barang,$edisi)
    {
        // dd($request->all());
        if($request['id_tiras'] == ''){
        if($request['kode_barang'] == '')
        if($request['kode_jenis_barang'] == '')
        if($request['edisi'] == '') 
            $check = \App\Models\Tiras\Tiras_Detail::where('id_tiras',$request['id_tiras'])->where('kode_barang',$request['kode_barang'])->where('kode_jenis_barang',$request['kode_jenis_barang'])->where('edisi',$request['edisi'])->first();
        if($check) {
            return redirect()->back()->with('error','Data Yang Anda Masukan Sudah Tersedia');
            }
        }
        if($request['id_tiras'] == ''){
        if($request['kode_barang'] == '')
        if($request['kode_jenis_barang'] == '')
        if($request['edisi'] == '') 
            $check = \App\Models\Tiras\Tiras_Detail::where('id_tiras',$request['id_tiras'])->where('kode_barang',$request['kode_barang'])->where('kode_jenis_barang',$request['kode_jenis_barang'])->where('edisi',$request['edisi'])->first();
        if($check) {
            return $update = [
            // 'kode_jenis_barang'=> $request ['kode_jenis_barang'],
            // 'kode_barang'=> $request ['kode_barang'],
            // 'edisi'=> $request ['edisi'],
            'cetakan'=> $request ['cetakan'],
            'stok'=> $request ['stok'],
            'usulan_tiras'=> $request ['usulan_tiras'],
            'jml_tiras'=> $request ['jml_tiras'],
            'keterangan'=> $request ['keterangan'],
            'user_change' => auth()->user()->name,
            'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
            ];
            }      
            }         
        else
        {
            $update=[
            'id_tiras'=> $request ['id_tiras'],
            'kode_jenis_barang'=> $request ['kode_jenis_barang'],
            'kode_barang'=> $request ['kode_barang'],
            'edisi'=> $request ['edisi'],
            'cetakan'=> $request ['cetakan'],
            'stok'=> $request ['stok'],
            'usulan_tiras'=> $request ['usulan_tiras'],
            'jml_tiras'=> $request ['jml_tiras'],
            'keterangan'=> $request ['keterangan'],
            'user_change' => auth()->user()->name,
            'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
            ];           
        }

         \App\Models\Tiras\Tiras_Detail::where('id_tiras', $id_tiras)->where('kode_barang', $kode_barang)->where('kode_jenis_barang', $kode_jenis_barang)->where('edisi', $edisi)->update($update);

        return redirect()->to('tiras/tampil/'. $id_tiras );
    }
    // }
        // }
    public function destroy($id_tiras,$kode_barang, $kode_jenis_barang, $edisi)
    {
        $checkInSPM = \App\SpmDetail::where('kode_barang', $kode_barang)->where('kode_jenis_barang', $kode_jenis_barang)->where('edisi', $edisi)->get();

        $checkInSPM = \App\SpmDetail::where('kode_barang', $kode_barang)->where('kode_jenis_barang', $kode_jenis_barang)->where('edisi', $edisi)->get();

        if(count($checkInSPM)) {

            return redirect()->back()->with('error','Anda Tidak Dapat Menghapus Data Ini, Data Telah Digunakan Untuk Data Lain.');

        } else if(count($checkInSPM)) {

            return redirect()->back()->with('error','Anda Tidak Dapat Menghapus Data Ini, Data Telah Digunakan Untuk Data Lain.');

        } else {

        $thisIs = \App\Models\Tiras\Tiras_Detail::where('id_tiras',$id_tiras)->where('kode_barang',$kode_barang)->where('kode_jenis_barang', $kode_jenis_barang)->where('edisi', $edisi);
    }
  
        // Create new Notification
        $notif = new Notifikasi;
        $log = 'menghapus barang '.$kode_barang;
        $notifikasi = $notif->CreateNotifikasi(auth()->user()->getProfile->name, 1, $log);
        $thisIs->delete();
      
         return redirect()->to('tiras/tampil/'. $id_tiras);
    }


}
