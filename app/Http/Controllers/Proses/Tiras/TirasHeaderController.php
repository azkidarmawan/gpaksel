<?php

namespace App\Http\Controllers\Proses\Tiras;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Notifikasi;
use App\Models\Master\Fakultas;
use App\Models\Master\Masa;
use App\Models\Master\Status_Aktif;
use App\Models\Tiras\Tiras_Header;
use App\Models\Tiras\Tiras_Detail;
use App\Models\Master\StatusTiras;

class TirasHeaderController extends Controller
{
    public function __contruct()
    {
    	$this->middleware('auth');
    }
    public function unggah(Request $request,$keynum,$id_tiras)
    {
        // dd($id_tiras);
        $target_dir = "ut/uploads/Tiras/";
        $target_file = $target_dir . basename($_FILES["file".$keynum]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $check = \App\Models\Tiras\Tiras_Header::where('id_tiras',$id_tiras)->first();
        $no_surat_tiras = explode("/", $check->no_surat_tiras);
        $filename = $no_surat_tiras[0] . '-' . $no_surat_tiras[3];
        $target =  $target_dir . $filename . '.pdf';
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target)) {
            $filename = $check->file . 'I';
            $target = $target_dir . $filename . '.pdf';
        }
        // Allow certain file formats
        if($imageFileType != "pdf" ) {
            return redirect()->back()->with('error','Maaf, hanya file PDF yang bisa diunggah.');
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["file".$keynum]["tmp_name"], $target)) {
                $update = [
                'file' => $filename,
                'user_change' => auth()->user()->name,
                'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
                ];

            \App\Models\Tiras\Tiras_Header::where('id_tiras', $id_tiras)->update($update);
                return redirect()->back()->with('success','File berhasil diunggah.');
                
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
        return redirect()->to(config('spider.config.route_prefix').'/tiras');
    }
    public function index()
    {

        $tiras=DB::table('t_tiras_header')
        ->join('m_fakultas', 'm_fakultas.kode_fakultas','=','t_tiras_header.kode_fakultas')
        ->select('t_tiras_header.*', 'm_fakultas.nama_pendek')
        ->get();
        // $tiras = \App\Models\Tiras\Tiras_Header::orderBy('id_tiras')->get();
	// Mengupdate id_notif pada user
	$data = new Notifikasi;
	$daftar = $data->DaftarNotifikasi(auth()->user()->getProfile->id_notif,auth()->user()->getProfile->notifikasi);
	$jumlah = count($daftar);
	if($jumlah > 0)
	$update = $data->UpdateNotifikasi(auth()->user()->getProfile->id,$daftar[0]->id);
    $downloads=DB::table('t_tiras_header')->get();
   return view('Proses\Tiras\Header.index',compact('tiras', 'downloads'));  
    }

public function create()
    {
        $fakultas= \App\Models\Master\Fakultas::all();
        $masa= \App\Models\Master\masa::all();
        $statustiras= \App\Models\Master\StatusTiras::all();
        $tiras= \App\Models\Tiras\Tiras_Header::where('kode_status_tiras',2)->get(); 

        $spm = new Tiras_Header;
        $last_spm = $spm->LastSpm();
        // cek apakah sudah ada SPM pd DB atau blm
        if(count($last_spm) == 0){
            $no_surat_tiras = '0001/UN31.3/TRS/PP/'.date("Y");
        } else {
            $surat = explode("/", $last_spm[0]->no_surat_tiras);
            // cek apakah sudah berganti tahun
            if($surat[4] != date("Y")){
                $no_surat_tiras = '0001/UN31.3/TRS/PP/'.date("Y");
            } else {
                $kode = (int) $surat[0];
                $kode = $kode + 1;
                if($kode < 10)
                    $no_surat_tiras = '000'.$kode.'/UN31.3/TRS/PP/'.date("Y");
                else if($kode > 99)
                    $no_surat_tiras = $kode.'/UN31.3/TRS/PP/'.date("Y");
                else
                    $no_surat_tiras = '00'.$kode.'/UN31.3/TRS/PP/'.date("Y");
            }
            
        }

        return view('Proses\Tiras\Header.add', compact('fakultas','masa','tiras','no_surat_tiras', 'statustiras'));
    }

    public function store(Request $request) 
    {
        // dd($request->all());
        if($request['no_surat_tiras'] != '') {
            $check = \App\Models\Tiras\Tiras_Header::where('no_surat_tiras',$request['no_surat_tiras'])->first();
        if($check) {
            return redirect()->back()->with('error','Nomor Surat Sudah Ada');
            }

        $this->validate($request, [
        'no_surat_tiras' => 'required',
        'ketua_rapat' => 'required',
        'tgl_rapat' => 'required',


        ]);

        $old = explode("/", $request ['masa']);
        $masa = $old[0].'-'.$old[1];

        $tambah = new \App\Models\Tiras\Tiras_Header();
        $tambah->masa = $masa;
        $tambah->no_surat_tiras = $request['no_surat_tiras'];
        $tambah->ketua_rapat = $request['ketua_rapat'];
        $tambah->jabatan = $request['jabatan'];
        $tambah->nip = $request['nip'];
        $tambah->nip = $request['file'];
        $tambah->kode_status_tiras = $request['kode_status_tiras'];
        $tambah->kode_fakultas = $request['kode_fakultas'];
        $tambah->tgl_rapat = date('Y-m-d', strtotime($request['tgl_rapat']));
        $tambah->user_create =auth()->user()->name;
        $tambah->user_date_create =\Carbon\Carbon::Now('Asia/Jakarta');
        $tambah->save();

        return redirect()->to(config('spider.config.route_prefix').'/tiras');
        }
    }

     public function edit($no_surat)
    {
        $checkInDetail = \App\Models\Tiras\Tiras_Detail::where('id_tiras', $no_surat)->first();

        // $checkInHeader = \App\Models\Master\Fakultas::where('nomor_spc', $nomor_spc)->first();

        $ubahdata = \App\Models\Tiras\Tiras_Header::find($no_surat);
        $fakultas= \App\Models\Master\Fakultas::all();
        $masa= \App\Models\Master\masa::all();
        $statustiras= \App\Models\Master\StatusTiras::all();

        return view('Proses\Tiras\Header.edit',compact('ubahdata','fakultas','masa','checkInDetail','statustiras'));

    }

    public function update( Request $request, $no_surat)
    {
        // dd($request->all());
        if($request['id_tiras'] == '')
        {
            $old = explode("/", $request ['masa']);
            $masa = $old[0].'-'.$old[1];

            $update = [
            
            'ketua_rapat'=> $request ['ketua_rapat'],
            'jabatan'=> $request ['jabatan'],
            'nip'=> $request ['nip'],
            'tgl_rapat'=> $request ['tgl_rapat'],
            'masa'=> $masa,
            'kode_fakultas'=> $request ['kode_fakultas'], 
            'kode_status_tiras'=> $request ['kode_status_tiras'],       
            'user_change' => auth()->user()->name,
            'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
            ];
        }
        
        else
        {
            $old = explode("/", $request ['masa']);
            $masa = $old[0].'-'.$old[1];

            $update=[
            'no_surat_tiras' => $request['no_surat_tiras'],
            'ketua_rapat'=> $request ['ketua_rapat'],
            'jabatan'=> $request ['jabatan'],
            'nip'=> $request ['nip'],
            'tgl_rapat'=> $request ['tgl_rapat'],
            'masa'=> $masa,
            'kode_fakultas'=> $request ['kode_fakultas'],
            'kode_status_tiras'=> $request ['kode_status_tiras'],
            'user_change' => auth()->user()->name,
            'user_date_change' => \Carbon\Carbon::Now('Asia/Jakarta')
            ];
        }

        \App\Models\Tiras\Tiras_Header::where('id_tiras', $no_surat)->update($update);
        return redirect()->to(config('spider.config.route_prefix').'/tiras');
    }


    public function destroy($no_surat)
    {

        $LakukanCheckTirasDetail = \App\Models\Tiras\Tiras_Detail::where('id_tiras',$no_surat)->get();
        $LakukanChechSpmHeader = \App\Models\Tiras\Tiras_Detail::where('id_tiras',$no_surat)->get();

        if(count($LakukanCheckTirasDetail ))
        {
            return redirect()->back()->with('error','Data Ini Memiliki Detail');
        }
        elseif (count($LakukanChechSpmHeader )) 
        {
            return redirect()->back()->with('error','Data Ini Meliki Spm');
        }
        else
        {
            $ini= \App\Models\Tiras\Tiras_Header::find($no_surat);
            $ini->delete();

            return redirect()->to(config('spider.config.route_prefix').'/tiras');
        }
    }

    public function show($no_surat)
    {
        $menampilkan=DB::table('t_tiras_detail')
        ->join('m_jenis_barang', 'm_jenis_barang.kode_jenis_barang','=','t_tiras_detail.kode_jenis_barang')
        ->select('t_tiras_detail.*', 'm_jenis_barang.nama_jenis_barang')
        ->where('id_tiras', $no_surat)
        ->get();
        $id_tiras = $no_surat;
        $daftar = DB::select( DB::raw("SELECT * FROM t_tiras_header WHERE id_tiras = :id_tiras"), array(
                'id_tiras' => $id_tiras,
                ));
       $barang = \App\BahanAjar::all();
         return view('Proses\Tiras\Header.tampil', compact('menampilkan','no_surat', 'daftar', 'barang'));
    }

}
