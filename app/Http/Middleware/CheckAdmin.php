<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$role = auth()->user()->getProfile->roles;
        if ($role != 'admin') {
            return new Response(view('unauthorized'));;
        }
        return $next($request);
    }
}
