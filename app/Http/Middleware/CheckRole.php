<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$role = auth()->user()->getProfile->roles;
        if ($role != 'admin' && $role != 'admin_tiras' && $role != 'admin_spm' && $role != 'admin_spc' && $role != 'admin_terima') {
            return new Response(view('unauthorized'));
        }

        return $next($request);
    }
}
