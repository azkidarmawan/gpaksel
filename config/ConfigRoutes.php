<?php

namespace Ken\SpiderAdmin\Config;

use App\Notifikasi;

trait ConfigRoutes
{
    protected function prefixRoutes()
    {
        $this->config('spider.config.route_prefix');
    }

    protected function loginPath()
    {
        $this->prefixRoutes().'/login';
    }

    protected function redirectTo()
    {
        $this->prefixRoutes().'/dashboard';
    }

    protected function redirectAfterLogout()
    {
		$data = new Notifikasi;
		$daftar = $data->DaftarNotifikasi(auth()->user()->getProfile->id_notif,auth()->user()->getProfile->notifikasi);
		$jumlah = count($daftar);
		if($jumlah > 0)
		$update = $data->UpdateNotifikasi(auth()->user()->getProfile->id,$daftar[$jumlah-1]->id);
		
        $this->prefixRoutes();
    }
}