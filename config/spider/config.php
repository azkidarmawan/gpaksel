<?php

return [
	'title_name'            => 'GP-Aksel',
	'title_name_login'      => 'GP-Aksel',
	'route_prefix'          => 'gpa',
	'application_name_mini' => '<b><i>GP</i></b>',
	'application_name'      => '<b>GP</b><i>-Aksel</i>',
	'developer_web'         => 'gp-aksel.co.id',
	'developer_name'        => 'GP AKSEL TANGSEL'
];
