@extends('spider::layouts.apps')
@section('content')



<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>

    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="{{ url('absen-guru')}}"><i class="fa fa-circle-o"></i> Absensi Guru</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="form-group"
    <div class="col-md-12">
    <input type="hidden" id="act" value="{{ auth()->user()->getProfile->roles }}">
      <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
            <div class="box-tools pull-right">
			  <a href="{{ url('absen-guru/add') }}" id="btn-add" class="btn btn-xs btn-flat btn-primary"><i class="fa fa-edit"></i> Tambah Data</a>
            </div>
          </div>
            <div id="list" class="box-body">
              <table id="product-table" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <!-- <th width="10px">No</th> -->
                    <th>Tahun</th>
          					<th>Bulan</th>
                    <th>Minggu</th>
                    <th>Sesi 1</th>
                    <th>Sesi 2</th>
                    <th>Sesi 3</th>
                    <th>Sesi 4</th>
                    <th>Sesi 5</th>
                    <th>Sesi 6</th>
          					@if (auth()->user()->getProfile->roles == 'admin')
          					<th id="action">Aksi</th>
          					@endif
                  </tr>
                </thead>
                <tbody>
                @foreach($datas as $data)
                <tr>
                    <td>{{ $data->tahun }}</td>
                    <td>{{ $data->bulan }}</td>
                    <td>{{ $data->minggu }}</td>
                    <td>{{ $data->sesi1 }}</td>
                    <td>{{ $data->sesi2 }}</td>
                    <td>{{ $data->sesi3 }}</td>
                    <td>{{ $data->sesi4 }}</td>
                    <td>{{ $data->sesi5 }}</td>
                    <td>{{ $data->sesi6 }}</td>
          					@if (auth()->user()->getProfile->roles == 'admin')
          					<td style="white-space: nowrap;">
          					  <center>
          						<a href="{{ url('absen-guru/edit', $data->tahun) }}/{{$data->bulan}}/{{$data->minggu}}" class="btn btn-xs btn-flat btn-info" style="min-width: 45px" >Ubah</a>
          						<a href="{{ url('absen-guru/delete', $data->tahun) }}/{{$data->bulan}}/{{$data->minggu}}" type="button" class="btn btn-xs btn-flat btn-danger" style="min-width: 45px" onclick="return confirm('Apakah Anda yakin?')">Hapus</a>
          					  </center>
                    </td>
					          @endif
                </tr>
                 @endforeach
                </tbody>
              </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>

</section><!-- /.content -->
@endsection

@section('css')

<style>
  .table-bordered , th, td, tr{
    border: 1px solid #e3e3e3 !important;
  }
  th,td {
    text-align: center;
  }
</style>
@endsection

@section('script')
<script>
  	
	
  $(function(){
	var role = $('#act').val();
	if(role == 'user'){
		document.getElementById("btn-add").remove();
	}
	
    @if(Session::has('error'))
      swal({
        title:"Gagal",
        text:"{{ Session::get('error') }}",
        type:"error",
        // timer:2000,// optional
        showConfirmButton:true // set to true or false
      });
    @endif
    $('#product-table').DataTable({
      "order": []
    });
  });

  function hapus(){
    swal({
  title: 'Hapus data',
  text: "Apakah Anda yakin?",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Ya, hapuskan!',
  cancelButtonText: 'Tidak, batalkan!',
  confirmButtonClass: 'btn btn-success',
  cancelButtonClass: 'btn btn-danger',
  buttonsStyling: false
}).then(function () {
  swal(
    'Berhasil!',
    'Data berhasil dihapus.',
    'success'
  )
}, function (dismiss) {
  // dismiss can be 'cancel', 'overlay',
  // 'close', and 'timer'
  if (dismiss === 'cancel') {
    swal(
      'Dibatalkan',
      'Data tidak terhapus :)',
      'error'
    )
  }
})
  }
</script>
@stop