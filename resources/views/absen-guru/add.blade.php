@extends('spider::layouts.apps')
@section('content')



<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="{{ url('absen-guru')}}"><i class="fa fa-circle-o"></i> Absen Guru</a></li>
    <li class="active"><a href="{{ url('absen-guru/add')}}"><i class="fa fa-plus"></i> Tambah Data</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title" style="margin-left: 1vw">Tambah Absen Guru</h3>
        <div class="box-tools pull-right">
          <a href="{{ url('absen-guru')}}" class="btn btn-xs btn-flat btn-primary"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12" style="margin-left: 1vw">
            <form class="form-horizontal" action="{{ url('absen-guru/store') }}" method="POST">
            {!! csrf_field() !!}
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-1">
                <label>Tahun</label>
              </div>
              <div class="col-md-3">
                <select class="select" name="tahun" style="width: 40%" required="">
                  <option value="{{(int)date("Y")+1}}">{{(int)date("Y")+1}}</option>
                  <option selected="" value="{{date("Y")}}">{{date("Y")}}</option>
                  <option value="{{(int)date("Y")-1}}">{{(int)date("Y")-1}}</option>
                  <option value="{{(int)date("Y")-2}}">{{(int)date("Y")-2}}</option>
                </select>
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
              <div class="col-md-1">
                <label>Bulan</label>
              </div>
              <div class="col-md-3">
                <select class="select" name="bulan" style="width: 50%" required="">
                  <option @if(date("m") == '01') selected="" @endif value="Januari">Januari</option>
                  <option @if(date("m") == '02') selected="" @endif value="Februari">Februari</option>
                  <option @if(date("m") == '03') selected="" @endif value="Maret">Maret</option>
                  <option @if(date("m") == '04') selected="" @endif value="April">April</option>
                  <option @if(date("m") == '05') selected="" @endif value="Mei">Mei</option>
                  <option @if(date("m") == '06') selected="" @endif value="Juni">Juni</option>
                  <option @if(date("m") == '07') selected="" @endif value="Juli">Juli</option>
                  <option @if(date("m") == '08') selected="" @endif value="Agustus">Agustus</option>
                  <option @if(date("m") == '09') selected="" @endif value="September">September</option>
                  <option @if(date("m") == '10') selected="" @endif value="Oktober">Oktober</option>
                  <option @if(date("m") == '11') selected="" @endif value="November">November</option>
                  <option @if(date("m") == '12') selected="" @endif value="Desember">Desember</option>
                </select>
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
              <div class="col-md-1">
                <label>Minggu</label>
              </div>
              <div class="col-md-3">
                <input type="number" name="minggu" style="width: 20%" min="1" max="99" value="2" required="">
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div id="list" class="box-body">
                <table class="table table-striped table-bordered" style="width: 97%">
                  <thead>
                    <tr>
                      <th style="text-align: center;width: 16%">Sesi 1</th>
                      <th style="text-align: center;width: 16%">Sesi 2</th>
                      <th style="text-align: center;width: 16%">Sesi 3</th>
                      <th style="text-align: center;width: 16%">Sesi 4</th>
                      <th style="text-align: center;width: 16%">Sesi 5</th>
                      <th style="text-align: center;width: 16%">Sesi 6</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <select class="select" name="sesi1" style="width: 100%">
                          <option value="0"> Pilih Guru </option>
                          @foreach($gurus as $data)
                            <option value="{{$data->kode_guru}}">{{$data->nama_guru}}</option>
                          @endforeach
                        </select>
                      </td>
                      <td>
                        <select class="select" name="sesi2" style="width: 100%">
                          <option value="0"> Pilih Guru </option>
                          @foreach($gurus as $data)
                            <option value="{{$data->kode_guru}}">{{$data->nama_guru}}</option>
                          @endforeach
                        </select>
                      </td>
                      <td>
                        <select class="select" name="sesi3" style="width: 100%">
                          <option value="0"> Pilih Guru </option>
                          @foreach($gurus as $data)
                            <option value="{{$data->kode_guru}}">{{$data->nama_guru}}</option>
                          @endforeach
                        </select>
                      </td>
                      <td>
                        <select class="select" name="sesi4" style="width: 100%">
                          <option value="0"> Pilih Guru </option>
                          @foreach($gurus as $data)
                            <option value="{{$data->kode_guru}}">{{$data->nama_guru}}</option>
                          @endforeach
                        </select>
                      </td>
                      <td>
                        <select class="select" name="sesi5" style="width: 100%">
                          <option value="0"> Pilih Guru </option>
                          @foreach($gurus as $data)
                            <option value="{{$data->kode_guru}}">{{$data->nama_guru}}</option>
                          @endforeach
                        </select>
                      </td>
                      <td>
                        <select class="select" name="sesi6" style="width: 100%">
                          <option value="0"> Pilih Guru </option>
                          @foreach($gurus as $data)
                            <option value="{{$data->kode_guru}}">{{$data->nama_guru}}</option>
                          @endforeach
                        </select>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <br>
            <button type="submit" class = 'btn btn-primary' style="margin-bottom: 0.5vw">Simpan</button>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
</section><!-- /.content -->
@endsection

@section('css')

<style>
  .table-bordered , th, td, tr{
    border: 1px solid #e3e3e3 !important;

  }
</style>
@endsection

@section('script')
<script>
  $(function(){
    @if(Session::has('error'))
      swal({
        title:"Maaf",
        text:"{{ Session::get('error') }}",
        type:"error",
        // timer:2000,// optional
        showConfirmButton:true // set to true or false
      });
    @endif
    $('.select').select2();
  });

  function hapus(){
    swal({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, cancel!',
  confirmButtonClass: 'btn btn-success',
  cancelButtonClass: 'btn btn-danger',
  buttonsStyling: false
}).then(function () {
  swal(
    'Deleted!',
    'Your file has been deleted.',
    'success'
  )
}, function (dismiss) {
  // dismiss can be 'cancel', 'overlay',
  // 'close', and 'timer'
  if (dismiss === 'cancel') {
    swal(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})
  }
</script>
@stop