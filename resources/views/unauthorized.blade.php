@extends('spider::layouts.apps')
@section('content')
{{-- You can create your content in here or you can create new file like this file --}}

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>

    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-exclamation-triangle fa-3x"></i> WARNING!</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<h1>
	<small>
	</small>
</h1>
	<div class="form-group" style="margin-left:1%">
		<h3>
		Mohon maaf, Anda tidak diizinkan untuk mengakses url ini.
		</h3>
	</div>

<style>
.panel {
	margin-top: 0cm;
	margin-bottom: 0cm;
	padding-top: 0cm;
	padding-bottom: 0cm;
}

.box-footer {
    margin-top: 0cm;
	margin-bottom: 0cm;
	padding-top: 0cm;
	padding-bottom: 0cm;
}

.panel-green {
    border-color: #5cb85c;
}

.panel-green .panel-heading {
    border-color: #5cb85c;
    color: #fff;
    background-color: #5cb85c;
}

.panel-green a {
    color: #5cb85c;
}

.panel-green a:hover {
    color: #3d8b3d;
}

.panel-red {
    border-color: #d9534f;
}

.panel-red .panel-heading {
    border-color: #d9534f;
    color: #fff;
    background-color: #d9534f;
}

.panel-red a {
    color: #d9534f;
}

.panel-red a:hover {
    color: #b52b27;
}

.panel-yellow {
    border-color: #f0ad4e;
}

.panel-yellow .panel-heading {
    border-color: #f0ad4e;
    color: #fff;
    background-color: #f0ad4e;
}

.panel-yellow a {
    color: #f0ad4e;
}

.panel-yellow a:hover {
    color: #df8a13;
}
</style>

</section><!-- /.content -->
@endsection

@section('script')
<script>
  @if(Session::has('success'))
    swal({
      type: "success",
      text: "Selamat Datang di Sistem Distribusi UT",
      title: "Welcome !!!"
    });
  @endif
</script>
@stop