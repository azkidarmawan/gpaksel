@extends('spider::layouts.apps')
@section('content')



<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="{{ url('kelompok')}}"><i class="fa fa-circle-o"></i> Kelompok</a></li>
    <li class="active"><a href="{{ url('kelompok/add')}}"><i class="fa fa-plus"></i> Tambah Data</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title" style="margin-left: 1vw">Tambah Kelompok</h3>
        <div class="box-tools pull-right">
          <a href="{{ url('kelompok')}}" class="btn btn-xs btn-flat btn-primary"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12" style="margin-left: 1vw">
            <form class="form-horizontal" action="{{ url('kelompok/store') }}" method="POST">
            {!! csrf_field() !!}
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Desa</label>
              </div>
              <div class="col-md-8">
                <Select class="select" name="kode_desa" style="width: 50%" required="">
                  <option value=""> -- Pilih Desa -- </option>
                  @foreach($datas as $key=>$data)
                    <option value="{{$data->kode_desa}}">{{$data->nama_desa}}</option>
                  @endforeach
                </Select>
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Nama Kelompok</label>
              </div>
              <div class="col-md-8">
                <input type="text" name="nama_kelompok" style="width: 50%" maxlength="50" required="">
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Kyai Kelompok</label>
              </div>
              <div class="col-md-8">
                <input type="text" name="kyai_kelompok" style="width: 50%" maxlength="100" required="">
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Nomor Telepon</label>
              </div>
              <div class="col-md-8">
                <input type="text" data-inputmask="'mask': '9999 9999 99999'" name="no_telp" style="width: 50%">
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Keterangan</label>
              </div>
              <div class="col-md-8">
                <input type="text" name="keterangan" style="width: 50%" maxlength="100">
              </div>
            </div>
            <br>
            <button type="submit" class = 'btn btn-primary' style="margin-bottom: 0.5vw">Simpan</button>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
</section><!-- /.content -->
@endsection

@section('css')

<style>
  .table-bordered , th, td, tr{
    border: 1px solid #e3e3e3 !important;

  }
</style>
@endsection

@section('script')
<script>
  $(function(){
    @if(Session::has('error'))
      swal({
        title:"Maaf",
        text:"{{ Session::get('error') }}",
        type:"error",
        // timer:2000,// optional
        showConfirmButton:true // set to true or false
      });
    @endif
    $( ".select" ).select2();
    $(":input").inputmask();
  });

  function hapus(){
    swal({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, cancel!',
  confirmButtonClass: 'btn btn-success',
  cancelButtonClass: 'btn btn-danger',
  buttonsStyling: false
}).then(function () {
  swal(
    'Deleted!',
    'Your file has been deleted.',
    'success'
  )
}, function (dismiss) {
  // dismiss can be 'cancel', 'overlay',
  // 'close', and 'timer'
  if (dismiss === 'cancel') {
    swal(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})
  }
</script>
@stop