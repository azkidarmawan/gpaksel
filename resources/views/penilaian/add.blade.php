@extends('spider::layouts.apps')
@section('content')



<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="{{ url('penilaian')}}"><i class="fa fa-circle-o"></i> Penilaian</a></li>
    <li class="active"><a href="{{ url('penilaian/add')}}"><i class="fa fa-plus"></i> Tambah Data</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title" style="margin-left: 1vw">Tambah Penilaian</h3>
        <div class="box-tools pull-right">
          <a href="{{ url('penilaian')}}" class="btn btn-xs btn-flat btn-primary"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
          <a href="#" onclick="reset()" class="btn btn-xs btn-flat btn-danger"><i class="fa fa-refresh"></i> Reset Variable Nilai</a>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12" style="margin-left: 1vw">
            <form class="form-horizontal" action="{{ url('penilaian/store') }}" method="POST">
            {!! csrf_field() !!}
            <div class="row" style="margin-bottom: 1vw">
              <div class="col-md-3">
                <label>Semester</label>
                <input type="number" name="semester" min="1" max="99" required="" style="width: 20%">
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
              <div class="col-md-5">
                <label>Murid</label>
                <select class="select" name="kode_murid" style="width: 60%" required="">
                  <option value=""> Pilih Murid </option>
                  @foreach($murids as $key=>$data)
                    <option value="{{$data->kode_murid}}">{{$data->nama_lengkap}}</option>
                  @endforeach
                </select>
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
              <div class="col-md-4">
                <label>Wali Kelas</label>
                <select class="select" name="kode_guru" style="width: 40%" required="">
                  <option value=""> Pilih Guru </option>
                  @foreach($gurus as $key=>$data)
                    <option value="{{$data->kode_guru}}">{{$data->nama_guru}}</option>
                  @endforeach
                </select>
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
            </div>
            <div class="row" style="margin-left: 0.4vw">
              <strong><h3>A. Prestasi Bacaan</h3></strong>
              <table id="bacaan-table" class="table table-bordered table-striped table-hover" style="width: 97%">
                <thead>
                  <tr>
                    <th colspan="4">Kategori Penilaian</th>
                    <th rowspan="2" style="vertical-align: middle;">Rata Rata</th>
                  </tr>
                  <tr>
                    <th>Fashohah</th>
                    <th>Tajwid</th>
                    <th>Tartil</th>
                    <th>Suara & Lagu</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);rataRata()" onkeyup="this.value=minmax(this.value);rataRata()" type="number" name="fashohah" min="0" max="100" value="0"></td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);rataRata()" onkeyup="this.value=minmax(this.value);rataRata()" type="number" name="tajwid" min="0" max="100" value="0"></td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);rataRata()" onkeyup="this.value=minmax(this.value);rataRata()" type="number" name="ghorib_muskhilat" min="0" max="100" value="0"></td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);rataRata()" onkeyup="this.value=minmax(this.value);rataRata()" type="number" name="suara_lagu" min="0" max="100" value="0"></td>
                    <td><input type="number" name="rata_rata" min="0" max="100" value="0" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="row" style="margin-left: 0.4vw">
              <strong><h3>B. Prestasi Hafalan</h3></strong>
              <table id="doa-table" class="table table-bordered table-striped table-hover" style="width: 97%">
                <thead>
                  <tr>
                    <th colspan="3" style="font-size: 16px">Hafalan Doa</th>
                  </tr>
                  <tr>
                    <th>Nama Doa</th>
                    <th>Nilai</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody id="doa_body">
                <input type="hidden" id="id_doa" name="id_doa" value="{{count($doa)}}">
                @foreach($doa as $key=>$data)
                  <tr>
                    <td><input style="width: 100%;" type="text" name="doa{{$key}}" value="{{$data->doa}}"></td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganHafal(this.value,'status_doa{{$key}}')" onkeyup="this.value=minmax(this.value);keteranganHafal(this.value,'status_doa{{$key}}')" type="number" name="nilai_doa{{$key}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_doa{{$key}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                @endforeach
                  <tr>
                    <td><input style="width: 100%;" type="text" name="doa{{count($doa)}}" value=""></td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganHafal(this.value,'status_doa{{count($doa)}}')" onkeyup="this.value=minmax(this.value);keteranganHafal(this.value,'status_doa{{count($doa)}}')" type="number" name="nilai_doa{{count($doa)}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_doa{{count($doa)}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                </tbody>
              </table>
              <a id="tambah_doa" style="margin-top: 0px" class="btn btn-primary" onclick="tambahDoa({{count($doa)}})"><i class="fa fa-plus"></i> Tambah Hafalan Doa</a>
              <hr>
              <table id="dalil-table" class="table table-bordered table-striped table-hover" style="width: 97%">
                <thead>
                  <tr>
                    <th colspan="4" style="font-size: 16px">Hafalan Dalil</th>
                  </tr>
                  <tr>
                    <th>Nama Dalil</th>
                    <th>Jumlah</th>
                    <th>Nilai</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody id="dalil_body">
                <input type="hidden" id="id_dalil" name="id_dalil" value="{{count($dalil)}}">
                @foreach($dalil as $key=>$data)
                  <tr>
                    <td><input style="width: 100%;" type="text" name="dalil{{$key}}" value="{{$data->dalil}}"></td>
                    <td><input style="text-align: center;" type="number" name="jml_dalil{{$key}}" value="{{$data->jumlah}}" min="1" max="100"></td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganHafal(this.value,'status_dalil{{$key}}')" onkeyup="this.value=minmax(this.value);keteranganHafal(this.value,'status_dalil{{$key}}')" type="number" name="nilai_dalil{{$key}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_dalil{{$key}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                @endforeach
                  <tr>
                    <td><input style="width: 100%;" type="text" name="dalil{{count($dalil)}}" value=""></td>
                    <td><input style="text-align: center;" type="number" name="jml_dalil{{count($dalil)}}" value="1" min="1" max="100"></td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganHafal(this.value,'status_dalil{{count($dalil)}}')" onkeyup="this.value=minmax(this.value);keteranganHafal(this.value,'status_dalil{{count($dalil)}}')" type="number" name="nilai_dalil{{count($dalil)}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_dalil{{count($dalil)}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                </tbody>
              </table>
              <a id="tambah_dalil" style="margin-top: 0px" class="btn btn-primary" onclick="tambahDalil({{count($dalil)}})"><i class="fa fa-plus"></i> Tambah Hafalan Dalil</a>
              <hr>
              <table id="surat-table" class="table table-bordered table-striped table-hover" style="width: 97%">
                <thead>
                  <tr>
                    <th colspan="3" style="font-size: 16px">Hafalan Surat</th>
                  </tr>
                  <tr>
                    <th>Nama Surat</th>
                    <th>Nilai</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody id="surat_body">
                <input type="hidden" id="id_surat" name="id_surat" value="{{count($surat)}}">
                @foreach($surat as $key=>$data)
                  <tr>
                    <td><input style="width: 100%;" type="text" name="surat{{$key}}" value="{{$data->surat}}"></td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganHafal(this.value,'status_surat{{$key}}')" onkeyup="this.value=minmax(this.value);keteranganHafal(this.value,'status_surat{{$key}}')" type="number" name="nilai_surat{{$key}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_surat{{$key}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                @endforeach
                  <tr>
                    <td>
                      <select style="width: 100%;" name="surat{{count($surat)}}" class="select">
                        <option value="">Pilih Surat</option><option value="An-Naba">An-Naba</option><option value="An-Naziat">An-Naziat</option><option value="Abasa">Abasa</option><option value="At-Takwir">At-Takwir</option><option value="Al-Infitar">Al-Infitar</option><option value="Al-Tatfif">Al-Tatfif</option><option value="Al-Insyiqaq">Al-Insyiqaq</option><option value="Al-Buruj">Al-Buruj</option><option value="At-Tariq">At-Tariq</option><option value="Al-Ala">Al-Ala</option><option value="Al-Gasyiyah">Al-Gasyiyah</option><option value="Al-Fajr">Al-Fajr</option><option value="Al-Balad">Al-Balad</option><option value="Asy-Syams">Asy-Syams</option><option value="Al-Lail">Al-Lail</option><option value="Ad-Duha">Ad-Duha</option><option value="Al-Insyirah">Al-Insyirah</option><option value="At-Tin">At-Tin</option><option value="Al-Alaq">Al-Alaq</option><option value="Al-Qadr">Al-Qadr</option><option value="Al-Bayyinah">Al-Bayyinah</option><option value="Az-Zalzalah">Az-Zalzalah</option><option value="Al-Adiyat">Al-Adiyat</option><option value="Al-Qariah">Al-Qariah</option><option value="At-Takasur">At-Takasur</option><option value="Al-Asr">Al-Asr</option><option value="Al-Humazah">Al-Humazah</option><option value="Al-Fil">Al-Fil</option><option value="Quraisy">Quraisy</option><option value="Al-Maun">Al-Maun</option><option value="Al-Kausar">Al-Kausar</option><option value="Al-Kafirun">Al-Kafirun</option><option value="An-Nasr">An-Nasr</option><option value="Al-Lahab">Al-Lahab</option><option value="Al-Ikhlas">Al-Ikhlas</option><option value="Al-Falaq">Al-Falaq</option><option value="An-Nas">An-Nas</option>
                      </select>
                    </td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganHafal(this.value,'status_surat{{count($surat)}}')" onkeyup="this.value=minmax(this.value);keteranganHafal(this.value,'status_surat{{count($surat)}}')" type="number" name="nilai_surat{{count($surat)}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_surat{{count($surat)}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                </tbody>
              </table>
              <a id="tambah_surat" style="margin-top: 0px" class="btn btn-primary" onclick="tambahSurat({{count($surat)}})"><i class="fa fa-plus"></i> Tambah Hafalan Surat</a>
              <a id="tambah_khusus" style="margin-top: 0px;margin-left: 20px" class="btn btn-primary" onclick="tambahKhusus({{count($surat)}})"><i class="fa fa-plus"></i> Tambah Hafalan Khusus</a>
              <br>
              <br>
            </div>
            <div class="row" style="margin-left: 0.4vw">
              <strong><h3>C. Prestasi Manqulan</h3></strong>
              <table id="quran-table" class="table table-bordered table-striped table-hover" style="width: 97%">
                <thead>
                  <tr>
                    <th colspan="3" style="font-size: 16px">Manqulan Quran</th>
                  </tr>
                  <tr>
                    <th>Nama Surat</th>
                    <th>Nilai</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody id="quran_body">
                <input type="hidden" id="id_quran" name="id_quran" value="{{count($quran)}}">
                @foreach($quran as $key=>$data)
                  <tr>
                    <td><input style="width: 100%;" type="text" name="quran{{$key}}" value="{{$data->quran}}"></td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganTuntas(this.value,'status_quran{{$key}}')" onkeyup="this.value=minmax(this.value);keteranganTuntas(this.value,'status_quran{{$key}}')" type="number" name="nilai_quran{{$key}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_quran{{$key}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                @endforeach
                  <tr>
                    <td>
                    <input type="text" style="width: 100%;" name="quran{{count($quran)}}">
                    </td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganTuntas(this.value,'status_quran{{count($quran)}}')" onkeyup="this.value=minmax(this.value);keteranganTuntas(this.value,'status_quran{{count($quran)}}')" type="number" name="nilai_quran{{count($quran)}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_quran{{count($quran)}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                </tbody>
              </table>
              <a id="tambah_quran" style="margin-top: 0px" class="btn btn-primary" onclick="tambahQuran({{count($quran)}})"><i class="fa fa-plus"></i> Tambah Manqulan Quran</a>
              <hr>
              <table id="hadits-table" class="table table-bordered table-striped table-hover" style="width: 97%">
                <thead>
                  <tr>
                    <th colspan="3" style="font-size: 16px">Manqulan Hadits</th>
                  </tr>
                  <tr>
                    <th>Nama Hadits</th>
                    <th>Nilai</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody id="hadits_body">
                <input type="hidden" id="id_hadits" name="id_hadits" value="{{count($hadits)}}">
                @foreach($hadits as $key=>$data)
                  <tr>
                    <td><input style="width: 100%;" type="text" name="hadits{{$key}}" value="{{$data->hadits}}"></td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganTuntas(this.value,'status_hadits{{$key}}')" onkeyup="this.value=minmax(this.value);keteranganTuntas(this.value,'status_hadits{{$key}}')" type="number" name="nilai_hadits{{$key}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_hadits{{$key}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                @endforeach
                  <tr>
                    <td>
                    <select style="width: 100%;" name="hadits{{count($hadits)}}" class="select">
                        <option value="">Pilih Hadits</option>
                        <option value="K. Manasikil Haji">K. Manasikil Haji</option><option value="K. Sholah">K. Sholah</option><option value="K. Faroidh">K. Faroidh</option><option value="K. Adilah">K. Adilah</option><option value="K. Imaroh">K. Imaroh</option><option value="K. Janaiz">K. Janaiz</option><option value="K. Ahkam">K. Ahkam</option><option value="K. Sholatin Nawafil">K. Sholatin Nawafil</option><option value="K. Manasik Wal Jihad">K. Manasik Wal Jihad</option><option value="K. Haji">K. Haji</option><option value="K. Jihad">K. Jihad</option><option value="K. Sifati Janahwanar">K. Sifati Janahwanar</option><option value="K. Da'wat">K. Da'wat</option><option value="Khotbah">Khotbah</option><option value="K. Shoum">K. Shoum</option><option value="K. Kanzil Umal">K. Kanzil Umal</option><option value="K. Adab">K. Adab</option>
                      </select>
                    </td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganTuntas(this.value,'status_hadits{{count($hadits)}}')" onkeyup="this.value=minmax(this.value);keteranganTuntas(this.value,'status_hadits{{count($hadits)}}')" type="number" name="nilai_hadits{{count($hadits)}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_hadits{{count($hadits)}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                </tbody>
              </table>
              <a id="tambah_hadits" style="margin-top: 0px" class="btn btn-primary" onclick="tambahHadits({{count($hadits)}})"><i class="fa fa-plus"></i> Tambah Manqulan Hadits</a>
              <br>
              <br>
            </div>
            <div class="row" style="margin-left: 0.4vw">
              <strong><h3>D. Materi Tambahan</h3></strong>
              <table id="tambahan-table" class="table table-bordered table-striped table-hover" style="width: 97%">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Nilai</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody id="tambahan_body">
                <input type="hidden" id="id_tambahan" name="id_tambahan" value="{{count($tambahan)}}">
                @foreach($tambahan as $key=>$data)
                  <tr>
                    <td><input style="width: 100%;" type="text" name="tambahan{{$key}}" value="{{$data->tambahan}}"></td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganTambahan(this.value,'status_tambahan{{$key}}')" onkeyup="this.value=minmax(this.value);keteranganTambahan(this.value,'status_tambahan{{$key}}')" type="number" name="nilai_tambahan{{$key}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_tambahan{{$key}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                @endforeach
                  <tr>
                    <td>
                      <input type="text" name="tambahan{{count($tambahan)}}" style="width: 100%;" value="">
                    </td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganTambahan(this.value,'status_tambahan{{count($tambahan)}}')" onkeyup="this.value=minmax(this.value);keteranganTambahan(this.value,'status_tambahan{{count($tambahan)}}')" type="number" name="nilai_tambahan{{count($tambahan)}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_tambahan{{count($tambahan)}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                </tbody>
              </table>
              <a id="tambah_tambahan" style="margin-top: 0px" class="btn btn-primary" onclick="tambahTambahan({{count($tambahan)}})"><i class="fa fa-plus"></i> Tambah Materi</a>
              <br>
              <br>
            </div>
            <div class="row" style="margin-left: 0.4vw">
              <strong><h3>E. Akhlaq & Kepribadian</h3></strong>
              <table id="akhlaq-table" class="table table-bordered table-striped table-hover" style="width: 97%">
                <thead>
                  <tr>
                    <th>Sikap</th>
                    <th>Nilai</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody id="akhlaq_body">
                <input type="hidden" id="id_akhlaq" name="id_akhlaq" value="{{count($akhlaq)}}">
                @foreach($akhlaq as $key=>$data)
                  <tr>
                    <td><input style="width: 100%;" type="text" name="akhlaq{{$key}}" value="{{$data->akhlaq}}"></td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganAkhlaq(this.value,'status_akhlaq{{$key}}')" onkeyup="this.value=minmax(this.value);keteranganAkhlaq(this.value,'status_akhlaq{{$key}}')" type="number" name="nilai_akhlaq{{$key}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_akhlaq{{$key}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                @endforeach
                  <tr>
                    <td>
                      <input type="text" name="akhlaq{{count($akhlaq)}}" style="width: 100%;" value="">
                    </td>
                    <td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganAkhlaq(this.value,'status_akhlaq{{count($akhlaq)}}')" onkeyup="this.value=minmax(this.value);keteranganAkhlaq(this.value,'status_akhlaq{{count($akhlaq)}}')" type="number" name="nilai_akhlaq{{count($akhlaq)}}" min="0" max="100" value=""></td>
                    <td><input type="text" name="status_akhlaq{{count($akhlaq)}}" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>
                  </tr>
                </tbody>
              </table>
              <a id="tambah_akhlaq" style="margin-top: 0px" class="btn btn-primary" onclick="tambahAkhlaq({{count($akhlaq)}})"><i class="fa fa-plus"></i> Tambah Sikap</a>
              <br>
              <br>
            </div>
            <div class="row" style="margin-left: 0.4vw">
              <strong><h3>F. Catatan Wali Kelas</h3></strong>
              <table id="catatan-table" class="table table-bordered table-striped table-hover" style="width: 97%">
                <thead>
                  <tr>
                    <th>Catatan Tambahan</th>
                  </tr>
                </thead>
                <tbody id="catatan_body">
                  <tr>
                    <td>
                      <textarea name="catatan" rows="5" maxlength="500" style="width: 100%;font-size: 18px"></textarea>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="row" style="margin-left: 0.4vw">
              <strong><h3>Keterangan</h3></strong>
              <table id="keterangan-table" class="table table-bordered table-striped table-hover" style="width: 97%">
                <thead>
                </thead>
                <tbody id="keterangan_body">
                  <tr>
                    <td>Keterangan</td>
                    <td>
                      <input name="keterangan" type="text" maxlength="50" style="width: 100%;font-size: 16px">
                    </td>
                  </tr>
                </tbody>
              </table>
              <hr>
            </div>
            <button type="submit" class = 'btn btn-success' style="margin-bottom: 0.5vw">Simpan</button>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
</section><!-- /.content -->
@endsection

@section('css')

<style>
  .selected{
    background-color: #a9b7d1 !important;
  }
  .table-bordered , th, td, tr{
    border: 1px solid #e3e3e3 !important;
  }
  th {
    text-align: center;
  }
  td {
    text-align: center;
    padding: 2px;
  }
  table { 
    border-spacing: 0;
    border-collapse: collapse;
  }
</style>
@endsection

@section('script')
<script>
  $(function(){
    @if(Session::has('error'))
      swal({
        title:"Maaf",
        text:"{{ Session::get('error') }}",
        type:"error",
        // timer:2000,// optional
        showConfirmButton:true // set to true or false
      });
    @endif
    $('.select').select2();
    $(":input").inputmask();

    var table = document.getElementById("absen-table");
 
    $('#absen-table tbody').on( 'click', 'tr', function () {
        // if ( $(this).hasClass('selected') ) {
        //     $(this).removeClass('selected');
        // }
        // else {
            $('.selected').removeClass('selected');
            // table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        // }
    });
  });

  function rataRata(){
    var fashohah = $('[name="fashohah"]').val();
    var tajwid = $('[name="tajwid"]').val();
    var ghorib_muskhilat = $('[name="ghorib_muskhilat"]').val();
    var suara_lagu = $('[name="suara_lagu"]').val();
    $('[name="rata_rata"]').val((parseInt(fashohah)+parseInt(tajwid)+parseInt(ghorib_muskhilat)+parseInt(suara_lagu))/4);
  }

  function keteranganHafal(value,id){
    // typeof(var) and/or var instanceof something
    if(typeof(id) == "string"){
      id = document.getElementsByName(id)[0];
    }
    if(value < 60)
      id.value = 'Qobikh';
    else if(value < 71)
      id.value = 'Maqbul';
    else if(value < 81)
      id.value = 'Jayyid';
    else if(value < 91)
      id.value = 'Jayyid Jiddan';
    else
      id.value = 'Mumtaz';
  }

  function keteranganTuntas(value,id){
    // typeof(var) and/or var instanceof something
    if(typeof(id) == "string"){
      id = document.getElementsByName(id)[0];
    }
    if(value < 25)
      id.value = 'Tidak Lengkap';
    else if(value < 50)
      id.value = 'Kurang Lengkap';
    else if(value < 85)
      id.value = 'Cukup Lengkap';
    else
      id.value = 'Khatam';
  }

  function keteranganAkhlaq(value,id){
    // typeof(var) and/or var instanceof something
    if(typeof(id) == "string"){
      id = document.getElementsByName(id)[0];
    }
    if(value < 25)
      id.value = 'Tidak Baik';
    else if(value < 50)
      id.value = 'Kurang Baik';
    else if(value < 70)
      id.value = 'Cukup';
    else if(value < 90)
      id.value = 'Baik';
    else
      id.value = 'Sangat Baik';
  }

  function keteranganTambahan(value,id){
    // typeof(var) and/or var instanceof something
    if(typeof(id) == "string"){
      id = document.getElementsByName(id)[0];
    }
    if(value < 25)
      id.value = 'Tidak Tercapai';
    else if(value < 50)
      id.value = 'Kurang Tercapai';
    else if(value < 85)
      id.value = 'Tuntas';
    else
      id.value = 'Sangat Tuntas';
  }

  function tambahDoa(id){
    id++;
    var baris_doa = '<td><input style="width: 100%;" type="text" name="doa'+id+'" value=""></td><td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganHafal(this.value,status_doa'+id+')" onkeyup="this.value=minmax(this.value);keteranganHafal(this.value,status_doa'+id+')" type="number" name="nilai_doa'+id+'" min="0" max="100" value=""></td><td><input type="text" name="status_doa'+id+'" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>';
    var body = document.getElementById("doa_body");
    var tr = document.createElement('tr');
    tr.innerHTML = baris_doa;
    // var elements = tr.childNodes;
    body.append(tr);
    document.getElementById("id_doa").value = id;
    document.getElementById("tambah_doa").setAttribute("onclick","tambahDoa("+id+")");
  }

  function tambahDalil(id){
    id++;
    var baris_dalil = '<td><input style="width: 100%;" type="text" name="dalil'+id+'" value=""></td><td><input style="text-align: center;" type="number" name="jml_dalil'+id+'" value="1" min="1" max="100"></td><td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganHafal(this.value,status_dalil'+id+')" onkeyup="this.value=minmax(this.value);keteranganHafal(this.value,status_dalil'+id+')" type="number" name="nilai_dalil'+id+'" min="0" max="100" value=""></td><td><input type="text" name="status_dalil'+id+'" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>';
    var body = document.getElementById("dalil_body");
    var tr = document.createElement('tr');
    tr.innerHTML = baris_dalil;
    // var elements = tr.childNodes;
    body.append(tr);
    document.getElementById("id_dalil").value = id;
    document.getElementById("tambah_dalil").setAttribute("onclick","tambahDalil("+id+")");
  }

  function tambahSurat(id){
    id++;
    var baris_surat = '<td><select style="width: 100%;" name="surat'+id+'" class="select"><option value="">Pilih Surat</option><option value="An-Naba">An-Naba</option><option value="An-Naziat">An-Naziat</option><option value="Abasa">Abasa</option><option value="At-Takwir">At-Takwir</option><option value="Al-Infitar">Al-Infitar</option><option value="Al-Tatfif">Al-Tatfif</option><option value="Al-Insyiqaq">Al-Insyiqaq</option><option value="Al-Buruj">Al-Buruj</option><option value="At-Tariq">At-Tariq</option><option value="Al-Ala">Al-Ala</option><option value="Al-Gasyiyah">Al-Gasyiyah</option><option value="Al-Fajr">Al-Fajr</option><option value="Al-Balad">Al-Balad</option><option value="Asy-Syams">Asy-Syams</option><option value="Al-Lail">Al-Lail</option><option value="Ad-Duha">Ad-Duha</option><option value="Al-Insyirah">Al-Insyirah</option><option value="At-Tin">At-Tin</option><option value="Al-Alaq">Al-Alaq</option><option value="Al-Qadr">Al-Qadr</option><option value="Al-Bayyinah">Al-Bayyinah</option><option value="Az-Zalzalah">Az-Zalzalah</option><option value="Al-Adiyat">Al-Adiyat</option><option value="Al-Qariah">Al-Qariah</option><option value="At-Takasur">At-Takasur</option><option value="Al-Asr">Al-Asr</option><option value="Al-Humazah">Al-Humazah</option><option value="Al-Fil">Al-Fil</option><option value="Quraisy">Quraisy</option><option value="Al-Maun">Al-Maun</option><option value="Al-Kausar">Al-Kausar</option><option value="Al-Kafirun">Al-Kafirun</option><option value="An-Nasr">An-Nasr</option><option value="Al-Lahab">Al-Lahab</option><option value="Al-Ikhlas">Al-Ikhlas</option><option value="Al-Falaq">Al-Falaq</option><option value="An-Nas">An-Nas</option></select></td><td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganHafal(this.value,status_surat'+id+')" onkeyup="this.value=minmax(this.value);keteranganHafal(this.value,status_surat'+id+')" type="number" name="nilai_surat'+id+'" min="0" max="100" value=""></td><td><input type="text" name="status_surat'+id+'" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>';
    var body = document.getElementById("surat_body");
    var tr = document.createElement('tr');
    tr.innerHTML = baris_surat;
    // var elements = tr.childNodes;
    body.append(tr);
    document.getElementById("id_surat").value = id;
    document.getElementById("tambah_surat").setAttribute("onclick","tambahSurat("+id+")");
    document.getElementById("tambah_khusus").setAttribute("onclick","tambahKhusus("+id+")");
    $('.select').select2();
  }

  function tambahKhusus(id){
    id++;
    var baris_surat = '<td><input type="text" style="width: 100%;" name="surat'+id+'"></td><td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganHafal(this.value,status_surat'+id+')" onkeyup="this.value=minmax(this.value);keteranganHafal(this.value,status_surat'+id+')" type="number" name="nilai_surat'+id+'" min="0" max="100" value=""></td><td><input type="text" name="status_surat'+id+'" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>';
    var body = document.getElementById("surat_body");
    var tr = document.createElement('tr');
    tr.innerHTML = baris_surat;
    // var elements = tr.childNodes;
    body.append(tr);
    document.getElementById("id_surat").value = id;
    document.getElementById("tambah_surat").setAttribute("onclick","tambahSurat("+id+")");
    document.getElementById("tambah_khusus").setAttribute("onclick","tambahKhusus("+id+")");
    $('.select').select2();
  }

  function tambahQuran(id){
    id++;
    var baris_quran = '<td><input type="text" style="width: 100%;" name="quran'+id+'"></td><td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganTuntas(this.value,status_quran'+id+')" onkeyup="this.value=minmax(this.value);keteranganTuntas(this.value,status_quran'+id+')" type="number" name="nilai_quran'+id+'" min="0" max="100" value=""></td><td><input type="text" name="status_quran'+id+'" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>';
    var body = document.getElementById("quran_body");
    var tr = document.createElement('tr');
    tr.innerHTML = baris_quran;
    // var elements = tr.childNodes;
    body.append(tr);
    document.getElementById("id_quran").value = id;
    document.getElementById("tambah_quran").setAttribute("onclick","tambahQuran("+id+")");
    $('.select').select2();
  }

  function tambahHadits(id){
    id++;
    var baris_hadits = '<td><select style="width: 100%;" name="hadits'+id+'" class="select"><option value="">Pilih Hadits</option><option value="K. Manasikil Haji">K. Manasikil Haji</option><option value="K. Sholah">K. Sholah</option><option value="K. Faroidh">K. Faroidh</option><option value="K. Adilah">K. Adilah</option><option value="K. Imaroh">K. Imaroh</option><option value="K. Janaiz">K. Janaiz</option><option value="K. Ahkam">K. Ahkam</option><option value="K. Sholatin Nawafil">K. Sholatin Nawafil</option><option value="K. Manasik Wal Jihad">K. Manasik Wal Jihad</option><option value="K. Haji">K. Haji</option><option value="K. Jihad">K. Jihad</option><option value="K. Sifati Janahwanar">K. Sifati Janahwanar</option><option value="K. Da`wat">K. Da`wat</option><option value="Khotbah">Khotbah</option><option value="K. Shoum">K. Shoum</option><option value="K. Kanzil Umal">K. Kanzil Umal</option><option value="K. Adab">K. Adab</option></select></td><td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganTuntas(this.value,status_hadits'+id+')" onkeyup="this.value=minmax(this.value);keteranganTuntas(this.value,status_hadits'+id+')" type="number" name="nilai_hadits'+id+'" min="0" max="100" value=""></td><td><input type="text" name="status_hadits'+id+'" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>';
    var body = document.getElementById("hadits_body");
    var tr = document.createElement('tr');
    tr.innerHTML = baris_hadits;
    // var elements = tr.childNodes;
    body.append(tr);
    document.getElementById("id_hadits").value = id;
    document.getElementById("tambah_hadits").setAttribute("onclick","tambahHadits("+id+")");
    $('.select').select2();
  }

  function tambahAkhlaq(id){
    id++;
    var baris_akhlaq = '<td><input type="text" name="akhlaq'+id+'" style="width: 100%;" value=""></td><td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganAkhlaq(this.value,status_akhlaq'+id+')" onkeyup="this.value=minmax(this.value);keteranganAkhlaq(this.value,status_akhlaq'+id+')" type="number" name="nilai_akhlaq'+id+'" min="0" max="100" value=""></td><td><input type="text" name="status_akhlaq'+id+'" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>';
    var body = document.getElementById("akhlaq_body");
    var tr = document.createElement('tr');
    tr.innerHTML = baris_akhlaq;
    // var elements = tr.childNodes;
    body.append(tr);
    document.getElementById("id_akhlaq").value = id;
    document.getElementById("tambah_akhlaq").setAttribute("onclick","tambahAkhlaq("+id+")");
    $('.select').select2();
  }

  function tambahTambahan(id){
    id++;
    var baris_tambahan = '<td><input type="text" name="tambahan'+id+'" style="width: 100%;" value=""></td><td><input style="text-align: center;" onchange="this.value=minmax(this.value);keteranganTambahan(this.value,status_tambahan'+id+')" onkeyup="this.value=minmax(this.value);keteranganTambahan(this.value,status_tambahan'+id+')" type="number" name="nilai_tambahan'+id+'" min="0" max="100" value=""></td><td><input type="text" name="status_tambahan'+id+'" value="" readonly="" style="background-color: transparent;border: none;width: 100%;text-align: center;"></td>';
    var body = document.getElementById("tambahan_body");
    var tr = document.createElement('tr');
    tr.innerHTML = baris_tambahan;
    // var elements = tr.childNodes;
    body.append(tr);
    document.getElementById("id_tambahan").value = id;
    document.getElementById("tambah_tambahan").setAttribute("onclick","tambahTambahan("+id+")");
    $('.select').select2();
  }

  function minmax(value){
    if(value.length > 1 && value.substr(0,1) == '0')
      value = parseInt(value.substr(1,value.length));
    if(value > 100){
      return 100;
    } else if (value < 0) {
      return '';
    }
    return value;
  }

  function reset(){
    swal({
      title: 'Konfirmasi',
      text: 'Apakah Anda yakin?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, saya yakin',
      cancelButtonText: 'Tidak, batalkan!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    },function () {
      var APP_URL = {!! json_encode(url('penilaian')) !!},
          uRl = APP_URL+'/reset';
      window.location.href = uRl;
    });
  }

  function hapus(){
    swal({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, cancel!',
  confirmButtonClass: 'btn btn-success',
  cancelButtonClass: 'btn btn-danger',
  buttonsStyling: false
}).then(function () {
  swal(
    'Deleted!',
    'Your file has been deleted.',
    'success'
  )
}, function (dismiss) {
  // dismiss can be 'cancel', 'overlay',
  // 'close', and 'timer'
  if (dismiss === 'cancel') {
    swal(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})
  }
</script>
@stop