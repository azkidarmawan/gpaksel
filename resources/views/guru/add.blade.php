@extends('spider::layouts.apps')
@section('content')



<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="{{ url('guru')}}"><i class="fa fa-circle-o"></i> Guru</a></li>
    <li class="active"><a href="{{ url('guru/add')}}"><i class="fa fa-plus"></i> Tambah Data</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title" style="margin-left: 1vw">Tambah Guru</h3>
        <div class="box-tools pull-right">
          <a href="{{ url('guru')}}" class="btn btn-xs btn-flat btn-primary"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12" style="margin-left: 1vw">
            <form class="form-horizontal" action="{{ url('guru/store') }}" method="POST">
            {!! csrf_field() !!}
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Nama Guru</label>
              </div>
              <div class="col-md-4">
                <input type="text" name="nama_guru" style="width: 60%" maxlength="100" required="">
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
              <div class="col-md-6">
                <div class="col-md-4" style="margin-left: -14px">
                  <label>Jenis Kelamin</label>
                </div>
                <div class="col-md-8">
                  <input value="Laki-Laki" type="radio" name="jenis_kelamin" style="width: 4%" required="">
                  <label style="font-size: 13px;margin-right: 1vw"> Laki laki </label>
                  <input value="Perempuan" type="radio" name="jenis_kelamin" style="width: 4%" required="">
                  <label style="font-size: 13px;margin-right: 1vw"> Perempuan </label>
                  <label style="color: red"><i class="fa fa-certificate"></i> required</label>
                </div>
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Alamat Tinggal</label>
              </div>
              <div class="col-md-4">
                <input type="text" name="alamat_tinggal" maxlength="300" style="width: 60%;font-size: 12px">
              </div>
              <div class="col-md-2">
                <label>Nomor Telepon</label>
              </div>
              <div class="col-md-4">
                <input type="text" data-inputmask="'mask': '9999 9999 99999'" name="no_telp" style="width: 60%">
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Desa</label>
              </div>
              <div class="col-md-4">
                <Select id="select1" class="select" name="kode_desa" style="width: 60%" required="">
                  <option value=""> -- Pilih Desa -- </option>
                  @foreach($datas as $key=>$data)
                    <option value="{{$data->kode_desa}}">{{$data->nama_desa}}</option>
                  @endforeach
                </Select>
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
              <div class="col-md-2">
                <label>Keterangan</label>
              </div>
              <div class="col-md-4">
                <input type="text" name="keterangan" style="width: 60%" maxlength="100">
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Kelompok</label>
              </div>
              <div class="col-md-4">
                <Select id="select2" class="select" name="kode_kelompok" style="width: 60%" required="">
                  <option value=""> -- Pilih Kelompok -- </option>
                </Select>
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
            </div>
            <br>
            <button type="submit" class = 'btn btn-primary' style="margin-bottom: 0.5vw">Simpan</button>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
</section><!-- /.content -->
@endsection

@section('css')

<style>
  .table-bordered , th, td, tr{
    border: 1px solid #e3e3e3 !important;

  }
</style>
@endsection

@section('script')
<script>
  $(function(){
    @if(Session::has('error'))
      swal({
        title:"Maaf",
        text:"{{ Session::get('error') }}",
        type:"error",
        // timer:2000,// optional
        showConfirmButton:true // set to true or false
      });
    @endif
    $( ".select" ).select2();
    $(":input").inputmask();
  });

  $(document).on('change', '#select1', function(e) {
    console.log(e);
    var kode = e.target.value, 
        APP_URL = {!! json_encode(url('kelompok')) !!},
                uRl = APP_URL+'/getkelompok?kode_desa='+kode; 
    $.get(uRl, function(data) {
      $('#select2').find('option').remove(); // untuk mereset option yg ada di select2
      var opt = document.createElement("option");
      opt.text = "-- Pilih Kelompok --";
      opt.value = "";
      var sel = document.getElementById("select2");
      sel.appendChild(opt);
      // loop utk menambahkan option pd select2
      for (var i = 0; i < data.length; i++) {
        var option = document.createElement("option");
        option.text = data[i].nama_kelompok;
        option.value = data[i].kode_kelompok;
        var select = document.getElementById("select2");
        select.appendChild(option);
      }
    });
  });


  function hapus(){
    swal({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, cancel!',
  confirmButtonClass: 'btn btn-success',
  cancelButtonClass: 'btn btn-danger',
  buttonsStyling: false
}).then(function () {
  swal(
    'Deleted!',
    'Your file has been deleted.',
    'success'
  )
}, function (dismiss) {
  // dismiss can be 'cancel', 'overlay',
  // 'close', and 'timer'
  if (dismiss === 'cancel') {
    swal(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})
  }
</script>
@stop