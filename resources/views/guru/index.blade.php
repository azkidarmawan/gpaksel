@extends('spider::layouts.apps')
@section('content')



<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>

    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="{{ url('guru')}}"><i class="fa fa-circle-o"></i> Guru</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="form-group"
    <div class="col-md-12">
    <input type="hidden" id="act" value="{{ auth()->user()->getProfile->roles }}">
      <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
            <div class="box-tools pull-right">
			  <a href="{{ url('guru/add') }}" id="btn-add" class="btn btn-xs btn-flat btn-primary"><i class="fa fa-edit"></i> Tambah Data</a>
            </div>
          </div>
            <div id="list" class="box-body">
              <table id="product-table" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <!-- <th width="10px">No</th> -->
                    <th>Nama Guru</th>
          					<th>Jenis Kelamin</th>
                    <th>Alamat Sambung</th>
                    <th>Alamat Tinggal</th>
                    <th>No. Telp</th>
                    <th>Keterangan</th>
          					@if (auth()->user()->getProfile->roles == 'admin')
          					<th id="action">Aksi</th>
          					@endif
                  </tr>
                </thead>
                <tbody>
                @foreach($datas as $data)
                <tr>
                    <td>{{ $data->nama_guru }}</td>
                    <td>{{ $data->jenis_kelamin }}</td>
                    <td style="white-space: nowrap;">Desa {{ $data->nama_desa }} - {{ $data->nama_kelompok }}</td>
                    <td>{{ $data->alamat_tinggal }}</td>
                    <td style="white-space: nowrap;">{{ $data->no_telp }}</td>
                    <td>{{ $data->keterangan }}</td>
          					@if (auth()->user()->getProfile->roles == 'admin')
          					<td style="white-space: nowrap;">
          					  <center>
          						<a href="{{ url('guru/edit', $data->kode_guru) }}" class="btn btn-xs btn-flat btn-info" style="min-width: 55px" >Ubah</a>
          						<a href="{{ url('guru/delete', $data->kode_guru) }}" type="button" class="btn btn-xs btn-flat btn-danger" style="min-width: 55px" onclick="return confirm('Apakah Anda yakin?')">Hapus</a>
          					  </center>
                    </td>
					          @endif
                </tr>
                 @endforeach
                </tbody>
              </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>

</section><!-- /.content -->
@endsection

@section('css')

<style>
  .table-bordered , th, td, tr{
    border: 1px solid #e3e3e3 !important;
  }
  th,td {
    text-align: center;
  }
</style>
@endsection

@section('script')
<script>
  	
	
  $(function(){
	var role = $('#act').val();
	if(role == 'user'){
		document.getElementById("btn-add").remove();
	}
	
    @if(Session::has('error'))
      swal({
        title:"Gagal",
        text:"{{ Session::get('error') }}",
        type:"error",
        // timer:2000,// optional
        showConfirmButton:true // set to true or false
      });
    @endif
    $('#product-table').DataTable();
  });

  function hapus(){
    swal({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, cancel!',
  confirmButtonClass: 'btn btn-success',
  cancelButtonClass: 'btn btn-danger',
  buttonsStyling: false
}).then(function () {
  swal(
    'Deleted!',
    'Your file has been deleted.',
    'success'
  )
}, function (dismiss) {
  // dismiss can be 'cancel', 'overlay',
  // 'close', and 'timer'
  if (dismiss === 'cancel') {
    swal(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})
  }
</script>
@stop