@extends('spider::layouts.apps')
@section('content')
{{-- You can create your content in here or you can create new file like this file --}}

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>

    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-area-chart"></i> Dashboard</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
      <!-- Widget: user widget style 1 -->
      <div class="box widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <!-- <div class="widget-user-header bg-black" style="background: url({{ asset('vendor/spider/alte/dist/img/photo1.png') }}) center center;">
          <h3 class="widget-user-username">GP <i>- Aksel</i></h3>
          <h6 class="widget-user-desc">Welcome to Administrator System {{config('spider.config.title_name')}}.</h6>
        </div>
        <div class="widget-user-image">
          <img class="img-circle" src="{{ asset('vendor/upload/images/thumbnails/'.auth()->user()->getProfile->images_profile) }}" alt="User Avatar">
        </div>
		<h1></h1> -->
        <div class="box-footer" style="padding-top: 10px">
          <div class="row">
            <div class="col-lg-4 col-md-6">
      				<div class="panel panel-primary">
      					<div class="panel-heading">
      						<div class="row">
      							<div class="col-xs-3">
      								<i class="fa fa-users fa-5x"></i>
      							</div>
      							<div class="col-xs-9 text-right">
      								<div class="huge" style="font-size:40px">{{$jml_murid[0]->jumlah}}</div>
      								<div>Jumlah Siswa/i</div>
      							</div>
      						</div>
      					</div>
      					<a href="{{ url('murid')}}">
      						<div class="panel-footer">
      							<span class="pull-left">View Details</span>
      							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
      							<div class="clearfix"></div>
      						</div>
      					</a>
      				</div>
      			</div>
            <div class="col-lg-4 col-md-6">
      				<div class="panel panel-green">
      					<div class="panel-heading">
      						<div class="row">
      							<div class="col-xs-3">
      								<i class="fa fa-file-text-o fa-5x"></i>
      							</div>
      							<div class="col-xs-9 text-right">
      								<div class="huge" style="font-size:40px">{{$absensi_kehadiran}}%</div>
      								<div>Absensi Kehadiran ({{date("F",strtotime("-1 month"))}})</div>
      							</div>
      						</div>
      					</div>
      					<a href="{{ url('absensi')}}">
      						<div class="panel-footer">
      							<span class="pull-left">View Details</span>
      							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
      							<div class="clearfix"></div>
      						</div>
      					</a>
      				</div>
  			     </div>
            <div class="col-lg-4 col-md-6">
      				<div class="panel panel-yellow">
      					<div class="panel-heading">
      						<div class="row">
      							<div class="col-xs-3">
      								<i class="fa fa-tasks fa-5x"></i>
      							</div>
      							<div class="col-xs-9 text-right">
      								<div class="huge" style="font-size:40px">{{number_format($kompetensi,2,',','')}}%</div>
      								<div>Kompetensi Murid</div>
      							</div>
      						</div>
      					</div>
      					<a href="{{ url('penilaian')}}">
      						<div class="panel-footer">
      							<span class="pull-left">View Details</span>
      							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
      							<div class="clearfix"></div>
      						</div>
      					</a>
      				</div>
      			</div>
          </div><!-- /.row -->
          <hr>

          							 <!--  DATA ABSENSI  -->
          <input type="hidden" id="jan_hadir" value="{{$absen1[0]->hadir}}">
          <input type="hidden" id="jan_izin" value="{{$absen1[0]->izin}}">
          <input type="hidden" id="jan_alfa" value="{{$absen1[0]->alfa}}">
          <input type="hidden" id="feb_hadir" value="{{$absen2[0]->hadir}}">
          <input type="hidden" id="feb_izin" value="{{$absen2[0]->izin}}">
          <input type="hidden" id="feb_alfa" value="{{$absen2[0]->alfa}}">
          <input type="hidden" id="mar_hadir" value="{{$absen3[0]->hadir}}">
          <input type="hidden" id="mar_izin" value="{{$absen3[0]->izin}}">
          <input type="hidden" id="mar_alfa" value="{{$absen3[0]->alfa}}">
          <input type="hidden" id="apr_hadir" value="{{$absen4[0]->hadir}}">
          <input type="hidden" id="apr_izin" value="{{$absen4[0]->izin}}">
          <input type="hidden" id="apr_alfa" value="{{$absen4[0]->alfa}}">
          <input type="hidden" id="mei_hadir" value="{{$absen5[0]->hadir}}">
          <input type="hidden" id="mei_izin" value="{{$absen5[0]->izin}}">
          <input type="hidden" id="mei_alfa" value="{{$absen5[0]->alfa}}">
          <input type="hidden" id="jun_hadir" value="{{$absen6[0]->hadir}}">
          <input type="hidden" id="jun_izin" value="{{$absen6[0]->izin}}">
          <input type="hidden" id="jun_alfa" value="{{$absen6[0]->alfa}}">
          <input type="hidden" id="jul_hadir" value="{{$absen7[0]->hadir}}">
          <input type="hidden" id="jul_izin" value="{{$absen7[0]->izin}}">
          <input type="hidden" id="jul_alfa" value="{{$absen7[0]->alfa}}">
          <input type="hidden" id="agu_hadir" value="{{$absen8[0]->hadir}}">
          <input type="hidden" id="agu_izin" value="{{$absen8[0]->izin}}">
          <input type="hidden" id="agu_alfa" value="{{$absen8[0]->alfa}}">
          <input type="hidden" id="sep_hadir" value="{{$absen9[0]->hadir}}">
          <input type="hidden" id="sep_izin" value="{{$absen9[0]->izin}}">
          <input type="hidden" id="sep_alfa" value="{{$absen9[0]->alfa}}">
          <input type="hidden" id="okt_hadir" value="{{$absen10[0]->hadir}}">
          <input type="hidden" id="okt_izin" value="{{$absen10[0]->izin}}">
          <input type="hidden" id="okt_alfa" value="{{$absen10[0]->alfa}}">
          <input type="hidden" id="nov_hadir" value="{{$absen11[0]->hadir}}">
          <input type="hidden" id="nov_izin" value="{{$absen11[0]->izin}}">
          <input type="hidden" id="nov_alfa" value="{{$absen11[0]->alfa}}">
          <input type="hidden" id="des_hadir" value="{{$absen12[0]->hadir}}">
    		  <input type="hidden" id="des_izin" value="{{$absen12[0]->izin}}">
    		  <input type="hidden" id="des_alfa" value="{{$absen12[0]->alfa}}">

    		  <input type="hidden" id="total" value="{{$total}}">
    		  <input type="hidden" id="total_hadir" value="{{$total_hadir}}">
    		  <input type="hidden" id="total_izin" value="{{$total_izin}}">
    		  <input type="hidden" id="total_alfa" value="{{$total_alfa}}">

          @foreach($guru as $key=>$value)
            <input type="hidden" id="guru{{$key}}" value="{{$guru[$key]}}">
            <input type="hidden" id="jml_sesi{{$key}}" value="{{$jml_sesi[$key]}}">
          @endforeach
          <input type="hidden" id="total_guru" value="{{count($guru)}}">

          @foreach($guru_tahun as $key=>$value)
            <input type="hidden" id="guru_tahun{{$key}}" value="{{$guru_tahun[$key]}}">
            <input type="hidden" id="jml_sesi_tahun{{$key}}" value="{{$jml_sesi_tahun[$key]}}">
          @endforeach
          <input type="hidden" id="total_guru_tahun" value="{{count($guru_tahun)}}">
              							<!--  /.DATA ABSENSI  -->

          <div class="row">
          	<div class="col-md-2">
          		<center><strong><h3>Statistik Kehadiran</h3></strong></center>
          		<br>
          		<div class="row">
          			<div style="position: absolute;width: 16px;height: 8px;margin-left: 20px;margin-top: 6px;background-color: limegreen"></div>
          			<div class="col-md-1"></div>
          			<div class="col-md-9"><label>Hadir : {{number_format($total_hadir*100/$total,0,',','')}}%</label></div>
          		</div>
          		<div class="row">
          			<div style="position: absolute;width: 16px;height: 8px;margin-left: 20px;margin-top: 6px;background-color: gold"></div>
          			<div class="col-md-1"></div>
          			<div class="col-md-9"><label>Izin : {{number_format($total_izin*100/$total,0,',','')}}%</label></div>
          		</div>
          		<div class="row">
          			<div style="position: absolute;width: 16px;height: 8px;margin-left: 20px;margin-top: 6px;background-color: crimson"></div>
          			<div class="col-md-1"></div>
          			<div class="col-md-9"><label>Tanpa Ket. : {{number_format($total_alfa*100/$total,0,',','')}}%</label></div>
          		</div>
          	</div>
          	<div class="col-md-2" style="text-align: left">
          		<div class="chart">
          			<canvas id="pieChart" style="height:200px;"></canvas>
          		</div>
	        </div>
          	<div class="col-md-8">
          		<div class="chart">
                <!-- Page unresponsive -->
	              <canvas id="barChart" style="height:200px;"></canvas>
	            </div>
          	</div>
          </div><!-- /.row -->
          <hr>
          <div class="row">
            <div class="col-md-2">
              <center><strong><h3>Statistik Jumlah Sesi Pengajaran</h3></strong></center>
              <br>
              <div class="row">
                <div style="position: absolute;width: 16px;height: 8px;margin-left: 20px;margin-top: 6px;background-color: steelblue"></div>
                <div class="col-md-1"></div>
                <div class="col-md-9"><label> Tahun {{date("Y")}} : {{$total_sesi_tahun}}</label></div>
              </div>
              <div class="row">
                <div style="position: absolute;width: 16px;height: 8px;margin-left: 20px;margin-top: 6px;background-color: lightgreen"></div>
                <div class="col-md-1"></div>
                <div class="col-md-9"><label> {{date("F",strtotime("-1 month"))}} : {{$total_sesi}}</label></div>
              </div>
            </div>
            <div class="col-md-10">
              <div class="chart">
                <canvas id="barChart2" style="height:200px;"></canvas>
              </div>
            </div>
          </div><!-- /.row -->
          <hr>
          <div class="row">
			<div class="col-md-12">
				<div class="panel panel-red">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-1">
								<i class="fa fa-newspaper-o fa-3x"></i>
							</div>
							<div class="col-xs-9 text-left">
								<div class="huge" style="font-size:28px;margin-left: 2vw">Pengumuman!</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<br>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat.
						<br></br>
					</div>
					<a href="{{ url('guru')}}">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
          </div><!-- /.row -->
          <br>
        </div>
      </div><!-- /.widget-user -->
    </div><!-- /.col -->
</div>

<style>
.panel {
	margin-top: 0cm;
	margin-bottom: 0cm;
	padding-top: 0cm;
	padding-bottom: 0cm;
}

.box-footer {
    margin-top: 0cm;
	margin-bottom: 0cm;
	padding-top: 0cm;
	padding-bottom: 0cm;
}

.panel-green {
    border-color: #5cb85c;
}

.panel-green .panel-heading {
    border-color: #5cb85c;
    color: #fff;
    background-color: #5cb85c;
}

.panel-green a {
    color: #5cb85c;
}

.panel-green a:hover {
    color: #3d8b3d;
}

.panel-red {
    border-color: #d9534f;
}

.panel-red .panel-heading {
    border-color: #d9534f;
    color: #fff;
    background-color: #d9534f;
}

.panel-red a {
    color: #d9534f;
}

.panel-red a:hover {
    color: #b52b27;
}

.panel-yellow {
    border-color: #f0ad4e;
}

.panel-yellow .panel-heading {
    border-color: #f0ad4e;
    color: #fff;
    background-color: #f0ad4e;
}

.panel-yellow a {
    color: #f0ad4e;
}

.panel-yellow a:hover {
    color: #df8a13;
}
</style>

</section><!-- /.content -->
@endsection

@section('script')
<script>
  @if(Session::has('success'))
    swal({
      type: "success",
      text: "Selamat Datang di Sistem Distribusi UT",
      title: "Welcome !!!"
    });
  @endif

  $(function (){
  	//-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
      datasets: [
        {
          label               : 'Hadir',
          fillColor           : 'limegreen',
          strokeColor         : 'limegreen',
          pointColor          : 'limegreen',
          pointStrokeColor    : 'limegreen',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'limegreen',
          data                : [$('#jan_hadir').val(),$('#feb_hadir').val(),$('#mar_hadir').val(),$('#apr_hadir').val(),$('#mei_hadir').val(),$('#jun_hadir').val(),$('#jul_hadir').val(),$('#agu_hadir').val(),$('#sep_hadir').val(),$('#okt_hadir').val(),$('#nov_hadir').val(),$('#des_hadir').val()]
        },
        {
          label               : 'Izin',
          fillColor           : 'gold',
          strokeColor         : 'gold',
          pointColor          : 'gold',
          pointStrokeColor    : 'gold',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'gold',
          data                : [$('#jan_izin').val(),$('#feb_izin').val(),$('#mar_izin').val(),$('#apr_izin').val(),$('#mei_izin').val(),$('#jun_izin').val(),$('#jul_izin').val(),$('#agu_izin').val(),$('#sep_izin').val(),$('#okt_izin').val(),$('#nov_izin').val(),$('#des_izin').val()]
        },
        {
          label               : 'Tanpa Keterangan',
          fillColor           : 'crimson',
          strokeColor         : 'crimson',
          pointColor          : 'crimson',
          pointStrokeColor    : 'crimson',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'crimson',
          data                : [$('#jan_alfa').val(),$('#feb_alfa').val(),$('#mar_alfa').val(),$('#apr_alfa').val(),$('#mei_alfa').val(),$('#jun_alfa').val(),$('#jul_alfa').val(),$('#agu_alfa').val(),$('#sep_alfa').val(),$('#okt_alfa').val(),$('#nov_alfa').val(),$('#des_alfa').val()]
        }
      ]
    }

    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 2,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 8,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)

    //-------------
    //- BAR CHART 2 -
    //-------------
    var jml_guru = $('#total_guru').val();
    var jml_guru_tahun = $('#total_guru_tahun').val();
    var guru_tahun = [];
    var guru = [];
    var sesi = [];
    var total_sesi = [];
    for(var i = 0;i < jml_guru_tahun;i++){
      guru_tahun.push($('#guru_tahun'+i).val());
      total_sesi.push($('#jml_sesi_tahun'+i).val());
    }
    for(var i = 0;i < jml_guru;i++){
      guru.push($('#guru'+i).val());
      sesi.push($('#jml_sesi'+i).val());
    }
    while(jml_guru != jml_guru_tahun){
      for(var i = 0;i < jml_guru_tahun;i++){
        for(var j = 0;j < jml_guru;j++){
          if(guru_tahun[i] == guru[j])
            break;
          if(j == jml_guru-1){
            guru[jml_guru] = guru_tahun[i];
            sesi[jml_guru] = 0;
            jml_guru++;
          }
        }
      }
    }
    for(var i = 0;i < jml_guru_tahun;i++){
      if(guru[i] != guru_tahun[i]){
        var temp_guru = guru[i];
        var temp_sesi = sesi[i];
        var index = guru.indexOf(guru_tahun[i]);
        if (~index) {
            guru[i] = guru[index];
            sesi[i] = sesi[index];
            guru[index] = temp_guru;
            sesi[index] = temp_sesi;
        }
      }
    }

    var barChartCanvas                   = $('#barChart2').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : guru_tahun,
      datasets: [
        {
          label               : 'Total Sesi',
          fillColor           : 'steelblue',
          strokeColor         : 'steelblue',
          pointColor          : 'steelblue',
          pointStrokeColor    : 'steelblue',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'steelblue',
          data                : total_sesi
        },
        {
          label               : 'Hadir',
          fillColor           : 'lightgreen',
          strokeColor         : 'lightgreen',
          pointColor          : 'lightgreen',
          pointStrokeColor    : 'lightgreen',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'lightgreen',
          data                : sesi
        }
      ]
    }

    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 2,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 16,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 2,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieChart       = new Chart(pieChartCanvas)
    // document.getElementById("status")
    var PieData        = [
      {
        value    : $('#total_alfa').val(),
        color    : 'crimson',
        highlight: 'crimson',
        label    : 'Tanpa Ket. '
      },
      {
        value    : $('#total_izin').val(),
        color    : 'gold',
        highlight: 'gold',
        label    : 'Sakit/Izin '
      },
      {
        value    : $('#total_hadir').val(),
        color    : 'limegreen',
        highlight: 'limegreen',
        label    : 'Hadir '
      }
    ]
    var pieOptions     = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke    : true,
      //String - The colour of each segment stroke
      segmentStrokeColor   : '#fff',
      //Number - The width of each segment stroke
      segmentStrokeWidth   : 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 0, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps       : 100,
      //String - Animation easing effect
      animationEasing      : 'easeOutBounce',
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate        : true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale         : false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive           : true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio  : true,
      //String - A legend template
      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions)
  })
</script>
@stop