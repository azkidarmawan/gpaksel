@extends('spider::layouts.apps')
@section('content')



<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>

    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="{{ url('absensi')}}"><i class="fa fa-circle-o"></i> Absensi</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="form-group"
    <div class="col-md-12">
    <input type="hidden" id="act" value="{{ auth()->user()->getProfile->roles }}">
      <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Data</h3>
            <div class="box-tools pull-right">
			  <a href="{{ url('absensi/add') }}" id="btn-add" class="btn btn-xs btn-flat btn-primary"><i class="fa fa-edit"></i> Tambah Data</a>
            </div>
          </div>
            <div id="list" class="box-body">
              <table id="product-table" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <!-- <th width="10px">No</th> -->
                    <th>Murid</th>
          					<th>Tahun</th>
                    <th>Jan</th>
                    <th>Feb</th>
                    <th>Mar</th>
                    <th>Apr</th>
                    <th>Mei</th>
                    <th>Jun</th>
                    <th>Jul</th>
                    <th>Agu</th>
                    <th>Sep</th>
                    <th>Okt</th>
                    <th>Nov</th>
                    <th>Des</th>
          					@if (auth()->user()->getProfile->roles == 'admin')
          					<th id="action">Aksi</th>
          					@endif
                  </tr>
                </thead>
                <tbody>
                @foreach($datas as $data)
                <?php
                  $pertemuanjanuari = 5;
                  $januari = 0;
                  for($i = 0;$i < 5;$i++){
                    if(substr($data->januari, $i,1) == 0)
                      $pertemuanjanuari--;
                    if(substr($data->januari, $i,1) == 1)
                      $januari++;
                  }
                  $pertemuanfebruari = 5;
                  $februari = 0;
                  for($i = 0;$i < 5;$i++){
                    if(substr($data->februari, $i,1) == 0)
                      $pertemuanfebruari--;
                    if(substr($data->februari, $i,1) == 1)
                      $februari++;
                  }
                  $pertemuanmaret = 5;
                  $maret = 0;
                  for($i = 0;$i < 5;$i++){
                    if(substr($data->maret, $i,1) == 0)
                      $pertemuanmaret--;
                    if(substr($data->maret, $i,1) == 1)
                      $maret++;
                  }
                  $pertemuanapril = 5;
                  $april = 0;
                  for($i = 0;$i < 5;$i++){
                    if(substr($data->april, $i,1) == 0)
                      $pertemuanapril--;
                    if(substr($data->april, $i,1) == 1)
                      $april++;
                  }
                  $pertemuanmei = 5;
                  $mei = 0;
                  for($i = 0;$i < 5;$i++){
                    if(substr($data->mei, $i,1) == 0)
                      $pertemuanmei--;
                    if(substr($data->mei, $i,1) == 1)
                      $mei++;
                  }
                  $pertemuanjuni = 5;
                  $juni = 0;
                  for($i = 0;$i < 5;$i++){
                    if(substr($data->juni, $i,1) == 0)
                      $pertemuanjuni--;
                    if(substr($data->juni, $i,1) == 1)
                      $juni++;
                  }
                  $pertemuanjuli = 5;
                  $juli = 0;
                  for($i = 0;$i < 5;$i++){
                    if(substr($data->juli, $i,1) == 0)
                      $pertemuanjuli--;
                    if(substr($data->juli, $i,1) == 1)
                      $juli++;
                  }
                  $pertemuanagustus = 5;
                  $agustus = 0;
                  for($i = 0;$i < 5;$i++){
                    if(substr($data->agustus, $i,1) == 0)
                      $pertemuanagustus--;
                    if(substr($data->agustus, $i,1) == 1)
                      $agustus++;
                  }
                  $pertemuanseptember = 5;
                  $september = 0;
                  for($i = 0;$i < 5;$i++){
                    if(substr($data->september, $i,1) == 0)
                      $pertemuanseptember--;
                    if(substr($data->september, $i,1) == 1)
                      $september++;
                  }
                  $pertemuanoktober = 5;
                  $oktober = 0;
                  for($i = 0;$i < 5;$i++){
                    if(substr($data->oktober, $i,1) == 0)
                      $pertemuanoktober--;
                    if(substr($data->oktober, $i,1) == 1)
                      $oktober++;
                  }
                  $pertemuannovember = 5;
                  $november = 0;
                  for($i = 0;$i < 5;$i++){
                    if(substr($data->november, $i,1) == 0)
                      $pertemuannovember--;
                    if(substr($data->november, $i,1) == 1)
                      $november++;
                  }
                  $pertemuandesember = 5;
                  $desember = 0;
                  for($i = 0;$i < 5;$i++){
                    if(substr($data->desember, $i,1) == 0)
                      $pertemuandesember--;
                    if(substr($data->desember, $i,1) == 1)
                      $desember++;
                  }
                ?>
                <tr style="text-align: center;">
                    <td><a href="#" onclick="showimage({{ $data->kode_murid }})">{{ $data->nama_panggilan }}</a></td>
                    <td>{{ $data->tahun }}</td>
                    <td>{{ $januari }} / {{ $pertemuanjanuari }}</td>
                    <td>{{ $februari }} / {{ $pertemuanfebruari }}</td>
                    <td>{{ $maret }} / {{ $pertemuanmaret }}</td>
                    <td>{{ $april }} / {{ $pertemuanapril }}</td>
                    <td>{{ $mei }} / {{ $pertemuanmei }}</td>
                    <td>{{ $juni }} / {{ $pertemuanjuni }}</td>
                    <td>{{ $juli }} / {{ $pertemuanjuli }}</td>
                    <td>{{ $agustus }} / {{ $pertemuanagustus }}</td>
                    <td>{{ $september }} / {{ $pertemuanseptember }}</td>
                    <td>{{ $oktober }} / {{ $pertemuanoktober }}</td>
                    <td>{{ $november }} / {{ $pertemuannovember }}</td>
                    <td>{{ $desember }} / {{ $pertemuandesember }}</td>
          					@if (auth()->user()->getProfile->roles == 'admin')
          					<td style="white-space: nowrap;">
          					  <center>
          						<a href="{{ url('absensi/edit', $data->kode_absen) }}/{{$data->kode_murid}}/{{$data->tahun}}" class="btn btn-xs btn-flat btn-info" style="min-width: 55px" >Lihat</a>
          					  </center>
                    </td>
					          @endif
                </tr>
                 @endforeach
                </tbody>
              </table>
            </div><!-- /.box-body -->
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <center>
                  <hr>
                    <strong><h2>Biodata Murid</h2></strong>
                    <hr>
                    <table id="detail-table" style="max-width: 95%">
                      <thead>
                        <tr>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td style="text-align: center;" rowspan="9"><img id="source" style="max-width: 195px;max-height: 260px;margin-bottom: 1vw;"></td>
                          <td><label style="margin-left: 10px;margin-right: 10px"> No. Induk Murid </label></td>
                          <td><label style="margin-left: 10px;margin-right: 10px" id="nim" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> Nama Lengkap </label></td>
                          <td><label style="margin-left: 10px;margin-right: 10px" id="desc" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> Nama Panggilan </label></td>
                          <td><label style="margin-left: 10px;margin-right: 10px" id="panggilan" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> Jenis Kelamin </label></td>
                          <td><label style="margin-left: 10px;margin-right: 10px" id="kelamin" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> Angkatan </label></td>
                          <td><label style="margin-left: 10px;margin-right: 10px" id="angkatan" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> Kelas </label></td>
                          <td><label style="margin-left: 10px;margin-right: 10px" id="kls" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> Tempat Lahir </label></td>
                          <td><label style="margin-left: 10px;margin-right: 10px" id="tmp_lahir" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> Tanggal Lahir </label></td>
                          <td><label style="margin-left: 10px;margin-right: 10px" id="tgl_lahir" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td colspan="2" style="text-align: center !important;color: white">-</td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> Alamat Sambung </label></td>
                          <td colspan="2"><label style="margin-left: 10px;margin-right: 10px" id="sambung" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> Alamat Tinggal </label></td>
                          <td colspan="2" style="vertical-align: middle !important;"><label style="margin-left: 10px;margin-right: 10px" id="tinggal" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> Nomor HP Murid </label></td>
                          <td colspan="2"><label style="margin-left: 10px;margin-right: 10px" id="no_hp" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> Nama Ortu/Wali </label></td>
                          <td colspan="2"><label style="margin-left: 10px;margin-right: 10px" id="wali" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> No. Telp Ortu/Wali </label></td>
                          <td colspan="2"><label style="margin-left: 10px;margin-right: 10px" id="telp_wali" style="width: 100%"></label></td>
                        </tr>
                        <tr>
                          <td><label style="margin-left: 10px;margin-right: 10px"> Keterangan </label></td>
                          <td colspan="2"><label style="margin-left: 10px;margin-right: 10px" id="ket" style="width: 100%"></label></td>
                        </tr>
                      </tbody>
                    </table>
                    <br>
                  </center>
                </div>
              </div>
            </div>
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>

</section><!-- /.content -->
@endsection

@section('css')

<style>
  .table-bordered , th, td, tr{
    border: 1px solid #e3e3e3 !important;
  }
  th {
    text-align: center;
  }
</style>
@endsection

@section('script')
<script>
  	
	
  $(function(){
	var role = $('#act').val();
	if(role == 'user'){
		document.getElementById("btn-add").remove();
	}
	
    @if(Session::has('error'))
      swal({
        title:"Gagal",
        text:"{{ Session::get('error') }}",
        type:"error",
        // timer:2000,// optional
        showConfirmButton:true // set to true or false
      });
    @endif
    $('#product-table').DataTable();
  });

  function showimage(value){
    var img = document.getElementById('source');
    var desc = document.getElementById('desc');
    var panggilan = document.getElementById('panggilan');
    var kelamin = document.getElementById('kelamin');
    var angkatan = document.getElementById('angkatan');
    var kls = document.getElementById('kls');
    var tmp_lahir = document.getElementById('tmp_lahir');
    var tgl_lahir = document.getElementById('tgl_lahir');
    var sambung = document.getElementById('sambung');
    var tinggal = document.getElementById('tinggal');
    var no_hp = document.getElementById('no_hp');
    var wali = document.getElementById('wali');
    var telp_wali = document.getElementById('telp_wali');
    var ket = document.getElementById('ket');

    var APP_URL = {!! json_encode(url('murid')) !!},
        uRl = APP_URL+'/getimg?kode_murid='+value;

    $.get(uRl, function(data) {
      if(data[0].foto == null){
        img.setAttribute("src","uploads/foto/No_Image_Available.jpg");
      } else {
        img.setAttribute("src","uploads/foto/"+data[0].foto+".jpg");
      }
      if(data[0].kode_murid < 10)
        var kode_nim = '00'+data[0].kode_murid;
      else if(data[0].kode_murid < 100)
        var kode_nim = '0'+data[0].kode_murid;
      else
        var kode_nim = data[0].kode_murid;
      nim.innerHTML = data[0].tahun_masuk.substr(2,4)+'0'+(data[0].tahun_masuk-2016)+kode_nim;
      desc.innerHTML = data[0].nama_lengkap;
      panggilan.innerHTML = data[0].nama_panggilan;
      kelamin.innerHTML = data[0].jenis_kelamin;
      angkatan.innerHTML = data[0].tahun_masuk;
      kls.innerHTML = data[0].kelas;
      tmp_lahir.innerHTML = data[0].tempat_lahir;
      var tgl = new Date(data[0].tanggal_lahir);
      tgl_lahir.innerHTML = tgl.getDate() + ' ' + bulan(tgl.getMonth()) + ' ' + tgl.getFullYear();
      sambung.innerHTML = data[0].nama_desa + ' - ' + data[0].nama_kelompok;
      tinggal.innerHTML = data[0].alamat_tinggal;
      no_hp.innerHTML = data[0].no_hp_murid;
      wali.innerHTML = data[0].nama_wali;
      telp_wali.innerHTML = data[0].no_telp_wali;
      ket.innerHTML = data[0].keterangan;
    });

    $('#myModal').modal('show');

    img.removeAttribute("src");
    desc.innerHTML = "";
  }

  function bulan(value) {
    if (value == 0) {
      return 'Januari';
    } else if(value == 1){
      return 'Februari';
    } else if(value == 2){
      return 'Maret';
    } else if(value == 3){
      return 'April';
    } else if(value == 4){
      return 'Mei';
    } else if(value == 5){
      return 'Juni';
    } else if(value == 6){
      return 'Juli';
    } else if(value == 7){
      return 'Agustus';
    } else if(value == 8){
      return 'September';
    } else if(value == 9){
      return 'Oktober';
    } else if(value == 10){
      return 'November';
    } else if(value == 11){
      return 'Desember';
    }
  }

  function hapus(){
    swal({
  title: 'Hapus data',
  text: "Apakah Anda yakin?",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Ya, hapuskan!',
  cancelButtonText: 'Tidak, batalkan!',
  confirmButtonClass: 'btn btn-success',
  cancelButtonClass: 'btn btn-danger',
  buttonsStyling: false
}).then(function () {
  swal(
    'Berhasil!',
    'Data berhasil dihapus.',
    'success'
  )
}, function (dismiss) {
  // dismiss can be 'cancel', 'overlay',
  // 'close', and 'timer'
  if (dismiss === 'cancel') {
    swal(
      'Dibatalkan',
      'Data tidak terhapus :)',
      'error'
    )
  }
})
  }
</script>
@stop