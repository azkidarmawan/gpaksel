@extends('spider::layouts.apps')
@section('content')



<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="{{ url('absensi')}}"><i class="fa fa-circle-o"></i> Absensi</a></li>
    <li class="active"><a href="{{ url('absensi/add')}}"><i class="fa fa-plus"></i> Tambah Data</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title" style="margin-left: 1vw">Tambah Absensi</h3>
        <div class="box-tools pull-right">
          <a href="{{ url('absensi')}}" class="btn btn-xs btn-flat btn-primary"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12" style="margin-left: 1vw">
            <form class="form-horizontal" action="{{ url('absensi/store') }}" method="POST">
            {!! csrf_field() !!}
            <div class="row" style="margin-bottom: 1vw">
              <div class="col-md-1">
                <label>Tahun</label>
              </div>
              <div class="col-md-3">
                <select class="select" name="tahun" required="" style="width: 50%">
                  <option value=""> Pilih Tahun </option>
                  <option>{{(date('Y')+1)}}</option>
                  <option selected="">{{(date('Y'))}}</option>
                  <option>{{(date('Y')-1)}}</option>
                </select>
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
              <div class="col-md-1">
                <label>Murid</label>
              </div>
              <div class="col-md-7">
                <select class="select" name="kode_murid" style="width: 70%" required="">
                  <option value=""> Pilih Murid </option>
                  @foreach($murids as $key=>$data)
                    <option value="{{$data->kode_murid}}">{{$data->nama_lengkap}}</option>
                  @endforeach
                </select>
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
            </div>
            <div class="row">
              <table id="absen-table" class="table table-bordered table-striped table-hover" style="width: 97%">
                <thead>
                  <tr>
                    <th>Bulan</th>
                    <th colspan="4">Minggu 1</th>
                    <th colspan="4">Minggu 2</th>
                    <th colspan="4">Minggu 3</th>
                    <th colspan="4">Minggu 4</th>
                    <th colspan="4">Minggu 5</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Januari</td>
                    <td>L <input type="radio" name="jan_minggu1" value="0" checked=""></td>
                    <td>H <input type="radio" name="jan_minggu1" value="1"></td>
                    <td>I <input type="radio" name="jan_minggu1" value="2"></td>
                    <td>A <input type="radio" name="jan_minggu1" value="3"></td>
                    <td>L <input type="radio" name="jan_minggu2" value="0" checked=""></td>
                    <td>H <input type="radio" name="jan_minggu2" value="1"></td>
                    <td>I <input type="radio" name="jan_minggu2" value="2"></td>
                    <td>A <input type="radio" name="jan_minggu2" value="3"></td>
                    <td>L <input type="radio" name="jan_minggu3" value="0" checked=""></td>
                    <td>H <input type="radio" name="jan_minggu3" value="1"></td>
                    <td>I <input type="radio" name="jan_minggu3" value="2"></td>
                    <td>A <input type="radio" name="jan_minggu3" value="3"></td>
                    <td>L <input type="radio" name="jan_minggu4" value="0" checked=""></td>
                    <td>H <input type="radio" name="jan_minggu4" value="1"></td>
                    <td>I <input type="radio" name="jan_minggu4" value="2"></td>
                    <td>A <input type="radio" name="jan_minggu4" value="3"></td>
                    <td>L <input type="radio" name="jan_minggu5" value="0" checked=""></td>
                    <td>H <input type="radio" name="jan_minggu5" value="1"></td>
                    <td>I <input type="radio" name="jan_minggu5" value="2"></td>
                    <td>A <input type="radio" name="jan_minggu5" value="3"></td>
                  </tr>
                  <tr>
                    <td>Februari</td>
                    <td>L <input type="radio" name="feb_minggu1" value="0" checked=""></td>
                    <td>H <input type="radio" name="feb_minggu1" value="1"></td>
                    <td>I <input type="radio" name="feb_minggu1" value="2"></td>
                    <td>A <input type="radio" name="feb_minggu1" value="3"></td>
                    <td>L <input type="radio" name="feb_minggu2" value="0" checked=""></td>
                    <td>H <input type="radio" name="feb_minggu2" value="1"></td>
                    <td>I <input type="radio" name="feb_minggu2" value="2"></td>
                    <td>A <input type="radio" name="feb_minggu2" value="3"></td>
                    <td>L <input type="radio" name="feb_minggu3" value="0" checked=""></td>
                    <td>H <input type="radio" name="feb_minggu3" value="1"></td>
                    <td>I <input type="radio" name="feb_minggu3" value="2"></td>
                    <td>A <input type="radio" name="feb_minggu3" value="3"></td>
                    <td>L <input type="radio" name="feb_minggu4" value="0" checked=""></td>
                    <td>H <input type="radio" name="feb_minggu4" value="1"></td>
                    <td>I <input type="radio" name="feb_minggu4" value="2"></td>
                    <td>A <input type="radio" name="feb_minggu4" value="3"></td>
                    <td>L <input type="radio" name="feb_minggu5" value="0" checked=""></td>
                    <td>H <input type="radio" name="feb_minggu5" value="1"></td>
                    <td>I <input type="radio" name="feb_minggu5" value="2"></td>
                    <td>A <input type="radio" name="feb_minggu5" value="3"></td>
                  </tr>
                  <tr>
                    <td>Maret</td>
                    <td>L <input type="radio" name="mar_minggu1" value="0" checked=""></td>
                    <td>H <input type="radio" name="mar_minggu1" value="1"></td>
                    <td>I <input type="radio" name="mar_minggu1" value="2"></td>
                    <td>A <input type="radio" name="mar_minggu1" value="3"></td>
                    <td>L <input type="radio" name="mar_minggu2" value="0" checked=""></td>
                    <td>H <input type="radio" name="mar_minggu2" value="1"></td>
                    <td>I <input type="radio" name="mar_minggu2" value="2"></td>
                    <td>A <input type="radio" name="mar_minggu2" value="3"></td>
                    <td>L <input type="radio" name="mar_minggu3" value="0" checked=""></td>
                    <td>H <input type="radio" name="mar_minggu3" value="1"></td>
                    <td>I <input type="radio" name="mar_minggu3" value="2"></td>
                    <td>A <input type="radio" name="mar_minggu3" value="3"></td>
                    <td>L <input type="radio" name="mar_minggu4" value="0" checked=""></td>
                    <td>H <input type="radio" name="mar_minggu4" value="1"></td>
                    <td>I <input type="radio" name="mar_minggu4" value="2"></td>
                    <td>A <input type="radio" name="mar_minggu4" value="3"></td>
                    <td>L <input type="radio" name="mar_minggu5" value="0" checked=""></td>
                    <td>H <input type="radio" name="mar_minggu5" value="1"></td>
                    <td>I <input type="radio" name="mar_minggu5" value="2"></td>
                    <td>A <input type="radio" name="mar_minggu5" value="3"></td>
                  </tr>
                  <tr>
                    <td>April</td>
                    <td>L <input type="radio" name="apr_minggu1" value="0" checked=""></td>
                    <td>H <input type="radio" name="apr_minggu1" value="1"></td>
                    <td>I <input type="radio" name="apr_minggu1" value="2"></td>
                    <td>A <input type="radio" name="apr_minggu1" value="3"></td>
                    <td>L <input type="radio" name="apr_minggu2" value="0" checked=""></td>
                    <td>H <input type="radio" name="apr_minggu2" value="1"></td>
                    <td>I <input type="radio" name="apr_minggu2" value="2"></td>
                    <td>A <input type="radio" name="apr_minggu2" value="3"></td>
                    <td>L <input type="radio" name="apr_minggu3" value="0" checked=""></td>
                    <td>H <input type="radio" name="apr_minggu3" value="1"></td>
                    <td>I <input type="radio" name="apr_minggu3" value="2"></td>
                    <td>A <input type="radio" name="apr_minggu3" value="3"></td>
                    <td>L <input type="radio" name="apr_minggu4" value="0" checked=""></td>
                    <td>H <input type="radio" name="apr_minggu4" value="1"></td>
                    <td>I <input type="radio" name="apr_minggu4" value="2"></td>
                    <td>A <input type="radio" name="apr_minggu4" value="3"></td>
                    <td>L <input type="radio" name="apr_minggu5" value="0" checked=""></td>
                    <td>H <input type="radio" name="apr_minggu5" value="1"></td>
                    <td>I <input type="radio" name="apr_minggu5" value="2"></td>
                    <td>A <input type="radio" name="apr_minggu5" value="3"></td>
                  </tr>
                  <tr>
                    <td>Mei</td>
                    <td>L <input type="radio" name="mei_minggu1" value="0" checked=""></td>
                    <td>H <input type="radio" name="mei_minggu1" value="1"></td>
                    <td>I <input type="radio" name="mei_minggu1" value="2"></td>
                    <td>A <input type="radio" name="mei_minggu1" value="3"></td>
                    <td>L <input type="radio" name="mei_minggu2" value="0" checked=""></td>
                    <td>H <input type="radio" name="mei_minggu2" value="1"></td>
                    <td>I <input type="radio" name="mei_minggu2" value="2"></td>
                    <td>A <input type="radio" name="mei_minggu2" value="3"></td>
                    <td>L <input type="radio" name="mei_minggu3" value="0" checked=""></td>
                    <td>H <input type="radio" name="mei_minggu3" value="1"></td>
                    <td>I <input type="radio" name="mei_minggu3" value="2"></td>
                    <td>A <input type="radio" name="mei_minggu3" value="3"></td>
                    <td>L <input type="radio" name="mei_minggu4" value="0" checked=""></td>
                    <td>H <input type="radio" name="mei_minggu4" value="1"></td>
                    <td>I <input type="radio" name="mei_minggu4" value="2"></td>
                    <td>A <input type="radio" name="mei_minggu4" value="3"></td>
                    <td>L <input type="radio" name="mei_minggu5" value="0" checked=""></td>
                    <td>H <input type="radio" name="mei_minggu5" value="1"></td>
                    <td>I <input type="radio" name="mei_minggu5" value="2"></td>
                    <td>A <input type="radio" name="mei_minggu5" value="3"></td>
                  </tr>
                  <tr>
                    <td>Juni</td>
                    <td>L <input type="radio" name="jun_minggu1" value="0" checked=""></td>
                    <td>H <input type="radio" name="jun_minggu1" value="1"></td>
                    <td>I <input type="radio" name="jun_minggu1" value="2"></td>
                    <td>A <input type="radio" name="jun_minggu1" value="3"></td>
                    <td>L <input type="radio" name="jun_minggu2" value="0" checked=""></td>
                    <td>H <input type="radio" name="jun_minggu2" value="1"></td>
                    <td>I <input type="radio" name="jun_minggu2" value="2"></td>
                    <td>A <input type="radio" name="jun_minggu2" value="3"></td>
                    <td>L <input type="radio" name="jun_minggu3" value="0" checked=""></td>
                    <td>H <input type="radio" name="jun_minggu3" value="1"></td>
                    <td>I <input type="radio" name="jun_minggu3" value="2"></td>
                    <td>A <input type="radio" name="jun_minggu3" value="3"></td>
                    <td>L <input type="radio" name="jun_minggu4" value="0" checked=""></td>
                    <td>H <input type="radio" name="jun_minggu4" value="1"></td>
                    <td>I <input type="radio" name="jun_minggu4" value="2"></td>
                    <td>A <input type="radio" name="jun_minggu4" value="3"></td>
                    <td>L <input type="radio" name="jun_minggu5" value="0" checked=""></td>
                    <td>H <input type="radio" name="jun_minggu5" value="1"></td>
                    <td>I <input type="radio" name="jun_minggu5" value="2"></td>
                    <td>A <input type="radio" name="jun_minggu5" value="3"></td>
                  </tr>
                  <tr>
                    <td>Juli</td>
                    <td>L <input type="radio" name="jul_minggu1" value="0" checked=""></td>
                    <td>H <input type="radio" name="jul_minggu1" value="1"></td>
                    <td>I <input type="radio" name="jul_minggu1" value="2"></td>
                    <td>A <input type="radio" name="jul_minggu1" value="3"></td>
                    <td>L <input type="radio" name="jul_minggu2" value="0" checked=""></td>
                    <td>H <input type="radio" name="jul_minggu2" value="1"></td>
                    <td>I <input type="radio" name="jul_minggu2" value="2"></td>
                    <td>A <input type="radio" name="jul_minggu2" value="3"></td>
                    <td>L <input type="radio" name="jul_minggu3" value="0" checked=""></td>
                    <td>H <input type="radio" name="jul_minggu3" value="1"></td>
                    <td>I <input type="radio" name="jul_minggu3" value="2"></td>
                    <td>A <input type="radio" name="jul_minggu3" value="3"></td>
                    <td>L <input type="radio" name="jul_minggu4" value="0" checked=""></td>
                    <td>H <input type="radio" name="jul_minggu4" value="1"></td>
                    <td>I <input type="radio" name="jul_minggu4" value="2"></td>
                    <td>A <input type="radio" name="jul_minggu4" value="3"></td>
                    <td>L <input type="radio" name="jul_minggu5" value="0" checked=""></td>
                    <td>H <input type="radio" name="jul_minggu5" value="1"></td>
                    <td>I <input type="radio" name="jul_minggu5" value="2"></td>
                    <td>A <input type="radio" name="jul_minggu5" value="3"></td>
                  </tr>
                  <tr>
                    <td>Agustus</td>
                    <td>L <input type="radio" name="agu_minggu1" value="0" checked=""></td>
                    <td>H <input type="radio" name="agu_minggu1" value="1"></td>
                    <td>I <input type="radio" name="agu_minggu1" value="2"></td>
                    <td>A <input type="radio" name="agu_minggu1" value="3"></td>
                    <td>L <input type="radio" name="agu_minggu2" value="0" checked=""></td>
                    <td>H <input type="radio" name="agu_minggu2" value="1"></td>
                    <td>I <input type="radio" name="agu_minggu2" value="2"></td>
                    <td>A <input type="radio" name="agu_minggu2" value="3"></td>
                    <td>L <input type="radio" name="agu_minggu3" value="0" checked=""></td>
                    <td>H <input type="radio" name="agu_minggu3" value="1"></td>
                    <td>I <input type="radio" name="agu_minggu3" value="2"></td>
                    <td>A <input type="radio" name="agu_minggu3" value="3"></td>
                    <td>L <input type="radio" name="agu_minggu4" value="0" checked=""></td>
                    <td>H <input type="radio" name="agu_minggu4" value="1"></td>
                    <td>I <input type="radio" name="agu_minggu4" value="2"></td>
                    <td>A <input type="radio" name="agu_minggu4" value="3"></td>
                    <td>L <input type="radio" name="agu_minggu5" value="0" checked=""></td>
                    <td>H <input type="radio" name="agu_minggu5" value="1"></td>
                    <td>I <input type="radio" name="agu_minggu5" value="2"></td>
                    <td>A <input type="radio" name="agu_minggu5" value="3"></td>
                  </tr>
                  <tr>
                    <td>September</td>
                    <td>L <input type="radio" name="sep_minggu1" value="0" checked=""></td>
                    <td>H <input type="radio" name="sep_minggu1" value="1"></td>
                    <td>I <input type="radio" name="sep_minggu1" value="2"></td>
                    <td>A <input type="radio" name="sep_minggu1" value="3"></td>
                    <td>L <input type="radio" name="sep_minggu2" value="0" checked=""></td>
                    <td>H <input type="radio" name="sep_minggu2" value="1"></td>
                    <td>I <input type="radio" name="sep_minggu2" value="2"></td>
                    <td>A <input type="radio" name="sep_minggu2" value="3"></td>
                    <td>L <input type="radio" name="sep_minggu3" value="0" checked=""></td>
                    <td>H <input type="radio" name="sep_minggu3" value="1"></td>
                    <td>I <input type="radio" name="sep_minggu3" value="2"></td>
                    <td>A <input type="radio" name="sep_minggu3" value="3"></td>
                    <td>L <input type="radio" name="sep_minggu4" value="0" checked=""></td>
                    <td>H <input type="radio" name="sep_minggu4" value="1"></td>
                    <td>I <input type="radio" name="sep_minggu4" value="2"></td>
                    <td>A <input type="radio" name="sep_minggu4" value="3"></td>
                    <td>L <input type="radio" name="sep_minggu5" value="0" checked=""></td>
                    <td>H <input type="radio" name="sep_minggu5" value="1"></td>
                    <td>I <input type="radio" name="sep_minggu5" value="2"></td>
                    <td>A <input type="radio" name="sep_minggu5" value="3"></td>
                  </tr>
                  <tr>
                    <td>Oktober</td>
                    <td>L <input type="radio" name="okt_minggu1" value="0" checked=""></td>
                    <td>H <input type="radio" name="okt_minggu1" value="1"></td>
                    <td>I <input type="radio" name="okt_minggu1" value="2"></td>
                    <td>A <input type="radio" name="okt_minggu1" value="3"></td>
                    <td>L <input type="radio" name="okt_minggu2" value="0" checked=""></td>
                    <td>H <input type="radio" name="okt_minggu2" value="1"></td>
                    <td>I <input type="radio" name="okt_minggu2" value="2"></td>
                    <td>A <input type="radio" name="okt_minggu2" value="3"></td>
                    <td>L <input type="radio" name="okt_minggu3" value="0" checked=""></td>
                    <td>H <input type="radio" name="okt_minggu3" value="1"></td>
                    <td>I <input type="radio" name="okt_minggu3" value="2"></td>
                    <td>A <input type="radio" name="okt_minggu3" value="3"></td>
                    <td>L <input type="radio" name="okt_minggu4" value="0" checked=""></td>
                    <td>H <input type="radio" name="okt_minggu4" value="1"></td>
                    <td>I <input type="radio" name="okt_minggu4" value="2"></td>
                    <td>A <input type="radio" name="okt_minggu4" value="3"></td>
                    <td>L <input type="radio" name="okt_minggu5" value="0" checked=""></td>
                    <td>H <input type="radio" name="okt_minggu5" value="1"></td>
                    <td>I <input type="radio" name="okt_minggu5" value="2"></td>
                    <td>A <input type="radio" name="okt_minggu5" value="3"></td>
                  </tr>
                  <tr>
                    <td>November</td>
                    <td>L <input type="radio" name="nov_minggu1" value="0" checked=""></td>
                    <td>H <input type="radio" name="nov_minggu1" value="1"></td>
                    <td>I <input type="radio" name="nov_minggu1" value="2"></td>
                    <td>A <input type="radio" name="nov_minggu1" value="3"></td>
                    <td>L <input type="radio" name="nov_minggu2" value="0" checked=""></td>
                    <td>H <input type="radio" name="nov_minggu2" value="1"></td>
                    <td>I <input type="radio" name="nov_minggu2" value="2"></td>
                    <td>A <input type="radio" name="nov_minggu2" value="3"></td>
                    <td>L <input type="radio" name="nov_minggu3" value="0" checked=""></td>
                    <td>H <input type="radio" name="nov_minggu3" value="1"></td>
                    <td>I <input type="radio" name="nov_minggu3" value="2"></td>
                    <td>A <input type="radio" name="nov_minggu3" value="3"></td>
                    <td>L <input type="radio" name="nov_minggu4" value="0" checked=""></td>
                    <td>H <input type="radio" name="nov_minggu4" value="1"></td>
                    <td>I <input type="radio" name="nov_minggu4" value="2"></td>
                    <td>A <input type="radio" name="nov_minggu4" value="3"></td>
                    <td>L <input type="radio" name="nov_minggu5" value="0" checked=""></td>
                    <td>H <input type="radio" name="nov_minggu5" value="1"></td>
                    <td>I <input type="radio" name="nov_minggu5" value="2"></td>
                    <td>A <input type="radio" name="nov_minggu5" value="3"></td>
                  </tr>
                  <tr>
                    <td>Desember</td>
                    <td>L <input type="radio" name="des_minggu1" value="0" checked=""></td>
                    <td>H <input type="radio" name="des_minggu1" value="1"></td>
                    <td>I <input type="radio" name="des_minggu1" value="2"></td>
                    <td>A <input type="radio" name="des_minggu1" value="3"></td>
                    <td>L <input type="radio" name="des_minggu2" value="0" checked=""></td>
                    <td>H <input type="radio" name="des_minggu2" value="1"></td>
                    <td>I <input type="radio" name="des_minggu2" value="2"></td>
                    <td>A <input type="radio" name="des_minggu2" value="3"></td>
                    <td>L <input type="radio" name="des_minggu3" value="0" checked=""></td>
                    <td>H <input type="radio" name="des_minggu3" value="1"></td>
                    <td>I <input type="radio" name="des_minggu3" value="2"></td>
                    <td>A <input type="radio" name="des_minggu3" value="3"></td>
                    <td>L <input type="radio" name="des_minggu4" value="0" checked=""></td>
                    <td>H <input type="radio" name="des_minggu4" value="1"></td>
                    <td>I <input type="radio" name="des_minggu4" value="2"></td>
                    <td>A <input type="radio" name="des_minggu4" value="3"></td>
                    <td>L <input type="radio" name="des_minggu5" value="0" checked=""></td>
                    <td>H <input type="radio" name="des_minggu5" value="1"></td>
                    <td>I <input type="radio" name="des_minggu5" value="2"></td>
                    <td>A <input type="radio" name="des_minggu5" value="3"></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <button type="submit" class = 'btn btn-primary' style="margin-bottom: 0.5vw">Simpan</button>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
</section><!-- /.content -->
@endsection

@section('css')

<style>
  .selected{
    background-color: #a9b7d1 !important;
  }
  .table-bordered , th, td, tr{
    border: 1px solid #e3e3e3 !important;
  }
  th {
    text-align: center;
  }
  td {
    text-align: center;
    padding: 2px;
  }
  table { 
    border-spacing: 0;
    border-collapse: collapse;
  }
</style>
@endsection

@section('script')
<script>
  $(function(){
    @if(Session::has('error'))
      swal({
        title:"Maaf",
        text:"{{ Session::get('error') }}",
        type:"error",
        // timer:2000,// optional
        showConfirmButton:true // set to true or false
      });
    @endif
    $('.select').select2();
    $(":input").inputmask();

    var table = document.getElementById("absen-table");
 
    $('#absen-table tbody').on( 'click', 'tr', function () {
        // if ( $(this).hasClass('selected') ) {
        //     $(this).removeClass('selected');
        // }
        // else {
            $('.selected').removeClass('selected');
            // table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        // }
    });
  });

  function hapus(){
    swal({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, cancel!',
  confirmButtonClass: 'btn btn-success',
  cancelButtonClass: 'btn btn-danger',
  buttonsStyling: false
}).then(function () {
  swal(
    'Deleted!',
    'Your file has been deleted.',
    'success'
  )
}, function (dismiss) {
  // dismiss can be 'cancel', 'overlay',
  // 'close', and 'timer'
  if (dismiss === 'cancel') {
    swal(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})
  }
</script>
@stop