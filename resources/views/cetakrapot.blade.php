<!DOCTYPE html>
<html>
<head>
  <title>{{$data[0]->nama_lengkap}} - Semester {{$data[0]->semester}}</title>
</head>
<style type="text/css">
table {
  border: none;
}
</style>
<body>
<?php
  $bacaan = number_format((float)(($data[0]->fashohah + $data[0]->tajwid + $data[0]->ghorib_muskhilat + $data[0]->suara_lagu) / 4), 2, '.', '');
  $hafalan = number_format((float)(($data[0]->nilai_doa + $data[0]->nilai_dalil + $data[0]->nilai_surat) / 3), 2, '.', '');
  if($data[0]->nilai_tambahan == 0)
    $manqulan = number_format((float)(($data[0]->nilai_quran + $data[0]->nilai_hadits) / 2), 2, '.', '');
  else 
    $manqulan = number_format((float)(($data[0]->nilai_quran + $data[0]->nilai_hadits + $data[0]->nilai_tambahan) / 3), 2, '.', '');
  $akhlaq = number_format((float)($data[0]->nilai_akhlaq), 2, '.', '');
  
  if($data[0]->kode_murid < 10)
    $kode_nim = '00'.$data[0]->kode_murid;
  else if($data[0]->kode_murid < 100)
    $kode_nim = '0'.$data[0]->kode_murid;
  else
    $kode_nim = $data[0]->kode_murid;
?>
  <table>
    <thead>
      <tr>
        <th><img src="../uploads/Logo GP Aksel.jpg" width="100px" height="100px" style="margin-top: -20px"></th>
        <th>
          <h3 style="margin-left: 50px;margin-top: -20px;margin-bottom: 5px">PENDIDIKAN ISLAM GENERUS AKSELERASI</h3>
          <img src="../uploads/Kop.jpg" width="420" height="50" style="margin-left: 50px;margin-bottom: 5px">
          <h5 style="margin-left: 50px;margin-top: 0px;margin-bottom: 0px">Jl. Kemiri III - Pondok Cabe Udik - Pamulang Timur - Kota Tangerang Selatan</h5>
        </th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <hr style="width: 115%;margin-left: -45px">
  <center><strong><h3 style="margin-top: 5px;margin-bottom: 25px;padding-bottom: 4px;padding-top: 4px;border: double 4px">LAPORAN HASIL BELAJAR PESERTA DIDIK SEMESTER {{$data[0]->semester}}</h3></strong></center>
  <table style="max-width: 100%">
    <thead>
      <tr style="text-align: left !important;">
        <th>NIM </th>
        <th> : </th>
        <th> {{ substr($data[0]->tahun_masuk,2,2) }}0{{ $data[0]->tahun_masuk-2016 }}{{$kode_nim}}</th>
        <th style="color: white">-----</th>
        <th>Tempat Lahir </th>
        <th> : </th>
        <th> {{$data[0]->tempat_lahir}}</th>
      </tr>
      <tr style="text-align: left !important;">
        <th>Nama Lengkap </th>
        <th> : </th>
        <th> {{$data[0]->nama_lengkap}}</th>
        <th style="color: white">-----</th>
        <th>Tanggal Lahir </th>
        <th> : </th>
        <th> {{indonesian_date(date("d F Y", strtotime($data[0]->tanggal_lahir)),'j F Y','')}}</th>
      </tr>
      <tr style="text-align: left !important;">
        <th>Panggilan </th>
        <th> : </th>
        <th> {{$data[0]->nama_panggilan}}</th>
        <th style="color: white">-----</th>
        <th>Ortu/Wali </th>
        <th> : </th>
        <th> {{$data[0]->nama_wali}}</th>
      </tr>
      <tr style="text-align: left !important;">
        <th>Jenis Kelamin </th>
        <th> : </th>
        <th> {{$data[0]->jenis_kelamin}}</th>
        <th style="color: white">-----</th>
        <th>Desa </th>
        <th> : </th>
        <th> {{$data[0]->nama_desa}}</th>
      </tr>
      <tr style="text-align: left !important;">
        <th>Angkatan </th>
        <th> : </th>
        <th> {{$data[0]->tahun_masuk}}</th>
        <th style="color: white">-----</th>
        <th>Kelompok </th>
        <th> : </th>
        <th> {{$data[0]->nama_kelompok}}</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <table>
    <thead>
      <tr>
        <th><br><img src="../uploads/foto/{{$data[0]->foto}}.jpg" width="150px" height="200px"></th>
        <th>
          <div style="margin-left: 120px;width: 4px;height: 190px;background-color: darkslategray;margin-top: 20px"></div>
          <div style="top: 345px;left: 284px;width: {{$bacaan*360/100}}px;height: 24px;background-color: @if($bacaan <= 20) indianred @elseif($bacaan <= 40) orange @elseif($bacaan <= 60) gold @elseif($bacaan <= 80) yellowgreen @else limegreen @endif;position: absolute;"></div>
          <div style="top: 347px;left: {{$bacaan*360/100+290}}px;position: absolute;">{{$bacaan}}</div>
          <div style="top: 347px;left: 220px;position: absolute;">Bacaan</div>
          <div style="top: 385px;left: 284px;width: {{$hafalan*360/100}}px;height: 24px;background-color: @if($hafalan <= 20) indianred @elseif($hafalan <= 40) orange @elseif($hafalan <= 60) gold @elseif($hafalan <= 80) yellowgreen @else limegreen @endif;position: absolute;"></div>
          <div style="top: 387px;left: {{$hafalan*360/100+290}}px;position: absolute;">{{$hafalan}}</div>
          <div style="top: 387px;left: 216px;position: absolute;">Hafalan</div>
          <div style="top: 425px;left: 284px;width: {{$manqulan*360/100}}px;height: 24px;background-color: @if($manqulan <= 20) indianred @elseif($manqulan <= 40) orange @elseif($manqulan <= 60) gold @elseif($manqulan <= 80) yellowgreen @else limegreen @endif;position: absolute;"></div>
          <div style="top: 427px;left: {{$manqulan*360/100+290}}px;position: absolute;">{{$manqulan}}</div>
          <div style="top: 427px;left: 200px;position: absolute;">Manqulan</div>
          <div style="top: 465px;left: 284px;width: {{$akhlaq*360/100}}px;height: 24px;background-color: @if($akhlaq <= 20) indianred @elseif($akhlaq <= 40) orange @elseif($akhlaq <= 60) gold @elseif($akhlaq <= 80) yellowgreen @else limegreen @endif;position: absolute;"></div>
          <div style="top: 467px;left: {{$akhlaq*360/100+290}}px;position: absolute;">{{$akhlaq}}</div>
          <div style="top: 467px;left: 221px;position: absolute;">Akhlaq</div>
          <div style="margin-left: 120px;width: 400px;height: 4px;background-color: darkslategray"></div>
        </th>
      </tr>
      <tr>
        <th style="color: white">-</th>
        <th style="color: white">-</th>
        <th style="color: crimson">0</th>
        <th style="color: white">---------</th>
        <th style="color: indianred">20</th>
        <th style="color: white">---------</th>
        <th style="color: orange">40</th>
        <th style="color: white">---------</th>
        <th style="color: gold">60</th>
        <th style="color: white">---------</th>
        <th style="color: yellowgreen">80</th>
        <th style="color: white">---------</th>
        <th style="color: limegreen">100</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <br>
  <h3>Laporan Kehadiran</h3>
  <div style="left: 250px;bottom: 420px;position: absolute;width: 24px;height: 8px;background-color: limegreen"></div>
  <div style="left: 280px;bottom: 415px;position: absolute;">Hadir</div>
  <div style="left: 380px;bottom: 420px;position: absolute;width: 24px;height: 8px;background-color: gold"></div>
  <div style="left: 410px;bottom: 415px;position: absolute;">Sakit/Izin</div>
  <div style="left: 510px;bottom: 420px;position: absolute;width: 24px;height: 8px;background-color: crimson"></div>
  <div style="left: 540px;bottom: 415px;position: absolute;">Tanpa Keterangan</div>
  <div style="left: 0px;top: 638px;position: absolute;">5</div>
  <div style="left: 15px;top: 646px;position: absolute;width: 700px;height: 2px;background-color: lightgrey"></div>
  <div style="left: 0px;top: 673px;position: absolute;">4</div>
  <div style="left: 15px;top: 681px;position: absolute;width: 700px;height: 2px;background-color: lightgrey"></div>
  <div style="left: 0px;top: 708px;position: absolute;">3</div>
  <div style="left: 15px;top: 716px;position: absolute;width: 700px;height: 2px;background-color: lightgrey"></div>
  <div style="left: 0px;top: 743px;position: absolute;">2</div>
  <div style="left: 15px;top: 751px;position: absolute;width: 700px;height: 2px;background-color: lightgrey"></div>
  <div style="left: 0px;top: 778px;position: absolute;">1</div>
  <div style="left: 15px;top: 786px;position: absolute;width: 700px;height: 2px;background-color: lightgrey"></div>
  <div style="left: 0px;top: 815px;position: absolute;">0</div>
  <div style="left: 15px;top: 622px;position: absolute;width: 4px;height: 200px;background-color: darkslategray"></div>
  <div style="left: 15px;top: 822px;position: absolute;width: 700px;height: 4px;background-color: darkslategray"></div>

  <div style="left: 50px;bottom: 210px;position: absolute;width: 24px;height: {{$absen1[0]->hadir*35+1}}px;background-color: limegreen"></div>
  <div style="left: 75px;bottom: 210px;position: absolute;width: 24px;height: {{$absen1[0]->izin*35+1}}px;background-color: gold"></div>
  <div style="left: 100px;bottom: 210px;position: absolute;width: 24px;height: {{$absen1[0]->alfa*35+1}}px;background-color: crimson"></div>

  <div style="left: 155px;bottom: 210px;position: absolute;width: 24px;height: {{$absen2[0]->hadir*35+1}}px;background-color: limegreen"></div>
  <div style="left: 180px;bottom: 210px;position: absolute;width: 24px;height: {{$absen2[0]->izin*35+1}}px;background-color: gold"></div>
  <div style="left: 205px;bottom: 210px;position: absolute;width: 24px;height: {{$absen2[0]->alfa*35+1}}px;background-color: crimson"></div>

  <div style="left: 260px;bottom: 210px;position: absolute;width: 24px;height: {{$absen3[0]->hadir*35+1}}px;background-color: limegreen"></div>
  <div style="left: 285px;bottom: 210px;position: absolute;width: 24px;height: {{$absen3[0]->izin*35+1}}px;background-color: gold"></div>
  <div style="left: 310px;bottom: 210px;position: absolute;width: 24px;height: {{$absen3[0]->alfa*35+1}}px;background-color: crimson"></div>

  <div style="left: 365px;bottom: 210px;position: absolute;width: 24px;height: {{$absen4[0]->hadir*35+1}}px;background-color: limegreen"></div>
  <div style="left: 390px;bottom: 210px;position: absolute;width: 24px;height: {{$absen4[0]->izin*35+1}}px;background-color: gold"></div>
  <div style="left: 415px;bottom: 210px;position: absolute;width: 24px;height: {{$absen4[0]->alfa*35+1}}px;background-color: crimson"></div>

  <div style="left: 470px;bottom: 210px;position: absolute;width: 24px;height: {{$absen5[0]->hadir*35+1}}px;background-color: limegreen"></div>
  <div style="left: 495px;bottom: 210px;position: absolute;width: 24px;height: {{$absen5[0]->izin*35+1}}px;background-color: gold"></div>
  <div style="left: 520px;bottom: 210px;position: absolute;width: 24px;height: {{$absen5[0]->alfa*35+1}}px;background-color: crimson"></div>

  <div style="left: 575px;bottom: 210px;position: absolute;width: 24px;height: {{$absen6[0]->hadir*35+1}}px;background-color: limegreen"></div>
  <div style="left: 600px;bottom: 210px;position: absolute;width: 24px;height: {{$absen6[0]->izin*35+1}}px;background-color: gold"></div>
  <div style="left: 625px;bottom: 210px;position: absolute;width: 24px;height: {{$absen6[0]->alfa*35+1}}px;background-color: crimson"></div>

  <br><br><br><br><br><br><br><br><br><br><br>
  <table>
    <thead><tr>
    @if($data[0]->semester % 2 == 1)
      <th style="color: white">-------------</th>
      <th>Jan</th>
      <th style="color: white">-------------</th>
      <th>Feb</th>
      <th style="color: white">-------------</th>
      <th>Mar</th>
      <th style="color: white">-------------</th>
      <th>Apr</th>
      <th style="color: white">-------------</th>
      <th>Mei</th>
      <th style="color: white">-------------</th>
      <th>Jun</th>
    @else
      <th style="color: white">-------------</th>
      <th>Jul</th>
      <th style="color: white">-------------</th>
      <th>Agu</th>
      <th style="color: white">-------------</th>
      <th>Sep</th>
      <th style="color: white">-------------</th>
      <th>Okt</th>
      <th style="color: white">-------------</th>
      <th>Nov</th>
      <th style="color: white">-------------</th>
      <th>Des</th>
    @endif</tr>
    </thead>
  </table>
  <br>
  <div>
    Total Pertemuan = {{$absen1[0]->hadir+$absen2[0]->hadir+$absen3[0]->hadir+$absen4[0]->hadir+$absen5[0]->hadir+$absen6[0]->hadir+$absen1[0]->izin+$absen2[0]->izin+$absen3[0]->izin+$absen4[0]->izin+$absen5[0]->izin+$absen6[0]->izin+$absen1[0]->alfa+$absen2[0]->alfa+$absen3[0]->alfa+$absen4[0]->alfa+$absen5[0]->alfa+$absen6[0]->alfa}}
  </div>
  <div>
    Total Hadir = {{$absen1[0]->hadir+$absen2[0]->hadir+$absen3[0]->hadir+$absen4[0]->hadir+$absen5[0]->hadir+$absen6[0]->hadir}}
  </div>
  <div>
    Total Sakit/Izin = {{$absen1[0]->izin+$absen2[0]->izin+$absen3[0]->izin+$absen4[0]->izin+$absen5[0]->izin+$absen6[0]->izin}}
  </div>
  <div style="page-break-after: right;">
    Total Tanpa Keterangan = {{$absen1[0]->alfa+$absen2[0]->alfa+$absen3[0]->alfa+$absen4[0]->alfa+$absen5[0]->alfa+$absen6[0]->alfa}}
  </div>
  
  <center><strong><h2>Laporan Prestasi Siswa</h2></strong></center>
  <h4 style="margin-bottom: 10px">A. Kemampuan Bacaan</h4>
  <table border="1" width="100%">
    <thead>
    <tr>
      <th colspan="4">Kategori Nilai</th>
      <th rowspan="2">Nilai Akhir</th>
    </tr>
    <tr>
      <th>Fashohah</th>
      <th>Tajwid</th>
      <th>Tartil</th>
      <th>Suara & Lagu</th>
    </tr>
    </thead>
    <tbody>
      <tr style="text-align: center">
        <td>{{$data[0]->fashohah}}</td>
        <td>{{$data[0]->tajwid}}</td>
        <td>{{$data[0]->ghorib_muskhilat}}</td>
        <td>{{$data[0]->suara_lagu}}</td>
        <td style="font-weight: bold;">{{$bacaan}}</td>
      </tr>
    </tbody>
  </table>
  <br>
  <h4 style="margin-bottom: 10px">B. Kompetensi Hafalan</h4>
  <?php
    $prestasi = explode(",", $data[0]->prestasi_doa);
    $keterangan = explode(",", $data[0]->keterangan_doa);
  ?>
  <table border="1" width="100%">
    <thead>
      <tr>
        <th colspan="2">Hafalan Doa</th>
        <th rowspan="2">Nilai Akhir</th>
      </tr>
      <tr>
        <th>Nama Doa</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <tbody>
    @for($i = 0;$i < count($prestasi)-1;$i++)
      <tr>
        <td> {{$prestasi[$i]}}</td>
        <td style="text-align: center;">{{$keterangan[$i]}}</td>
        @if($i == 0)
        <td rowspan="{{count($prestasi)-1}}" style="text-align: center;font-weight: bold;"> {{$data[0]->nilai_doa}}</td>
        @endif
      </tr>
    @endfor
    </tbody>
  </table>
  <br>
  <?php
    $prestasi = explode(",", $data[0]->prestasi_dalil);
    $keterangan = explode(",", $data[0]->keterangan_dalil);
  ?>
  <table border="1" width="100%" style="page-break-after: right;">
    <thead>
      <tr>
        <th colspan="3">Hafalan Dalil</th>
        <th rowspan="2">Nilai Akhir</th>
      </tr>
      <tr>
        <th>Nama Dalil</th>
        <th>Jumlah</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <tbody>
    @for($i = 0;$i < count($prestasi)-1;$i++)
    <?php
      $dalil = explode("#", $prestasi[$i]);
    ?>
      <tr>
        <td> {{$dalil[0]}}</td>
        <td style="text-align: center;"> {{$dalil[1]}}</td>
        <td style="text-align: center;">{{$keterangan[$i]}}</td>
        @if($i == 0)
        <td rowspan="{{count($prestasi)-1}}" style="text-align: center;font-weight: bold;"> {{$data[0]->nilai_dalil}}</td>
        @endif
      </tr>
    @endfor
    </tbody>
  </table>
  <br>
  <?php
    $prestasi = explode(",", $data[0]->prestasi_surat);
    $keterangan = explode(",", $data[0]->keterangan_surat);
  ?>
  <table border="1" width="100%" style="page-break-after: right;">
    <thead>
      <tr>
        <th colspan="2">Hafalan Surat Pendek</th>
        <th rowspan="2">Nilai Akhir</th>
      </tr>
      <tr>
        <th>Nama Surat</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <tbody>
    @for($i = 0;$i < count($prestasi)-1;$i++)
      <tr>
        <td> {{$prestasi[$i]}}</td>
        <td style="text-align: center;">{{$keterangan[$i]}}</td>
        @if($i == 0)
        <td rowspan="{{count($prestasi)-1}}" style="text-align: center;font-weight: bold;"> {{$data[0]->nilai_surat}}</td>
        @endif
      </tr>
    @endfor
    </tbody>
  </table>
  <br>
  <h4 style="margin-bottom: 10px">C. Ketercapaian Pemanqulan</h4>
  <?php
    $prestasi = explode(",", $data[0]->prestasi_quran);
    $keterangan = explode(",", $data[0]->keterangan_quran);
  ?>
  <table border="1" width="100%">
    <thead>
      <tr>
        <th colspan="2">Al Quran</th>
        <th rowspan="2">Nilai Akhir</th>
      </tr>
      <tr>
        <th>Nama Surat</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <tbody>
    @for($i = 0;$i < count($prestasi)-1;$i++)
      <tr>
        <td> {{$prestasi[$i]}}</td>
        <td style="text-align: center;">{{$keterangan[$i]}}</td>
        @if($i == 0)
        <td rowspan="{{count($prestasi)-1}}" style="text-align: center;font-weight: bold;"> {{$data[0]->nilai_quran}}</td>
        @endif
      </tr>
    @endfor
    </tbody>
  </table>
  <br>
  <?php
    $prestasi = explode(",", $data[0]->prestasi_hadits);
    $keterangan = explode(",", $data[0]->keterangan_hadits);
  ?>
  <table border="1" width="100%">
    <thead>
      <tr>
        <th colspan="2">Hadits</th>
        <th rowspan="2">Nilai Akhir</th>
      </tr>
      <tr>
        <th>Nama Hadits</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <tbody>
    @for($i = 0;$i < count($prestasi)-1;$i++)
      <tr>
        <td> {{$prestasi[$i]}}</td>
        <td style="text-align: center;">{{$keterangan[$i]}}</td>
        @if($i == 0)
        <td rowspan="{{count($prestasi)-1}}" style="text-align: center;font-weight: bold;"> {{$data[0]->nilai_hadits}}</td>
        @endif
      </tr>
    @endfor
    </tbody>
  </table>
  <br>
  <?php
    $prestasi = explode(",", $data[0]->prestasi_tambahan);
    $keterangan = explode(",", $data[0]->keterangan_tambahan);
  ?>
  <table border="1" width="100%">
    <thead>
      <tr>
        <th colspan="2">Materi Tambahan</th>
        <th rowspan="2">Nilai Akhir</th>
      </tr>
      <tr>
        <th>Nama Materi</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <tbody>
    @for($i = 0;$i < count($prestasi)-1;$i++)
      <tr>
        <td> {{$prestasi[$i]}}</td>
        <td style="text-align: center;">{{$keterangan[$i]}}</td>
        @if($i == 0)
        <td rowspan="{{count($prestasi)-1}}" style="text-align: center;font-weight: bold;"> {{$data[0]->nilai_tambahan}}</td>
        @endif
      </tr>
    @endfor
    </tbody>
  </table>
  <br>
  <h4 style="margin-bottom: 10px">D. Akhlaq & Kepribadian</h4>
  <?php
    $prestasi = explode(",", $data[0]->prestasi_akhlaq);
    $keterangan = explode(",", $data[0]->keterangan_akhlaq);
  ?>
  <table border="1" width="100%" style="page-break-after: right;">
    <thead>
      <tr>
        <th>Nama Sikap</th>
        <th>Keterangan</th>
        <th>Nilai Akhir</th>
      </tr>
    </thead>
    <tbody>
    @for($i = 0;$i < count($prestasi)-1;$i++)
      <tr>
        <td> {{$prestasi[$i]}}</td>
        <td style="text-align: center;">{{$keterangan[$i]}}</td>
        @if($i == 0)
        <td rowspan="{{count($prestasi)-1}}" style="text-align: center;font-weight: bold;"> {{$data[0]->nilai_akhlaq}}</td>
        @endif
      </tr>
    @endfor
    </tbody>
  </table>
  <h4 style="margin-bottom: 10px">E. Catatan Wali Kelas</h4>
  <table border="1" width="100%">
    <thead>
      <tr>
        <th style="height: 150px;text-align: left;vertical-align: top;padding: 8px;padding-left: 12px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$data[0]->catatan}}</th>
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
  <br><br><br>
  <div>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian laporan hasil belajar peserta didik pada Semester {{$data[0]->semester}}. Laporan ini supaya dijadikan bahan evaluasi anak oleh orang tua/wali murid. Semoga laporan hasil belajar ini dapat bermanfaat dan menjadi acuan untuk peningkatan pada Semester berikutnya. Kami berharap anak bpk/ibu dapat menambah ilmu dan kemampuannya di Semester mendatang.
  </div>
  <br><br>
  <center>
    <img src="../uploads/Ajkh.jpg" width="300px" height="40px" style="margin-top: -10px">
    <br><br><br><br>
    <img src="../uploads/Logo PPG.jpg" width="160px" height="160px" style="margin-top: -10px">
  </center>
<?php
  function indonesian_date ($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
    if (trim ($timestamp) == '')
    {
            $timestamp = time ();
    }
    elseif (!ctype_digit ($timestamp))
    {
        $timestamp = strtotime ($timestamp);
    }
    # remove S (st,nd,rd,th) there are no such things in indonesia :p
    $date_format = preg_replace ("/S/", "", $date_format);
    $pattern = array (
        '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
        '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
        '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
        '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
        '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
        '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
        '/April/','/June/','/July/','/August/','/September/','/October/',
        '/November/','/December/',
    );
    $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
        'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
        'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
        'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
        'Oktober','November','Desember',
    );
    $date = date ($date_format, $timestamp);
    $date = preg_replace ($pattern, $replace, $date);
    $date = "{$date} {$suffix}";
    return $date;
  }
?>
  <div style="left: 20px;bottom: 210px;position: absolute;text-align: center;">
  Menerima,
    <br>
    Orang Tua / Wali Murid
    <br><br><br><br><br>
    (____________________)
  </div>
  <div style="right: 0px;bottom: 210px;position: absolute;text-align: center;">
    Tangerang Selatan, {{indonesian_date(date("d F Y"),'j F Y','')}}
    <br>
    Wali Kelas Murid
    <br><br><br><br><br>
    {{$data[0]->nama_guru}}
  </div>
  <br><br><br><br><br><br><br><br><br><br>
  <center>
    <div style="text-align: center;">
    Mengetahui,
      <br>
      Pengurus GP Aksel
      <br><br><br><br><br>
      (____________________)
    </div>
  </center>
</body>
</html>