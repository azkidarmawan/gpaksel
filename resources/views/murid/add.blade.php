@extends('spider::layouts.apps')
@section('content')



<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="{{ url('murid')}}"><i class="fa fa-circle-o"></i> Murid</a></li>
    <li class="active"><a href="{{ url('murid/add')}}"><i class="fa fa-plus"></i> Tambah Data</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title" style="margin-left: 1vw">Tambah Murid</h3>
        <div class="box-tools pull-right">
          <a href="{{ url('murid')}}" class="btn btn-xs btn-flat btn-primary"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12" style="margin-left: 1vw">
            <form class="form-horizontal" action="{{ url('murid/store') }}" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Nama Lengkap</label>
              </div>
              <div class="col-md-4">
                <input type="text" name="nama_lengkap" style="width: 60%" maxlength="100" required="">
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
              <div class="col-md-2">
                <label>Nama Panggilan</label>
              </div>
              <div class="col-md-4">
                <input type="text" name="nama_panggilan" style="width: 60%" maxlength="50" required="">
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Kelas</label>
              </div>
              <div class="col-md-4">
                <input type="text" name="kelas" maxlength="20" style="width: 60%">
              </div>
              <div class="col-md-6">
                <div class="col-md-4" style="margin-left: -14px">
                  <label>Jenis Kelamin</label>
                </div>
                <div class="col-md-8">
                  <input value="Laki-Laki" type="radio" name="jenis_kelamin" style="width: 4%" required="">
                  <label style="font-size: 13px;margin-right: 1vw"> Laki laki </label>
                  <input value="Perempuan" type="radio" name="jenis_kelamin" style="width: 4%" required="">
                  <label style="font-size: 13px;margin-right: 1vw"> Perempuan </label>
                  <label style="color: red"><i class="fa fa-certificate"></i> required</label>
                </div>
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Tempat Lahir</label>
              </div>
              <div class="col-md-4">
                <input type="text" name="tempat_lahir" style="width: 60%" maxlength="100">
              </div>
              <div class="col-md-2">
                <label>Tanggal Lahir</label>
              </div>
              <div class="col-md-4">
                <input type="text" name="tanggal_lahir" style="width: 60%" required="">
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Desa</label>
              </div>
              <div class="col-md-4">
                <Select id="select1" class="select" name="kode_desa" style="width: 60%" required="">
                  <option value=""> -- Pilih Desa -- </option>
                  @foreach($datas as $key=>$data)
                    <option value="{{$data->kode_desa}}">{{$data->nama_desa}}</option>
                  @endforeach
                </Select>
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
              <div class="col-md-2">
                <label>Kelompok</label>
              </div>
              <div class="col-md-4">
                <Select id="select2" class="select" name="kode_kelompok" style="width: 60%" required="">
                  <option value=""> -- Pilih Kelompok -- </option>
                </Select>
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Alamat Tinggal</label>
              </div>
              <div class="col-md-4">
                <input type="text" name="alamat_tinggal" style="width: 60%;font-size: 12px" maxlength="300">
              </div>
              <div class="col-md-2">
                <label>Nomor Telepon Murid</label>
              </div>
              <div class="col-md-4">
                <input type="text" data-inputmask="'mask': '9999 9999 99999'" name="no_hp_murid" style="width: 60%">
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Nama Ortu/Wali</label>
              </div>
              <div class="col-md-4">
                <input type="text" name="nama_wali" style="width: 60%" maxlength="100">
              </div>
              <div class="col-md-2">
                <label>Nomor Telepon Wali</label>
              </div>
              <div class="col-md-4">
                <input type="text" data-inputmask="'mask': '9999 9999 99999'" name="no_telp_wali" style="width: 60%">
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Tahun Masuk</label>
              </div>
              <div class="col-md-4">
                <select class="select" name="tahun_masuk" style="width: 60%" required="">
                  <option>{{(date('Y')+1)}}</option>
                  <option selected="">{{(date('Y'))}}</option>
                  <option>{{(date('Y')-1)}}</option>
                  <option>{{(date('Y')-2)}}</option>
                  <option>{{(date('Y')-3)}}</option>
                </select>
                <label style="color: red"><i class="fa fa-certificate"></i> required</label>
              </div>
              <div class="col-md-2">
                <label>Keterangan</label>
              </div>
              <div class="col-md-4">
                <input type="text" name="keterangan" style="width: 60%" maxlength="100">
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Foto (.jpg)</label>
              </div>
              <div class="col-md-4">
                <input type="file" class="form-control" id="foto" name="foto" style="width: 60%;font-size: 12px">
              </div>
            </div>
            <div class="row" style="margin-bottom: 0.5vw">
              <div class="col-md-2">
                <label>Photo Preview</label>
              </div>
              <div class="col-md-4">
                <img src="../uploads/foto/300x400.png" alt="..." id="showgambar" class="margin" style="max-width: 150px;max-height: 200px;width: 100%;height: 100%;">
              </div>
              <div class="col-md-2">
                <button type="submit" class = 'btn btn-primary btn-lg' style="margin-bottom: 0.5vw">Simpan</button>
              </div>
            </div>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
</section><!-- /.content -->
@endsection

@section('css')

<style>
  .table-bordered , th, td, tr{
    border: 1px solid #e3e3e3 !important;

  }
</style>
@endsection

@section('script')
<script>
  $(function(){
    @if(Session::has('error'))
      swal({
        title:"Maaf",
        text:"{{ Session::get('error') }}",
        type:"error",
        // timer:2000,// optional
        showConfirmButton:true // set to true or false
      });
    @endif
    $('[name="tanggal_lahir"]').datepicker({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15, // Creates a dropdown of 15 years to control year
      format:'dd-M-yyyy'
    });
    $( ".select" ).select2();
    $(":input").inputmask();
  });

  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

  function readURL2(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar2').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

  $("#foto").change(function () {
      readURL(this);
  });

  $("#foto").change(function () {
      readURL2(this);
  });

  $(document).on('change', '#select1', function(e) {
    console.log(e);
    var kode = e.target.value, 
        APP_URL = {!! json_encode(url('kelompok')) !!},
                uRl = APP_URL+'/getkelompok?kode_desa='+kode; 
    $.get(uRl, function(data) {
      $('#select2').find('option').remove(); // untuk mereset option yg ada di select2
      var opt = document.createElement("option");
      opt.text = "-- Pilih Kelompok --";
      opt.value = "";
      var sel = document.getElementById("select2");
      sel.appendChild(opt);
      // loop utk menambahkan option pd select2
      for (var i = 0; i < data.length; i++) {
        var option = document.createElement("option");
        option.text = data[i].nama_kelompok;
        option.value = data[i].kode_kelompok;
        var select = document.getElementById("select2");
        select.appendChild(option);
      }
    });
  });


  function hapus(){
    swal({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, cancel!',
  confirmButtonClass: 'btn btn-success',
  cancelButtonClass: 'btn btn-danger',
  buttonsStyling: false
}).then(function () {
  swal(
    'Deleted!',
    'Your file has been deleted.',
    'success'
  )
}, function (dismiss) {
  // dismiss can be 'cancel', 'overlay',
  // 'close', and 'timer'
  if (dismiss === 'cancel') {
    swal(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})
  }
</script>
@stop