<!DOCTYPE html>
<html>
  <head>
<!--     <style>
table, td, th {
    border: 1px solid black;
}

table {
    border-collapse: collapse;
    width: 100%;
}
th {
    height: 50px;
}
</style> -->
<style type="text/css">
   .contoh1 { font-size:16px;  line-height: 40px; }
   .isisurat { font-size:16px;  line-height: 70px; }
   .contoh2 { font-size:16px; line-height: -27px;}
   .contoh3 { font-size:16px; line-height: 110px;}
   .contoh4 { font-size:16px; line-height: 2;}
</style>
  </head>
  <body>
    <div class="container">
  <font face="Arial" color="black" size="4" > <p align="center"> KEMENTERIAN RISET, TEKNOLOGI, DAN PENDIDIKAN TINGGI</p></font>
    <p><img src="http://localhost/dist/resources/views/logo.png" alt="" width="80" height="60" style="float: left;"></p>
  <strong><font face="Arial" color="black" > <p class="contoh2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UNIVERSITAS TERBUKA </p></font></strong><br>
  <font face="Arial" color="black"> <p class="contoh2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jalan Cabe Raya, Pondok Cabe, Pamulang, Tangerang Selatan 15418 </p></font><br>
  <font face="Arial" color="black" size="3"> <p class="contoh2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Telepon: 021-7490941 (Hunting) </p></font><br>
  <font face="Arial" color="black" size="3"> <p class="contoh2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Faksimile: 021-7490147 (Bagian Umum), 021-7434290 (Sekretaris Rektor) </p></font><br>
  <font face="Arial" color="black" size="3"> <p class="contoh2" align="left"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Laman: ww.ut.ac.id</p></font><br>
  <br>
  <hr>
      <table class="table table-striped">

        <thead>
         <div class="container">
          @foreach($daftar as $blog)
                <?php
                $masa = explode("-", $blog->masa);
                $newmasa = $masa[0].'/'.$masa[1];
                ?>
              <font face="Arial"  color="black"> <p class="contoh2" align="left"> Nomor Surat: {{ ($blog->no_surat_tiras) }}</p></font><br>
              <font face="Arial" color="black"><p class="contoh2"  align="left">Lamp:1(satu) berkas</p></font><br>
              <font face="Arial" color="black"> <p class="contoh2" align="left">Hal: Hasil Rapat Tiras Non Pendas {{ ($newmasa) }}</p></font><br>
              <font face="Arial" color="black"> <p class="contoh1" align="left">Yth. Pembantu Dekan 1 {{ ($blog->nama_pendek) }}</p></font>
              <font face="Arial" color="black"> <p class="contoh2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Di Tempat</p></font><br>
              <br><br>
               <font face="Arial" color="black"><p class="contoh1"  align="justify">Bersama ini kamu sampaikan risalah rapat Tiras Bahan Ajar Non Pendas {{ ($blog->nama_pendek) }} pada tanggal {{ date("d-m-Y", strtotime($blog->tgl_rapat))}} untuk dikoreksi, apabila ada perubahan baik kode bahan ajar, bahan ajar aktif maupun non aktif mohon ditulis pada lampiran yang kami sampaikan. Selanjutnya hasil koreksi tersebut <strong>ditandatangani dan dibubuhi nama jelas</strong> . Hasil koreksi tersebut dapat kami terima kembali paling <strong>lambat tanggal 20 Januari 2017.</strong> </p></font>
               <br><br>
              <font face="Arial" color="black"><p class="contoh2"  align="left">Atas perhatian Bapak/Ibu, kami ucapkan terima kasih.</p></font><br><br><br><br>
              <font face="Arial" color="black"><p class="contoh2"  align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ ($blog->jabatan) }}</p></font><br><br><br><br><br>
              <font face="Arial" color="black"><p class="contoh2"  align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ ($blog->ketua_rapat) }}</p></font><br>
              <font face="Arial" color="black"><p class="contoh2"  align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP {{ ($blog->nip) }}</p></font><br><br>
              <font face="Arial" color="black"><p class="contoh2"  align="left">Tembusan</p></font><br>
              <font face="Arial" color="black"><p class="contoh2"  align="left">Dekan {{ ($blog->nama_pendek) }}</p></font><br>
          @endforeach
        </thead>
      </table>
    </div>
  </body>
</html>
