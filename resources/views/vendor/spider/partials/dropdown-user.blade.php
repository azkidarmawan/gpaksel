@include('spider::partials.customize.dropdown')
<!-- User Account: style can be found in dropdown.less -->
<li class="dropdown user user-menu">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <img src="{{ asset('vendor/upload/images/thumbnails/'.auth()->user()->getProfile->images_profile) }}" class="user-image user-online" alt="User Image">
    <span class="hidden-xs">
      <?php 
          $users = auth()->user()->getProfile->name;
		  $member_since = auth()->user()->getProfile->created_at;
		  $role = auth()->user()->getProfile->roles;
		  $date = date_format($member_since,"d-m-Y");
          $jumlah = 1;
          $hasil = implode(' ', array_slice(explode(' ', $users), 0, $jumlah));
        ?>
      <i class="user-name">{{ ucfirst($hasil) }}'s</i>
    </span>
  </a>
  <ul class="dropdown-menu">
    <!-- User image -->
    <li class="user-header">
      <img src="{{ asset('vendor/upload/images/thumbnails/'.auth()->user()->getProfile->images_profile) }}" class="img-circle user-online" alt="User Image">
      <p>
        <span class="user-name">{{ ucwords(auth()->user()->getProfile->name) }}</span>
		<small>Role : {{ $role }}</small>
        <small>Member since {{ $date }}</small>
      </p>
    
    <!-- Menu Footer-->
    <li class="user-footer">
      <div class="pull-left">
        <a href="{{ URL(config('spider.config.route_prefix').'/my-profile/'.base64_encode(auth()->user()->id)) }}" class="btn btn-info btn-flat">Profile</a>
      </div>
      <div class="pull-right">
        <a href="{{ url(config('spider.config.route_prefix').'/logout') }}"
          onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();" class="btn btn-danger btn-flat">Sign out</a>
           <form id="logout-form" action="{{ url(config('spider.config.route_prefix').'/logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>
      </div>
    </li>
  </ul>
</li>