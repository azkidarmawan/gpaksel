    <li class="treeview">
      <a href="#">
        <i class="fa fa-folder-open"></i> <span>Data Pendukung</span> <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ url('desa')}}"><i class="fa fa-university"></i> Desa</a></li>
        <li><a href="{{ url('kelompok')}}"><i class="fa fa-home"></i> Kelompok</a></li>
        <li><a href="{{ url('guru')}}"><i class="fa fa-graduation-cap"></i> Dewan Guru</a></li>
        <li><a href="{{ url('murid')}}"><i class="fa fa-user"></i> Murid</a></li>
      </ul>
    </li>