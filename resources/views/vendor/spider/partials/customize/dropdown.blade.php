<?php

use App\Notifikasi;
$data = new Notifikasi;
$daftar = $data->DaftarNotifikasi(auth()->user()->getProfile->id_notif,auth()->user()->getProfile->notifikasi);
$jumlah = count($daftar);

?>

<!-- Notifications: style can be found in dropdown.less -->
<li class="dropdown notifications-menu">
  <a href="" class="dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-bell-o"></i>
    <span class="label label-warning">{{ $jumlah }}</span>
  </a>
  <ul class="dropdown-menu">
    <li class="header">Anda memiliki {{ $jumlah }} notifikasi</li>
    <li>
      <!-- inner menu: contains the actual data -->
      <ul class="menu">
	  @foreach($daftar as $notif)
		<li><!-- start notifikasi -->
		<?php
		$url = explode(' ', $notif->log);
		?>
          <a href="{{ url($url[1]) }}">
		  @if($notif->user_create == auth()->user()->getProfile->name)
			Anda telah {{ $notif->log}}
		  @else
			{{ $notif->user_create }} telah {{ $notif->log}}
		  @endif
            <h4>
			  <?php
				$datetime1 = strtotime($notif->created_at);
				$datetime2 = strtotime(\Carbon\Carbon::Now('Asia/Jakarta'));
				$interval  = abs($datetime2 - $datetime1);
				$minutes   = round($interval / 60);
				// $datetime1 = $notif->created_at;
				// $datetime2 = \Carbon\Carbon::Now('Asia/Jakarta');
				// $diff=date_diff($datetime1,$datetime2);
				$days = round(($datetime2 - $datetime1) / 86400);
				$hours = round(($datetime2 - $datetime1)%86400 / 3600);
				$minutes = round(($datetime2 - $datetime1)%3600 / 60);
			  ?>
              <small><i class="fa fa-clock-o"></i> {{ $days }} hari {{ $hours }} jam {{ $minutes }} menit yang lalu</small>
            </h4>
          </a>
        </li><!-- end notifikasi -->
	  @endforeach
      </ul>
    </li>
    <!-- <li class="footer"><a href="#">View all</a></li> -->
  </ul>
</li>
<!-- Tasks: style can be found in dropdown.less -->
<li class="dropdown tasks-menu">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-flag-o"></i>
    <span class="label label-danger">9</span>
  </a>
  <ul class="dropdown-menu">
    <li class="header">You have 9 tasks</li>
    <li>
      <!-- inner menu: contains the actual data -->
      <ul class="menu">
        <li><!-- Task item -->
          <a href="#">
            <h3>
              Design some buttons
              <small class="pull-right">20%</small>
            </h3>
            <div class="progress xs">
              <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                <span class="sr-only">20% Complete</span>
              </div>
            </div>
          </a>
        </li><!-- end task item -->
      </ul>
    </li>
    <li class="footer">
      <a href="#">View all tasks</a>
    </li>
  </ul>
</li>