<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
    <div class="pull-left image">
      <img src="{{ asset('vendor/upload/images/thumbnails/'.auth()->user()->getProfile->images_profile) }}" class="img-circle user-online" alt="User Image">
    </div>
    <div class="pull-left info">
      <?php 
          $users = auth()->user()->getProfile->name;
          $jumlah = 1;
          $hasil = implode(' ', array_slice(explode(' ', $users), 0, $jumlah));
        ?>
      <p class="user-name">{{ ucfirst($hasil) }}'s</p>
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
	<br></br>
  </div>
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <li class="treeview">
      <a href="{{ URL(config('spider.config.route_prefix').'/dashboard') }}">
        <i class="fa fa-area-chart"></i> <span> Dashboard</span>
      </a>
    </li>
	<?php $role = auth()->user()->getProfile->roles; ?>
	@if ($role == 'admin' || $role == 'admin_tiras' || $role == 'admin_spm' || $role == 'admin_spc' || $role == 'admin_terima')
		@include('spider::partials.customize.sidebar-menu')
	@endif
	  <li class="treeview">
      <a href="#">
        <i class="fa fa-drivers-license"></i> <span> Laporan</span> <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ url('absen-guru')}}"><i class="fa fa-file-text"></i> Absensi Guru</a></li>
    		<li><a href="{{ url('absensi')}}"><i class="fa fa-file-text-o"></i> Absensi Murid</a></li>
        <li><a href="{{ url('penilaian')}}"><i class="fa fa-check-square-o"></i> Penilaian</a></li>
      </ul>
    </li>
    @if (auth()->user()->getProfile->roles != 'admin')
    @else
    <li class="header">SETTING</li>
    <li class="treeview">
      <a href="{{ URL(config('spider.config.route_prefix').'/users') }}">
        <i class="fa fa-users"></i> <span> Users</span>
      </a>
    </li>
    @endif
  </ul>
</section>
<!-- /.sidebar -->